import { all, fork } from 'redux-saga/effects';

import servicesSaga from 'services/servicesSaga';

export default function* root() {
  yield all([fork(servicesSaga)]);
}
