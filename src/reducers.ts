import { combineReducers } from 'redux';

import servicesReducer from './services/servicesReducer';
import { connectRouter } from 'connected-react-router';

const createReducers = (history: any) =>
  combineReducers({
    services: servicesReducer,
    router: connectRouter(history),
  });

export default createReducers;
