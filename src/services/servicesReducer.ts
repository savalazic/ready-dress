import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import sessionStorage from 'redux-persist/lib/storage/session';

import notificationReducer from 'services/notification/notificationReducer';
import utilReducer from 'services/util/utilReducer';
import adminReducer from 'services/admin/adminReducer';
import authReducer from 'services/auth/authReducer';
import merchantReducer from './merchant/merchantReducer';
import cartReducer from './cart/cartReducer';
import storeReducer from './store/storeReducer';
import appointmentReducer from './appointment/appointmentReducer';
import paymentReducer from './payment/paymentReducer';

const cartPersistConfig = {
  key: 'cart',
  storage,
};

const appointmentPersistConfig = {
  key: 'appointment',
  storage: sessionStorage,
};

const servicesReducer = combineReducers({
  notification: notificationReducer,
  util: utilReducer,
  auth: authReducer,
  admin: adminReducer,
  merchant: merchantReducer,
  store: storeReducer,
  cart: persistReducer(cartPersistConfig, cartReducer),
  appointment: persistReducer(appointmentPersistConfig, appointmentReducer),
  payment: paymentReducer,
});

export default servicesReducer;
