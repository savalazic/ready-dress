import { takeEvery, call, put } from 'redux-saga/effects';
import { ActionObject } from 'utils/redux';
import { push } from 'connected-react-router';

import {
  ActionTypes,
  ActionMap,
  getAppointmentsSuccess,
  getAppointmentsFailure,
  createAppointmentSuccess,
  createAppointmentFailure,
  acceptAppointmentSuccess,
  declineAppointmentFailure,
  declineAppointmentSuccess,
  acceptAppointmentFailure,
  saveCcAndCreateAppointmentSuccess,
} from './appointmentActions';
import { saveCreditCard } from '../payment/paymentSaga';
import appointmentApi from './appointmentApi';
import { showNotification } from 'services/notification/notificationActions';

function* getAppointments() {
  const { response, error } = yield call(appointmentApi.getAppointments);

  if (response) {
    yield put(getAppointmentsSuccess(response.data));
  } else {
    yield put(getAppointmentsFailure(error.response));
  }
}

function* createAppointment({
  payload: { storeBranchId, from, to, items },
}: ActionObject<ActionMap, ActionTypes.CREATE_APPOINTMENT_REQUEST>) {
  const { response, error } = yield call(
    appointmentApi.createAppointment,
    storeBranchId,
    from,
    to,
    items,
  );

  if (response) {
    yield put(createAppointmentSuccess(response.data));
    yield put(push('/'));
    yield put(
      showNotification(
        'Thanks! Your appointment has been successfully created.',
        3000,
        'success',
      ),
    );
  } else {
    yield put(createAppointmentFailure(error.response));
  }
}

function* acceptAppointment({
  payload: { id },
}: ActionObject<ActionMap, ActionTypes.ACCEPT_APPOINTMENT_REQUEST>) {
  const { response, error } = yield call(appointmentApi.acceptAppointment, id);

  if (response) {
    yield put(acceptAppointmentSuccess(response.data));
  } else {
    yield put(acceptAppointmentFailure(error.response));
  }
}

function* declineAppointment({
  payload: { id },
}: ActionObject<ActionMap, ActionTypes.ACCEPT_APPOINTMENT_REQUEST>) {
  const { response, error } = yield call(appointmentApi.declineAppointment, id);

  if (response) {
    yield put(declineAppointmentSuccess(response.data));
  } else {
    yield put(declineAppointmentFailure(error.response));
  }
}

function* saveCreditCardAndCreateAppointment({
  payload: { token, appointment },
}: ActionObject<
  ActionMap,
  ActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_REQUEST
>) {
  // @ts-ignore
  yield call(saveCreditCard, { payload: { token } });
  // @ts-ignore
  yield call(createAppointment, { payload: { ...appointment } });

  yield put(saveCcAndCreateAppointmentSuccess({}));
}

export default function* appointmentSaga() {
  yield takeEvery(ActionTypes.GET_APPOINTMENTS_REQUEST, getAppointments);
  yield takeEvery(ActionTypes.CREATE_APPOINTMENT_REQUEST, createAppointment);
  yield takeEvery(ActionTypes.ACCEPT_APPOINTMENT_REQUEST, acceptAppointment);
  yield takeEvery(ActionTypes.DECLINE_APPOINTMENT_REQUEST, declineAppointment);
  yield takeEvery(
    ActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_REQUEST,
    saveCreditCardAndCreateAppointment,
  );
}
