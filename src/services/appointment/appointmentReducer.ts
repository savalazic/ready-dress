import uniq from 'lodash/uniq';
import mapKeys from 'lodash/mapKeys';
import { createReducer } from 'utils/redux';
import { ActionMap, ActionTypes } from './appointmentActions';
import { StoreItem } from 'services/store/storeReducer';

export interface StoreAppointment {
  id: number;
  from: Date;
  to: Date;
  status: 'declined' | 'accepted' | 'pending_with_new_dates' | 'pending';
  articles: StoreItem[];
}

export interface AppointmentState {
  currentAppointment: {
    storeBranchId?: number;
    from?: Date;
    to?: Date;
    items: number[];
  };
  appointments: {
    byIds: { [id: number]: StoreAppointment };
    ids: number[];
  };
}

const initialState: AppointmentState = {
  currentAppointment: {
    items: [],
  },
  appointments: {
    byIds: {},
    ids: [],
  },
};

const appointmentReducer = createReducer<AppointmentState, ActionMap>(
  {
    [ActionTypes.CHANGE_APPOINTMENT]: (state, payload): AppointmentState => ({
      ...state,
      currentAppointment: { ...payload },
    }),
    [ActionTypes.GET_APPOINTMENTS_SUCCESS]: (
      state,
      payload,
    ): AppointmentState => ({
      ...state,
      appointments: {
        byIds: mapKeys(payload.appointments, 'id'),
        ids: uniq([
          ...state.appointments.ids,
          ...payload.appointments.map(a => a.id),
        ]),
      },
    }),
    [ActionTypes.CREATE_APPOINTMENT_SUCCESS]: (
      state,
      payload,
    ): AppointmentState => ({
      ...state,
      appointments: {
        byIds: Object.assign({}, state.appointments.byIds, {
          [payload.appointment.id]: payload.appointment,
        }),
        ids: [...state.appointments.ids, payload.appointment.id],
      },
    }),
    [ActionTypes.ACCEPT_APPOINTMENT_SUCCESS]: (
      state,
      payload,
    ): AppointmentState => ({
      ...state,
      appointments: {
        ...state.appointments,
        byIds: Object.assign({}, state.appointments.byIds, {
          [payload.appointment.id]: payload.appointment,
        }),
      },
    }),
    [ActionTypes.DECLINE_APPOINTMENT_SUCCESS]: (
      state,
      payload,
    ): AppointmentState => ({
      ...state,
      appointments: {
        ...state.appointments,
        byIds: Object.assign({}, state.appointments.byIds, {
          [payload.appointment.id]: payload.appointment,
        }),
      },
    }),
    [ActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_SUCCESS]: (
      state,
    ): AppointmentState => ({
      ...state,
      currentAppointment: {
        items: [],
      },
    }),
  },
  initialState,
);

export default appointmentReducer;
