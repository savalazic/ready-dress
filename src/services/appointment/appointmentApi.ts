import moment from 'moment';
import api from 'services/api';

const getAppointments = () =>
  api
    .get('appointment_requests')
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const createAppointment = (
  storeBranchId: number,
  from: Date,
  to: Date,
  items: Array<{ item_id: number; color_sample_id: number }>,
) =>
  api
    .post(`store_branches/${storeBranchId}/appointment_requests`, {
      appointment_request: {
        from: moment(from).format(),
        to: moment(to).format(),
      },
      articles: items,
    })
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const acceptAppointment = (id: number) =>
  api
    .put(`appointment_requests/${id}/accept`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const declineAppointment = (id: number) =>
  api
    .put(`appointment_requests/${id}/decline`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

export default {
  getAppointments,
  createAppointment,
  acceptAppointment,
  declineAppointment,
};
