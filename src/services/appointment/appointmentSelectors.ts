import { createSelector } from 'reselect';
import { State } from 'services/state';
import { createLoadingSelector } from 'utils/redux';

import { getStoresForCartSelector } from 'services/cart/cartSelectors';
import { ActionTypes } from './appointmentActions';

export const getCurrentAppointment = (state: State) =>
  state.services.appointment.currentAppointment;

export const getAppointmentsByIdSelector = (state: State) =>
  state.services.appointment.appointments.byIds;

export const getAppointmentsIdsSelector = (state: State) =>
  state.services.appointment.appointments.ids;

export const getAppointmentsSelector = createSelector(
  [getAppointmentsByIdSelector, getAppointmentsIdsSelector],
  (byId, ids) => ids.map(o => byId[o]),
);

export const getCurrentAppointmentBranchId = createSelector(
  [getStoresForCartSelector, getCurrentAppointment],
  (cartStores, appointment) => {
    if (appointment.storeBranchId) {
      return appointment.storeBranchId;
    }

    if (!appointment.storeBranchId && cartStores[0]) {
      return cartStores[0].id;
    }

    return null;
  },
);

export const isCreateAppointmentLoading = createLoadingSelector(
  ActionTypes.CREATE_APPOINTMENT_REQUEST,
);

export const isGetAppointmentsLoading = createLoadingSelector(
  ActionTypes.GET_APPOINTMENTS_REQUEST,
);

export const getAppointmentAcceptLoading = createLoadingSelector(
  ActionTypes.ACCEPT_APPOINTMENT_REQUEST,
);

export const getAppointmentDeclineLoading = createLoadingSelector(
  ActionTypes.DECLINE_APPOINTMENT_REQUEST,
);

export const getSaveCcAndCreateAppointmentLoading = createLoadingSelector(
  ActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_REQUEST,
);
