import { createAction } from 'utils/redux';
import { StoreAppointment } from './appointmentReducer';

export enum ActionTypes {
  CHANGE_APPOINTMENT = 'CUSTOMER/CHANGE_APPOINTMENT',

  CREATE_APPOINTMENT_REQUEST = 'CUSTOMER/CREATE_APPOINTMENT_REQUEST',
  CREATE_APPOINTMENT_SUCCESS = 'CUSTOMER/CREATE_APPOINTMENT_SUCCESS',
  CREATE_APPOINTMENT_FAILURE = 'CUSTOMER/CREATE_APPOINTMENT_FAILURE',

  GET_APPOINTMENTS_REQUEST = 'CUSTOMER/GET_APPOINTMENTS_REQUEST',
  GET_APPOINTMENTS_SUCCESS = 'CUSTOMER/GET_APPOINTMENTS_SUCCESS',
  GET_APPOINTMENTS_FAILURE = 'CUSTOMER/GET_APPOINTMENTS_FAILURE',

  ACCEPT_APPOINTMENT_REQUEST = 'CUSTOMER/ACCEPT_APPOINTMENT_REQUEST',
  ACCEPT_APPOINTMENT_SUCCESS = 'CUSTOMER/ACCEPT_APPOINTMENT_SUCCESS',
  ACCEPT_APPOINTMENT_FAILURE = 'CUSTOMER/ACCEPT_APPOINTMENT_FAILURE',

  DECLINE_APPOINTMENT_REQUEST = 'CUSTOMER/DECLINE_APPOINTMENT_REQUEST',
  DECLINE_APPOINTMENT_SUCCESS = 'CUSTOMER/DECLINE_APPOINTMENT_SUCCESS',
  DECLINE_APPOINTMENT_FAILURE = 'CUSTOMER/DECLINE_APPOINTMENT_FAILURE',

  SAVE_CC_AND_CREATE_APPOINTMENT_REQUEST = 'CUSTOMER/SAVE_CC_AND_CREATE_APPOINTMENT_REQUEST',
  SAVE_CC_AND_CREATE_APPOINTMENT_SUCCESS = 'CUSTOMER/SAVE_CC_AND_CREATE_APPOINTMENT_SUCCESS',
}

export interface ActionMap {
  [ActionTypes.CHANGE_APPOINTMENT]: {
    storeBranchId?: number;
    from?: Date;
    to?: Date;
    items: number[];
  };

  [ActionTypes.CREATE_APPOINTMENT_REQUEST]: {
    storeBranchId: number;
    from: Date;
    to: Date;
    items: any[];
  };
  [ActionTypes.CREATE_APPOINTMENT_SUCCESS]: { appointment: StoreAppointment };
  [ActionTypes.CREATE_APPOINTMENT_FAILURE]: any;

  [ActionTypes.GET_APPOINTMENTS_REQUEST]: {};
  [ActionTypes.GET_APPOINTMENTS_SUCCESS]: { appointments: StoreAppointment[] };
  [ActionTypes.GET_APPOINTMENTS_FAILURE]: any;

  [ActionTypes.ACCEPT_APPOINTMENT_REQUEST]: { id: number };
  [ActionTypes.ACCEPT_APPOINTMENT_SUCCESS]: { appointment: StoreAppointment };
  [ActionTypes.ACCEPT_APPOINTMENT_FAILURE]: any;

  [ActionTypes.DECLINE_APPOINTMENT_REQUEST]: { id: number };
  [ActionTypes.DECLINE_APPOINTMENT_SUCCESS]: { appointment: StoreAppointment };
  [ActionTypes.DECLINE_APPOINTMENT_FAILURE]: any;

  [ActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_REQUEST]: {
    token: string;
    appointment: {
      storeBranchId: number;
      from: Date;
      to: Date;
      items: number[];
    };
  };

  [ActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_SUCCESS]: any;
}

export const changeAppointment = ({
  storeBranchId,
  from,
  to,
  items = [],
}: {
  storeBranchId?: number;
  from?: Date;
  to?: Date;
  items?: number[];
}) =>
  createAction<ActionMap, ActionTypes.CHANGE_APPOINTMENT>(
    ActionTypes.CHANGE_APPOINTMENT,
  )({
    storeBranchId,
    from,
    to,
    items,
  });

export const createAppointmentRequest = (
  storeBranchId: number,
  from: Date,
  to: Date,
  items: number[],
) =>
  createAction<ActionMap, ActionTypes.CREATE_APPOINTMENT_REQUEST>(
    ActionTypes.CREATE_APPOINTMENT_REQUEST,
  )({
    storeBranchId,
    from,
    to,
    items,
  });

export const createAppointmentSuccess = (appointment: StoreAppointment) =>
  createAction<ActionMap, ActionTypes.CREATE_APPOINTMENT_SUCCESS>(
    ActionTypes.CREATE_APPOINTMENT_SUCCESS,
  )({
    appointment,
  });

export const createAppointmentFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.CREATE_APPOINTMENT_FAILURE>(
    ActionTypes.CREATE_APPOINTMENT_FAILURE,
  )({
    error,
  });

export const getAppointmentsRequest = createAction<
  ActionMap,
  ActionTypes.GET_APPOINTMENTS_REQUEST
>(ActionTypes.GET_APPOINTMENTS_REQUEST);

export const getAppointmentsSuccess = (appointments: StoreAppointment[]) =>
  createAction<ActionMap, ActionTypes.GET_APPOINTMENTS_SUCCESS>(
    ActionTypes.GET_APPOINTMENTS_SUCCESS,
  )({
    appointments,
  });

export const getAppointmentsFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_APPOINTMENTS_FAILURE>(
    ActionTypes.GET_APPOINTMENTS_FAILURE,
  )({
    error,
  });

export const acceptAppointmentRequest = (id: number) =>
  createAction<ActionMap, ActionTypes.ACCEPT_APPOINTMENT_REQUEST>(
    ActionTypes.ACCEPT_APPOINTMENT_REQUEST,
  )({
    id,
  });

export const acceptAppointmentSuccess = (appointment: StoreAppointment) =>
  createAction<ActionMap, ActionTypes.ACCEPT_APPOINTMENT_SUCCESS>(
    ActionTypes.ACCEPT_APPOINTMENT_SUCCESS,
  )({
    appointment,
  });

export const acceptAppointmentFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.ACCEPT_APPOINTMENT_FAILURE>(
    ActionTypes.ACCEPT_APPOINTMENT_FAILURE,
  )({
    error,
  });

export const declineAppointmentRequest = (id: number) =>
  createAction<ActionMap, ActionTypes.DECLINE_APPOINTMENT_REQUEST>(
    ActionTypes.DECLINE_APPOINTMENT_REQUEST,
  )({
    id,
  });

export const declineAppointmentSuccess = (appointment: StoreAppointment) =>
  createAction<ActionMap, ActionTypes.DECLINE_APPOINTMENT_SUCCESS>(
    ActionTypes.DECLINE_APPOINTMENT_SUCCESS,
  )({
    appointment,
  });

export const declineAppointmentFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.DECLINE_APPOINTMENT_FAILURE>(
    ActionTypes.DECLINE_APPOINTMENT_FAILURE,
  )({
    error,
  });

export const saveCcAndCreateAppointment = (
  token: string,
  appointment: {
    storeBranchId: number;
    from: Date;
    to: Date;
    items: number[];
  },
) =>
  createAction<ActionMap, ActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_REQUEST>(
    ActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_REQUEST,
  )({
    token,
    appointment,
  });

export const saveCcAndCreateAppointmentSuccess = createAction<
  ActionMap,
  ActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_SUCCESS
>(ActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_SUCCESS);
