import { combineReducers } from 'redux';

import storeBranchReducer, {
  StoreBranchState,
} from './storeBranch/storeBranchReducer';
import storeItemReducer, { StoreItemState } from './storeItem/storeItemReducer';

export interface MerchantState {
  storeBranch: StoreBranchState;
  storeItem: StoreItemState;
}

const merchantReducer = combineReducers({
  storeBranch: storeBranchReducer,
  storeItem: storeItemReducer,
});

export default merchantReducer;
