import toString from 'lodash/toString';
import mapKeys from 'lodash/mapKeys';
import { omit } from 'utils/object';
import { createReducer } from 'utils/redux';
import { ActionMap, ActionTypes } from './storeItemActions';

export interface StoreImage {
  id: number;
  url: string;
}

interface StoreItemImage {
  id: number;
  url: string;
}

export interface StoreItem {
  id: number;
  price_in_centum: number;
  title: string;
  images: StoreImage[];
  description: string;
  color_samples: StoreItemImage[];
}

export interface StoreItemState {
  items: {
    [id: number]: StoreItem;
  } | null;
}

export const initialState: StoreItemState = {
  items: null,
};

const storeItemReducer = createReducer<StoreItemState, ActionMap>(
  {
    [ActionTypes.GET_ITEMS_SUCCESS]: (state, payload): StoreItemState => ({
      ...state,
      items: mapKeys(payload.items, 'id'),
    }),
    [ActionTypes.REMOVE_ITEM_SUCCESS]: (state, payload): StoreItemState => ({
      ...state,
      items: omit(state.items, [toString(payload.id)]),
    }),
    [ActionTypes.ADD_ITEM_SUCCESS]: (state, payload): StoreItemState => ({
      ...state,
      items: Object.assign({}, state.items, {
        [payload.item.id]: payload.item,
      }),
    }),
    [ActionTypes.GET_ITEM_SUCCESS]: (state, payload): StoreItemState => ({
      ...state,
      items: Object.assign({}, state.items, {
        [payload.item.id]: payload.item,
      }),
    }),
    [ActionTypes.EDIT_ITEM_SUCCESS]: (state, payload): StoreItemState => ({
      ...state,
      items: Object.assign({}, state.items, {
        [payload.item.id]: payload.item,
      }),
    }),
  },
  initialState,
);

export default storeItemReducer;
