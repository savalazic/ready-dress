import api from 'services/api';
import { StoreItem } from './storeItemReducer';

const getStoreItems = (branchId: number) => {
  return api
    .get(`merchant/store_branches/${branchId}/items`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));
};

const getStoreItem = (id: number, branchId: number) => {
  return api
    .get(`merchant/store_branches/${branchId}/items/${id}`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));
};

const removeItem = (id: number, branchId: number) => {
  return api
    .remove(`merchant/store_branches/${branchId}/items`, id)
    .then(response => ({ response }))
    .catch(error => ({ error }));
};

const addItem = (
  item: {
    price_in_centum: number;
    title: string;
    images: string[];
    color_samples: string[];
  },
  branchId: number,
) => {
  return api
    .post(`merchant/store_branches/${branchId}/items`, { item })
    .then(response => ({ response }))
    .catch(error => ({ error }));
};

const editItem = (item: StoreItem, id: number, branchId: number) => {
  return api
    .put(`merchant/store_branches/${branchId}/items/${id}`, { item })
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));
};

const addImage = (branchId: number, itemId: number, base64image: string) =>
  api
    .post(`merchant/store_branches/${branchId}/items/${itemId}/images`, {
      image: {
        base64content: base64image,
      },
    })
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const removeImage = (branchId: number, itemId: number, imageId: number) =>
  api
    .remove(
      `merchant/store_branches/${branchId}/items/${itemId}/images`,
      imageId,
    )
    .then(response => ({ response }))
    .catch(error => ({ error }));

export default {
  getStoreItems,
  getStoreItem,
  removeItem,
  addItem,
  editItem,
  addImage,
  removeImage,
};
