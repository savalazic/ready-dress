import {
  ActionTypes,
  ActionMap,
  getItemsSuccess,
  getItemsFailure,
  removeItemFailure,
  removeItemSuccess,
  addItemFailure,
  addItemSuccess,
  getItemSuccess,
  getItemFailure,
  editItemSuccess,
  editItemFailure,
  addImageSuccess,
  addImageFailure,
  removeImageSuccess,
  removeImageFailure,
} from './storeItemActions';
import { ActionObject } from 'utils/redux';

import { getItemsSelector } from './storeItemSelectors';

import { showNotification } from 'services/notification/notificationActions';

import { takeEvery, put, call, select, all } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import storeItemApi from './storeItemApi';
import { getPrevPath, fileToBase64 } from 'utils/string';

function* getItems({
  payload: { branchId },
}: ActionObject<ActionMap, ActionTypes.GET_ITEMS_REQUEST>) {
  const { response, error } = yield call(storeItemApi.getStoreItems, branchId);
  if (response) {
    yield put(getItemsSuccess(response.data));
  } else {
    yield put(getItemsFailure(error.response));
  }
}

function* getItem({
  payload: { id, branchId },
}: ActionObject<ActionMap, ActionTypes.GET_ITEM_REQUEST>) {
  const { response, error } = yield call(
    storeItemApi.getStoreItem,
    id,
    branchId,
  );
  if (response) {
    yield put(getItemSuccess(response.data));
  } else {
    yield put(getItemFailure(error.response.data));
  }
}

function* removeItem({
  payload: { id, branchId },
}: ActionObject<ActionMap, ActionTypes.REMOVE_ITEM_REQUEST>) {
  const { response, error } = yield call(storeItemApi.removeItem, id, branchId);

  const items = yield select(getItemsSelector);
  const item = yield items[id];

  if (response) {
    yield put(removeItemSuccess(id));
    yield put(
      showNotification(`Item ${item.title} is removed`, 3000, 'success'),
    );
  } else {
    yield put(removeItemFailure(error.response));
  }
}

function* addItem({
  payload: { item, branchId },
}: ActionObject<ActionMap, ActionTypes.ADD_ITEM_REQUEST>) {
  const base64images = yield all(
    [...item.images].map((i: any) => call(fileToBase64, i)),
  );

  const base64ColorSamples = yield all(
    [...item.color_samples].map((i: any) => call(fileToBase64, i)),
  );

  const { response, error } = yield call(
    storeItemApi.addItem,
    { ...item, images: base64images, color_samples: base64ColorSamples },
    branchId,
  );
  if (response) {
    yield put(addItemSuccess(response.data));

    const currentPath = yield select(state => state.router.location.pathname);
    const prevPath = yield call(getPrevPath, currentPath, -1);

    yield put(push(prevPath));
  } else {
    yield put(addItemFailure(error.response));
  }
}

function* editItem({
  payload: { id, item, branchId },
}: ActionObject<ActionMap, ActionTypes.EDIT_ITEM_REQUEST>) {
  const { response, error } = yield call(
    storeItemApi.editItem,
    item,
    id,
    branchId,
  );

  const items = yield select(getItemsSelector);
  const editedItem = yield items[id];

  if (response) {
    yield put(editItemSuccess(response.data));
    yield put(
      showNotification(`Item ${editedItem.title} is Edited`, 3000, 'success'),
    );

    const currentPath = yield select(state => state.router.location.pathname);
    const prevPath = yield call(getPrevPath, currentPath, -2);

    yield put(push(prevPath));
  } else {
    yield put(editItemFailure(error.response));
  }
}

function* addImage({
  payload: { branchId, itemId, image },
}: ActionObject<ActionMap, ActionTypes.ADD_IMAGE_REQUEST>) {
  const base64images = yield call(fileToBase64, image);

  const { response, error } = yield call(
    storeItemApi.addImage,
    branchId,
    itemId,
    base64images,
  );
  if (response) {
    yield put(addImageSuccess(response.data));
  } else {
    yield put(addImageFailure(error.response.data));
  }
}

function* removeImage({
  payload: { branchId, itemId, imageId },
}: ActionObject<ActionMap, ActionTypes.REMOVE_IMAGE_REQUEST>) {
  const { response, error } = yield call(
    storeItemApi.removeImage,
    branchId,
    itemId,
    imageId,
  );
  if (response) {
    yield put(removeImageSuccess(imageId));
  } else {
    yield put(removeImageFailure(error.response.data));
  }
}

export default function* storeItemSaga() {
  yield takeEvery(ActionTypes.GET_ITEMS_REQUEST, getItems);
  yield takeEvery(ActionTypes.GET_ITEM_REQUEST, getItem);
  yield takeEvery(ActionTypes.REMOVE_ITEM_REQUEST, removeItem);
  yield takeEvery(ActionTypes.ADD_ITEM_REQUEST, addItem);
  yield takeEvery(ActionTypes.EDIT_ITEM_REQUEST, editItem);
  yield takeEvery(ActionTypes.ADD_IMAGE_REQUEST, addImage);
  yield takeEvery(ActionTypes.REMOVE_IMAGE_REQUEST, removeImage);
}
