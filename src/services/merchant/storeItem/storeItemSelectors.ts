import { State } from 'services/state';
import { createLoadingSelector } from 'utils/redux';
import { ActionTypes } from './storeItemActions';

export const getItemsSelector = (state: State) =>
  state.services.merchant.storeItem.items;

export const getItemsLoadingSelector = createLoadingSelector(
  ActionTypes.GET_ITEMS_REQUEST,
);

export const getItemLoadingSelector = createLoadingSelector(
  ActionTypes.GET_ITEM_REQUEST,
);

export const getEditItemLoadingSelector = createLoadingSelector(
  ActionTypes.EDIT_ITEM_REQUEST,
);

export const getRemoveItemLoadingSelector = createLoadingSelector(
  ActionTypes.REMOVE_ITEM_REQUEST,
);

export const getAddItemLoadingSelector = createLoadingSelector(
  ActionTypes.ADD_ITEM_REQUEST,
);

export const getAddImageLoadingSelector = createLoadingSelector(
  ActionTypes.ADD_IMAGE_REQUEST,
);

export const getAemoveImageLoadingSelector = createLoadingSelector(
  ActionTypes.REMOVE_IMAGE_REQUEST,
);
