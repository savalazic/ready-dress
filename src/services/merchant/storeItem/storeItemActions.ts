import { createAction } from 'utils/redux';
import { StoreItem, StoreImage } from './storeItemReducer';

export enum ActionTypes {
  GET_ITEMS_REQUEST = 'MERCHANT/GET_ITEMS_REQUEST',
  GET_ITEMS_SUCCESS = 'MERCHANT/GET_ITEMS_SUCCESS',
  GET_ITEMS_FAILURE = 'MERCHANT/GET_ITEMS_FAILURE',

  GET_ITEM_REQUEST = 'MERCHANT/GET_ITEM_REQUEST',
  GET_ITEM_SUCCESS = 'MERCHANT/GET_ITEM_SUCCESS',
  GET_ITEM_FAILURE = 'MERCHANT/GET_ITEM_FAILURE',

  REMOVE_ITEM_REQUEST = 'MERCHANT/REMOVE_ITEM_REQUEST',
  REMOVE_ITEM_SUCCESS = 'MERCHANT/REMOVE_ITEM_SUCCESS',
  REMOVE_ITEM_FAILURE = 'MERCHANT/REMOVE_ITEM_FAILURE',

  ADD_ITEM_REQUEST = 'MERCHANT/ADD_ITEM_REQUEST',
  ADD_ITEM_SUCCESS = 'MERCHANT/ADD_ITEM_SUCCESS',
  ADD_ITEM_FAILURE = 'MERCHANT/ADD_ITEM_FAILURE',

  EDIT_ITEM_REQUEST = 'MERCHANT/EDIT_ITEM_REQUEST',
  EDIT_ITEM_SUCCESS = 'MERCHANT/EDIT_ITEM_SUCCESS',
  EDIT_ITEM_FAILURE = 'MERCHANT/EDIT_ITEM_FAILURE',

  ADD_IMAGE_REQUEST = 'MERCHANT/ADD_IMAGE_REQUEST',
  ADD_IMAGE_SUCCESS = 'MERCHANT/ADD_IMAGE_SUCCESS',
  ADD_IMAGE_FAILURE = 'MERCHANT/ADD_IMAGE_FAILURE',

  REMOVE_IMAGE_REQUEST = 'MERCHANT/REMOVE_IMAGE_REQUEST',
  REMOVE_IMAGE_SUCCESS = 'MERCHANT/REMOVE_IMAGE_SUCCESS',
  REMOVE_IMAGE_FAILURE = 'MERCHANT/REMOVE_IMAGE_FAILURE',
}

export interface ActionMap {
  [ActionTypes.GET_ITEMS_REQUEST]: {
    branchId: number;
  };

  [ActionTypes.GET_ITEMS_SUCCESS]: {
    items: StoreItem[];
  };

  [ActionTypes.GET_ITEMS_FAILURE]: any;

  [ActionTypes.GET_ITEM_REQUEST]: {
    id: number;
    branchId: number;
  };
  [ActionTypes.GET_ITEM_SUCCESS]: {
    item: StoreItem;
  };
  [ActionTypes.GET_ITEM_FAILURE]: any;

  [ActionTypes.REMOVE_ITEM_REQUEST]: {
    id: number;
    branchId: number;
  };
  [ActionTypes.REMOVE_ITEM_SUCCESS]: {
    id: number;
  };
  [ActionTypes.REMOVE_ITEM_FAILURE]: any;

  [ActionTypes.ADD_ITEM_REQUEST]: {
    item: StoreItem;
    branchId: number;
  };
  [ActionTypes.ADD_ITEM_SUCCESS]: {
    item: StoreItem;
  };
  [ActionTypes.ADD_ITEM_FAILURE]: any;

  [ActionTypes.EDIT_ITEM_REQUEST]: {
    item: StoreItem;
    id: number;
    branchId: number;
  };
  [ActionTypes.EDIT_ITEM_SUCCESS]: {
    item: StoreItem;
  };
  [ActionTypes.EDIT_ITEM_FAILURE]: any;

  [ActionTypes.ADD_IMAGE_REQUEST]: {
    branchId: number;
    itemId: number;
    image: File;
  };
  [ActionTypes.ADD_IMAGE_SUCCESS]: {
    image: StoreImage;
  };
  [ActionTypes.ADD_IMAGE_FAILURE]: any;

  [ActionTypes.REMOVE_IMAGE_REQUEST]: {
    branchId: number;
    itemId: number;
    imageId: number;
  };
  [ActionTypes.REMOVE_IMAGE_SUCCESS]: {
    imageId: number;
  };
  [ActionTypes.REMOVE_IMAGE_FAILURE]: any;
}

// Get Items
export const getItemsRequest = (branchId: number) =>
  createAction<ActionMap, ActionTypes.GET_ITEMS_REQUEST>(
    ActionTypes.GET_ITEMS_REQUEST,
  )({
    branchId,
  });

export const getItemsSuccess = (items: StoreItem[]) =>
  createAction<ActionMap, ActionTypes.GET_ITEMS_SUCCESS>(
    ActionTypes.GET_ITEMS_SUCCESS,
  )({
    items,
  });

export const getItemsFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_ITEMS_FAILURE>(
    ActionTypes.GET_ITEMS_FAILURE,
  )({
    error,
  });

// Get Item
export const getItemRequest = (id: number, branchId: number) =>
  createAction<ActionMap, ActionTypes.GET_ITEM_REQUEST>(
    ActionTypes.GET_ITEM_REQUEST,
  )({
    id,
    branchId,
  });

export const getItemSuccess = (item: StoreItem) =>
  createAction<ActionMap, ActionTypes.GET_ITEM_SUCCESS>(
    ActionTypes.GET_ITEM_SUCCESS,
  )({
    item,
  });

export const getItemFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_ITEM_FAILURE>(
    ActionTypes.GET_ITEM_FAILURE,
  )({
    error,
  });

// Remove Item
export const removeItemRequest = (id: number, branchId: number) =>
  createAction<ActionMap, ActionTypes.REMOVE_ITEM_REQUEST>(
    ActionTypes.REMOVE_ITEM_REQUEST,
  )({
    id,
    branchId,
  });

export const removeItemSuccess = (id: number) =>
  createAction<ActionMap, ActionTypes.REMOVE_ITEM_SUCCESS>(
    ActionTypes.REMOVE_ITEM_SUCCESS,
  )({
    id,
  });

export const removeItemFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.REMOVE_ITEM_FAILURE>(
    ActionTypes.REMOVE_ITEM_FAILURE,
  )({
    error,
  });

// Add items
export const addItemRequest = (item: StoreItem, branchId: number) =>
  createAction<ActionMap, ActionTypes.ADD_ITEM_REQUEST>(
    ActionTypes.ADD_ITEM_REQUEST,
  )({
    item,
    branchId,
  });

export const addItemSuccess = (item: StoreItem) =>
  createAction<ActionMap, ActionTypes.ADD_ITEM_SUCCESS>(
    ActionTypes.ADD_ITEM_SUCCESS,
  )({
    item,
  });

export const addItemFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.ADD_ITEM_FAILURE>(
    ActionTypes.ADD_ITEM_FAILURE,
  )({
    error,
  });

// Edit item
export const editItemRequest = (
  item: StoreItem,
  id: number,
  branchId: number,
) =>
  createAction<ActionMap, ActionTypes.EDIT_ITEM_REQUEST>(
    ActionTypes.EDIT_ITEM_REQUEST,
  )({
    item,
    id,
    branchId,
  });
export const editItemSuccess = (item: StoreItem) =>
  createAction<ActionMap, ActionTypes.EDIT_ITEM_SUCCESS>(
    ActionTypes.EDIT_ITEM_SUCCESS,
  )({
    item,
  });
export const editItemFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.EDIT_ITEM_FAILURE>(
    ActionTypes.EDIT_ITEM_FAILURE,
  )({
    error,
  });

// add image
export const addImageRequest = (
  branchId: number,
  itemId: number,
  image: File,
) =>
  createAction<ActionMap, ActionTypes.ADD_IMAGE_REQUEST>(
    ActionTypes.ADD_IMAGE_REQUEST,
  )({
    branchId,
    itemId,
    image,
  });

export const addImageSuccess = (image: StoreImage) =>
  createAction<ActionMap, ActionTypes.ADD_IMAGE_SUCCESS>(
    ActionTypes.ADD_IMAGE_SUCCESS,
  )({
    image,
  });

export const addImageFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.ADD_IMAGE_FAILURE>(
    ActionTypes.ADD_IMAGE_FAILURE,
  )({
    error,
  });

// remove image
export const removeImageRequest = (
  branchId: number,
  itemId: number,
  imageId: number,
) =>
  createAction<ActionMap, ActionTypes.REMOVE_IMAGE_REQUEST>(
    ActionTypes.REMOVE_IMAGE_REQUEST,
  )({
    branchId,
    itemId,
    imageId,
  });

export const removeImageSuccess = (imageId: number) =>
  createAction<ActionMap, ActionTypes.REMOVE_IMAGE_SUCCESS>(
    ActionTypes.REMOVE_IMAGE_SUCCESS,
  )({
    imageId,
  });

export const removeImageFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.REMOVE_IMAGE_FAILURE>(
    ActionTypes.REMOVE_IMAGE_FAILURE,
  )({
    error,
  });
