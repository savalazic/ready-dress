import { all, fork } from 'redux-saga/effects';

import storeBranchSaga from 'services/merchant/storeBranch/storeBranchSaga';
import storeItemSaga from 'services/merchant/storeItem/storeItemSaga';

export default function* merchantSagas() {
  yield all([fork(storeBranchSaga), fork(storeItemSaga)]);
}
