import mapKeys from 'lodash/mapKeys';
// import merge  from 'lodash/fp/merge';
import set from 'lodash/fp/set';
import { createReducer } from 'utils/redux';

import { ActionTypes, ActionMap } from './storeBranchActions';
import { StoreItem } from '../storeItem/storeItemReducer';

// const setPath = (
//   [key, ...next]: string[],
//   value: object,
//   obj: { [key: string]: any },
// ): any => {
//   console.log(key, next, value, obj);
//   if (next.length === 0) {
//     return { ...obj, [key]: value };
//   }
//   return { ...obj, [key]: setPath(next, value, obj[key]) };
// };

// const set = (path: string, value: object, obj: object) =>
//   setPath(path.split('.'), value, obj);

export interface StoreBranch {
  id: number;
  city: string;
  phone_number: string;
  state: string;
  street_name: string;
  street_number: string;
  appointments: { [id: number]: StoreBranchAppointment };
  working_hours: any[];
}

export interface Appointment {
  id: number;
  from: string;
  to: string;
  accepted_by_user_id: number;
}

export interface StoreBranchAppointment {
  appointment: Appointment | null;
  id: number;
  from: string;
  to: string;
  status: 'declined' | 'accepted' | 'pending_with_new_dates' | 'pending';
  items: StoreItem[];
}

export interface StoreBranchState {
  storeBranches: {
    [id: number]: StoreBranch;
  } | null;
  selectedStoreBranch: number | null;
}

const initialState: StoreBranchState = {
  storeBranches: null,
  selectedStoreBranch: null,
};

const storeBranchReducer = createReducer<StoreBranchState, ActionMap>(
  {
    [ActionTypes.GET_STORE_BRANCHES_SUCCESS]: (
      state,
      payload,
    ): StoreBranchState => ({
      ...state,
      storeBranches: mapKeys(payload.storeBranches, 'id'),
    }),
    [ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_SUCCESS]: (
      state,
      payload,
    ): StoreBranchState =>
      set(
        `storeBranches.${payload.branchId}.appointments`,
        mapKeys(payload.appointments, 'id'),
        state,
      ),
    [ActionTypes.SET_SELECTED_STORE_BRANCH]: (
      state,
      payload,
    ): StoreBranchState => ({
      ...state,
      selectedStoreBranch: payload.selectedStoreBranch,
    }),
  },
  initialState,
);

export default storeBranchReducer;
