import { createAction } from 'utils/redux';
import { StoreBranch, StoreBranchAppointment } from './storeBranchReducer';

export enum ActionTypes {
  GET_STORE_BRANCHES_REQUEST = 'MERCHANT/GET_STORE_BRANCHES_REQUEST',
  GET_STORE_BRANCHES_SUCCESS = 'MERCHANT/GET_STORE_BRANCHES_SUCCESS',
  GET_STORE_BRANCHES_FAILURE = 'MERCHANT/GET_STORE_BRANCHES_FAILURE',

  GET_STORE_BRANCH_APPOINTMENTS_REQUEST = 'MERCHANT/GET_STORE_BRANCH_APPOINTMENTS_REQUEST',
  GET_STORE_BRANCH_APPOINTMENTS_SUCCESS = 'MERCHANT/GET_STORE_BRANCH_APPOINTMENTS_SUCCESS',
  GET_STORE_BRANCH_APPOINTMENTS_FAILURE = 'MERCHANT/GET_STORE_BRANCH_APPOINTMENTS_FAILURE',

  SET_SELECTED_STORE_BRANCH = 'MERCHANG/SET_SELECTED_STORE_BRANCH',

  ACCEPT_APPOINTMENT_REQUEST = 'MERCHANT/ACCEPT_APPOINTMENT_REQUEST',
  ACCEPT_APPOINTMENT_SUCCESS = 'MERCHANT/ACCEPT_APPOINTMENT_SUCCESS',
  ACCEPT_APPOINTMENT_FAILURE = 'MERCHANT/ACCEPT_APPOINTMENT_FAILURE',

  DECLINE_APPOINTMENT_REQUEST = 'MERCHANT/DECLINE_APPOINTMENT_REQUEST',
  DECLINE_APPOINTMENT_SUCCESS = 'MERCHANT/DECLINE_APPOINTMENT_SUCCESS',
  DECLINE_APPOINTMENT_FAILURE = 'MERCHANT/DECLINE_APPOINTMENT_FAILURE',

  APPOINTMENT_NEW_TIME_REQUEST = 'MERCHANT/APPOINTMENT_NEW_TIME_REQUEST',
  APPOINTMENT_NEW_TIME_SUCCESS = 'MERCHANT/APPOINTMENT_NEW_TIME_SUCCESS',
  APPOINTMENT_NEW_TIME_FAILURE = 'MERCHANT/APPOINTMENT_NEW_TIME_FAILURE',
}

export interface ActionMap {
  [ActionTypes.GET_STORE_BRANCHES_REQUEST]: {};
  [ActionTypes.GET_STORE_BRANCHES_SUCCESS]: {
    storeBranches: StoreBranch[];
  };
  [ActionTypes.GET_STORE_BRANCHES_FAILURE]: any;
  [ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_REQUEST]: { branchId: number };
  [ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_SUCCESS]: {
    appointments: StoreBranchAppointment[];
    branchId: number;
  };
  [ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_FAILURE]: any;
  [ActionTypes.SET_SELECTED_STORE_BRANCH]: { selectedStoreBranch: number };
  [ActionTypes.ACCEPT_APPOINTMENT_REQUEST]: {
    branchId: number;
    appointmentId: number;
  };
  [ActionTypes.ACCEPT_APPOINTMENT_SUCCESS]: any;
  [ActionTypes.ACCEPT_APPOINTMENT_FAILURE]: any;
  [ActionTypes.DECLINE_APPOINTMENT_REQUEST]: {
    branchId: number;
    appointmentId: number;
    comment: string;
  };
  [ActionTypes.DECLINE_APPOINTMENT_SUCCESS]: any;
  [ActionTypes.DECLINE_APPOINTMENT_FAILURE]: any;
  [ActionTypes.APPOINTMENT_NEW_TIME_REQUEST]: {
    branchId: number;
    appointmentId: number;
    comment: string;
    from: Date;
    to: Date;
  };
  [ActionTypes.APPOINTMENT_NEW_TIME_SUCCESS]: any;
  [ActionTypes.APPOINTMENT_NEW_TIME_FAILURE]: any;
}

// get store branches
export const getStoreBranchesRequest = createAction<
  ActionMap,
  ActionTypes.GET_STORE_BRANCHES_REQUEST
>(ActionTypes.GET_STORE_BRANCHES_REQUEST);

export const getStoreBranchesSuccess = (storeBranches: StoreBranch[]) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCHES_SUCCESS>(
    ActionTypes.GET_STORE_BRANCHES_SUCCESS,
  )({
    storeBranches,
  });

export const getStoreBranchesFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCHES_FAILURE>(
    ActionTypes.GET_STORE_BRANCHES_FAILURE,
  )({
    error,
  });

// get appointments for store branch
export const getAppointmentsForStoreBranchRequest = (branchId: number) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_REQUEST>(
    ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_REQUEST,
  )({ branchId });

export const getAppointmentsForStoreBranchSuccess = (
  appointments: StoreBranchAppointment[],
  branchId: number,
) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_SUCCESS>(
    ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_SUCCESS,
  )({ appointments, branchId });

export const getAppointmentsForStoreBranchFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_FAILURE>(
    ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_FAILURE,
  )({ error });

// set selected store branch
export const setSelectedStoreBranch = (selectedStoreBranch: number) =>
  createAction<ActionMap, ActionTypes.SET_SELECTED_STORE_BRANCH>(
    ActionTypes.SET_SELECTED_STORE_BRANCH,
  )({ selectedStoreBranch });

// accept appointment
export const acceptAppointmentRequest = (
  branchId: number,
  appointmentId: number,
) =>
  createAction<ActionMap, ActionTypes.ACCEPT_APPOINTMENT_REQUEST>(
    ActionTypes.ACCEPT_APPOINTMENT_REQUEST,
  )({ branchId, appointmentId });

export const acceptAppointmentSuccess = () =>
  createAction<ActionMap, ActionTypes.ACCEPT_APPOINTMENT_SUCCESS>(
    ActionTypes.ACCEPT_APPOINTMENT_SUCCESS,
  )({});

export const acceptAppointmentFailure = () =>
  createAction<ActionMap, ActionTypes.ACCEPT_APPOINTMENT_FAILURE>(
    ActionTypes.ACCEPT_APPOINTMENT_FAILURE,
  )({});

// decline appointment
export const declineAppointmentRequest = (
  branchId: number,
  appointmentId: number,
  comment: string,
) =>
  createAction<ActionMap, ActionTypes.DECLINE_APPOINTMENT_REQUEST>(
    ActionTypes.DECLINE_APPOINTMENT_REQUEST,
  )({ branchId, appointmentId, comment });

export const declineAppointmentSuccess = () =>
  createAction<ActionMap, ActionTypes.DECLINE_APPOINTMENT_SUCCESS>(
    ActionTypes.DECLINE_APPOINTMENT_SUCCESS,
  )({});

export const decclineAppointmentFailure = () =>
  createAction<ActionMap, ActionTypes.DECLINE_APPOINTMENT_FAILURE>(
    ActionTypes.DECLINE_APPOINTMENT_FAILURE,
  )({});

// suggest new time
export const suggestNewTimeRequest = (params: {
  branchId: number;
  appointmentId: number;
  comment: string;
  from: Date;
  to: Date;
}) =>
  createAction<ActionMap, ActionTypes.APPOINTMENT_NEW_TIME_REQUEST>(
    ActionTypes.APPOINTMENT_NEW_TIME_REQUEST,
  )(params);

export const suggestNewTimeSuccess = () =>
  createAction<ActionMap, ActionTypes.APPOINTMENT_NEW_TIME_SUCCESS>(
    ActionTypes.APPOINTMENT_NEW_TIME_SUCCESS,
  )({});

export const suggestNewTimeFailure = () =>
  createAction<ActionMap, ActionTypes.APPOINTMENT_NEW_TIME_FAILURE>(
    ActionTypes.APPOINTMENT_NEW_TIME_FAILURE,
  )({});
