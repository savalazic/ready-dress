import { takeEvery, call, put } from 'redux-saga/effects';
import {
  ActionTypes,
  ActionMap,
  getStoreBranchesSuccess,
  getStoreBranchesFailure,
  getAppointmentsForStoreBranchSuccess,
  getAppointmentsForStoreBranchFailure,
  acceptAppointmentSuccess,
  acceptAppointmentFailure,
  declineAppointmentSuccess,
  decclineAppointmentFailure,
  getAppointmentsForStoreBranchRequest,
  suggestNewTimeSuccess,
  suggestNewTimeFailure,
} from './storeBranchActions';
import storeBranchApi from './storeBranchApi';
import { ActionObject } from 'utils/redux';

function* getStoreBranches() {
  const { response, error } = yield call(storeBranchApi.getStoreBranches);

  if (response) {
    yield put(getStoreBranchesSuccess(response.data));
  } else {
    yield put(getStoreBranchesFailure(error.response));
  }
}

function* getAppointmentsForStoreBranch({
  payload: { branchId },
}: ActionObject<ActionMap, ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_REQUEST>) {
  const { response, error } = yield call(
    storeBranchApi.getAppointmentsForStoreBranch,
    branchId,
  );

  if (response) {
    yield put(getAppointmentsForStoreBranchSuccess(response.data, branchId));
  } else {
    yield put(getAppointmentsForStoreBranchFailure(error.response));
  }
}

function* acceptAppointmentRequest({
  payload: { appointmentId, branchId },
}: ActionObject<ActionMap, ActionTypes.ACCEPT_APPOINTMENT_REQUEST>) {
  const { response } = yield call(
    storeBranchApi.acceptAppointmentForStoreBranch,
    branchId,
    appointmentId,
  );

  if (response) {
    yield put(acceptAppointmentSuccess());
    yield put(getAppointmentsForStoreBranchRequest(branchId));
  } else {
    yield put(acceptAppointmentFailure());
  }
}

function* declineAppointmentRequest({
  payload: { appointmentId, branchId, comment },
}: ActionObject<ActionMap, ActionTypes.DECLINE_APPOINTMENT_REQUEST>) {
  const { response } = yield call(
    storeBranchApi.declineAppointmentForStoreBranch,
    branchId,
    appointmentId,
    comment,
  );

  if (response) {
    yield put(declineAppointmentSuccess());
    yield put(getAppointmentsForStoreBranchRequest(branchId));
  } else {
    yield put(decclineAppointmentFailure());
  }
}

function* suggestNewTime({
  payload: { comment, branchId, appointmentId, from, to },
}: ActionObject<ActionMap, ActionTypes.APPOINTMENT_NEW_TIME_REQUEST>) {
  const { response } = yield call(
    storeBranchApi.suggestNewTime,
    branchId,
    appointmentId,
    comment,
    from,
    to,
  );

  if (response) {
    yield put(suggestNewTimeSuccess());
    yield put(getAppointmentsForStoreBranchRequest(branchId));
  } else {
    yield put(suggestNewTimeFailure());
  }
}

export default function* storeBranchSaga() {
  yield takeEvery(ActionTypes.GET_STORE_BRANCHES_REQUEST, getStoreBranches);
  yield takeEvery(
    ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_REQUEST,
    getAppointmentsForStoreBranch,
  );
  yield takeEvery(
    ActionTypes.ACCEPT_APPOINTMENT_REQUEST,
    acceptAppointmentRequest,
  );
  yield takeEvery(
    ActionTypes.DECLINE_APPOINTMENT_REQUEST,
    declineAppointmentRequest,
  );
  yield takeEvery(ActionTypes.APPOINTMENT_NEW_TIME_REQUEST, suggestNewTime);
}
