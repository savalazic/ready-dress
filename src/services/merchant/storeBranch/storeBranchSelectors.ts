import { createLoadingSelector } from 'utils/redux';
import { ActionTypes } from './storeBranchActions';
import { State } from 'services/state';
import { createSelector } from 'reselect';
import filter from 'lodash/filter';
import sortBy from 'lodash/sortBy';

export const getStoreBranchesSelector = (state: State) =>
  state.services.merchant.storeBranch.storeBranches;

export const getStoreBranchesLoadingSelector = createLoadingSelector(
  ActionTypes.GET_STORE_BRANCHES_REQUEST,
);

export const getSelectedStoreBranchSelector = (state: State) =>
  state.services.merchant.storeBranch.selectedStoreBranch;

export const getAllAppointmentsForStoreBranchSelector = createSelector(
  getStoreBranchesSelector,
  getSelectedStoreBranchSelector,
  (storeBranches, selectedStoreBranch) => {
    if (selectedStoreBranch === null || storeBranches === null) {
      return [];
    }
    if (
      !storeBranches[selectedStoreBranch] ||
      !storeBranches[selectedStoreBranch].appointments
    ) {
      return [];
    }
    return storeBranches[selectedStoreBranch].appointments;
  },
);

export const getSortedAppointmentRequestsSelector = createSelector(
  getAllAppointmentsForStoreBranchSelector,
  appointmentRequests => sortBy(appointmentRequests, ['from', 'to']),
);

export const getAppointmentsSelector = createSelector(
  getSortedAppointmentRequestsSelector,
  appointmentRequests => filter(appointmentRequests, { status: 'accepted' }),
);

export const getFutureAppointments = createSelector(
  getAppointmentsSelector,
  appointments =>
    filter(
      appointments,
      ({ from }) => new Date().getTime() < new Date(from).getTime(),
    ),
);

export const getPastAppointments = createSelector(
  getAppointmentsSelector,
  appointments =>
    filter(
      appointments,
      ({ from }) => new Date().getTime() > new Date(from).getTime(),
    ),
);

export const getTodaysAppointments = createSelector(
  getAppointmentsSelector,
  appointments =>
    filter(
      appointments,
      ({ from }) => new Date().toDateString() === new Date(from).toDateString(),
    ),
);

// export const getFutureAppointmentsSelector = createSelector

export const getAppointmentRequests = createSelector(
  getSortedAppointmentRequestsSelector,
  appointments =>
    filter(appointments, ({ status }) =>
      ['pending', 'pending_with_new_dates'].includes(status),
    ),
);

export const isAppointmentRequestForStoreBranchLoading = createSelector(
  createLoadingSelector(ActionTypes.ACCEPT_APPOINTMENT_REQUEST),
  createLoadingSelector(ActionTypes.DECLINE_APPOINTMENT_REQUEST),
  createLoadingSelector(ActionTypes.APPOINTMENT_NEW_TIME_REQUEST),
  (accept, decline, newTime) => accept || decline || newTime,
);

export const getAppointmentsForStoreBranchLoadingSelector = createLoadingSelector(
  ActionTypes.GET_STORE_BRANCH_APPOINTMENTS_REQUEST,
);
