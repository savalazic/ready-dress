import moment from 'moment';
import api from 'services/api';
import { AxiosResponse, AxiosError } from 'axios';

const normalizeResponse = ({ data }: AxiosResponse) => ({ response: data });
const normalizeError = (error: AxiosError) => ({ error });

type ApiResponse = Promise<{ response?: any; error?: any }>;

const getStoreBranches = () =>
  api
    .get('merchant/store_branches')
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const getAppointmentsForStoreBranch = (branchId: number) =>
  api
    .get(`merchant/store_branches/${branchId}/appointment_requests`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const acceptAppointmentForStoreBranch = (
  branchId: number,
  appointmentId: number,
): ApiResponse =>
  api
    .put(
      `merchant/store_branches/${branchId}/appointment_requests/${appointmentId}/accept`,
      {},
    )
    .then(normalizeResponse)
    .catch(normalizeError);

const declineAppointmentForStoreBranch = (
  branchId: number,
  appointmentId: number,
  comment: string,
): ApiResponse =>
  api
    .put(
      `merchant/store_branches/${branchId}/appointment_requests/${appointmentId}/decline`,
      { comment },
    )
    .then(normalizeResponse)
    .catch(normalizeError);

const suggestNewTime = (
  branchId: number,
  appointmentId: number,
  comment: string,
  from: Date,
  to: Date,
): ApiResponse =>
  api
    .put(
      `merchant/store_branches/${branchId}/appointment_requests/${appointmentId}/new_time_suggestion`,
      {
        comment,
        appointment_request: {
          from: moment(from).format(),
          to: moment(to).format(),
        },
      },
    )
    .then(normalizeResponse)
    .catch(normalizeError);

export default {
  getStoreBranches,
  getAppointmentsForStoreBranch,
  acceptAppointmentForStoreBranch,
  declineAppointmentForStoreBranch,
  suggestNewTime,
};
