import { all, fork } from 'redux-saga/effects';

import errorSaga from 'services/util/errorSaga';
import authSaga from 'services/auth/authSaga';
import adminSaga from 'services/admin/adminSaga';
import merchantSaga from 'services/merchant/merchantSaga';
import storeSaga from 'services/store/storeSaga';
import appointmentSaga from 'services/appointment/appointmentSaga';
import paymentSaga from 'services/payment/paymentSaga';

export default function* servicesSagas() {
  yield all([
    fork(authSaga),
    fork(errorSaga),
    fork(adminSaga),
    fork(merchantSaga),
    fork(storeSaga),
    fork(appointmentSaga),
    fork(paymentSaga),
  ]);
}
