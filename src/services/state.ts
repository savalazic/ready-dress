import { NotificationState } from './notification/notificationReducer';
import { AdminState } from './admin/adminReducer';
import { AuthState } from './auth/authReducer';
import { UtilState } from './util/utilReducer';
import { MerchantState } from './merchant/merchantReducer';
import { CartState } from './cart/cartReducer';
import { StoreState } from './store/storeReducer';
import { AppointmentState } from './appointment/appointmentReducer';
import { PaymentState } from './payment/paymentReducer';

export interface State {
  services: {
    notification: NotificationState;
    util: UtilState;
    auth: AuthState;
    admin: AdminState;
    merchant: MerchantState;
    cart: CartState;
    store: StoreState;
    appointment: AppointmentState;
    payment: PaymentState;
  };
}
