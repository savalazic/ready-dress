import get from 'lodash/get';
import replace from 'lodash/replace';
import { State } from 'services/state';

export const getLoaderStatus = (loaders: any, action: any) =>
  get(loaders, replace(action, '_REQUEST', ''));
export const getAllLoadersFromState = (state: State) =>
  state.services.util.loaders;
export const getAllErrorsFromState = (state: State) =>
  state.services.util.errors;
export const getErrorStatus = (errors: any, action: any) =>
  get(errors, replace(action, '_FAILURE', ''));
