import endsWith from 'lodash/endsWith';
import isPlainObject from 'lodash/isPlainObject';
import { take, put } from 'redux-saga/effects';
import { showNotification } from 'services/notification/notificationActions';

const DEFAULT_ERROR_TOAST_DURATION = 5000;

export default function* watchErrorActions() {
  while (true) {
    const {
      payload: { error },
    } = yield take((action: any) => endsWith(action.type, '_FAILURE'));

    if (!error) {
      console.log('An error occured');
      return;
    }

    console.log(error); // @NOTE: keep this to debugg all cases

    if (error.data && error.status < 500) {
      const errors = error.data;

      console.log(errors); // @NOTE: keep this to debugg all cases

      for (const key in errors) {
        if (errors.hasOwnProperty(key)) {
          const errorMessage = errors[key];

          if (errorMessage instanceof Array) {
            for (const message of errorMessage) {
              yield put(
                showNotification(
                  message,
                  DEFAULT_ERROR_TOAST_DURATION,
                  'error',
                ),
              );
            }
          } else if (isPlainObject(errorMessage)) {
            for (const i in errorMessage) {
              const err = errorMessage[i];

              for (const msg in err) {
                yield put(
                  showNotification(
                    err[msg],
                    DEFAULT_ERROR_TOAST_DURATION,
                    'error',
                  ),
                );
              }
            }
          } else {
            yield put(
              showNotification(
                errorMessage,
                DEFAULT_ERROR_TOAST_DURATION,
                'error',
              ),
            );
          }
        }
      }
    } else if (error.status >= 500) {
      yield put(
        showNotification(
          error.statusText || 'Something wrong happened',
          DEFAULT_ERROR_TOAST_DURATION,
          'error',
        ),
      );
    }
  }
}
