export const saveLoadingFlag = (actionName: any) => {
  const matches = /(.*)_(REQUEST|SUCCESS|FAILURE)/.exec(actionName);
  if (matches) {
    const [, requestName, requestState] = matches;
    return {
      [requestName]: requestState === 'REQUEST',
    };
  }
  return null;
};

export const saveError = (action: any) => {
  const matches = /(.*)_(REQUEST|FAILURE)/.exec(action.type);
  if (matches) {
    const [, requestName, requestState] = matches;
    if (requestState === 'FAILURE') {
      return {
        [requestName]: action.payload.message,
      };
    }
    // @ts-ignore
    delete [requestName];
  }
  return null;
};

export interface UtilState {
  loaders: object;
  errors: object;
}

export const initialState: UtilState = {
  loaders: {},
  errors: {},
};

const utilsReducer = (state: UtilState = initialState, action: any) => {
  switch (action.type) {
    default:
      return {
        ...state,
        loaders: {
          ...state.loaders,
          ...saveLoadingFlag(action.type),
        },
        errors: {
          ...state.errors,
          ...saveError(action),
        },
      };
  }
};

export default utilsReducer;
