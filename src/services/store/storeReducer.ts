import uniq from 'lodash/uniq';
import mapKeys from 'lodash/mapKeys';
import { createReducer } from 'utils/redux';
import { ActionTypes, ActionMap } from './storeActions';
import { omit } from 'utils/object';

export interface GridBranchItem {
  images: Array<{ image_url: string }>;
  item_id: number;
}
export interface GridItem {
  items: GridBranchItem[];
  store_branch_id: number;
}

export interface Store {
  id: number;
  city?: string;
  phone_number?: string;
  state?: string;
  store_name: string;
  street_name?: string;
  street_number?: string;
  image_url: string;
}

export interface StoreMap {
  [storeId: number]: Store;
}

interface StoreItemImage {
  id: number;
  url: string;
}

export interface StoreItem {
  id: number;
  price_in_centum: number;
  title: string;
  description: string;
  images: any[];
  color_samples: StoreItemImage[];
}

type Order = 'asc' | 'desc';

export interface StoreParams {
  searchQuery?: string;
  lng?: number | string;
  lat?: number | string;
  radius?: number;
  order?: Order;
  page?: number;
  per_page?: number;
}

export interface Pagination {
  has_next: boolean;
  has_prev: boolean;
  next_page: number;
  page: number;
  prev_page: number;
  total_pages: number;
}

export interface StoreState {
  stores: {
    byIds: StoreMap;
    ids: number[];
    pagination?: Pagination | {};
  };
  storeItems: {
    byIds: {
      [id: number]: StoreItem;
    };
    ids: number[];
    pagination?: Pagination | {};
  };
  gridItems: GridItem[];
}

const initialState: StoreState = {
  stores: {
    byIds: {},
    ids: [],
    pagination: {},
  },
  storeItems: {
    byIds: {},
    ids: [],
    pagination: {},
  },
  gridItems: [],
};

const storeReducer = createReducer<StoreState, ActionMap>(
  {
    [ActionTypes.GET_STORES_SUCCESS]: (state, payload): StoreState => ({
      ...state,
      stores: {
        byIds: payload.isPagination
          ? Object.assign(
              {},
              state.stores.byIds,
              mapKeys(payload.stores.data, 'id'),
            )
          : mapKeys(payload.stores.data, 'id'),
        ids: payload.isPagination
          ? uniq([
              ...state.stores.ids,
              ...payload.stores.data.map(store => store.id),
            ])
          : payload.stores.data.map(store => store.id),
        pagination: omit(payload.stores, ['data']),
      },
    }),

    [ActionTypes.GET_STORE_ITEMS_SUCCESS]: (state, payload): StoreState => ({
      ...state,
      storeItems: {
        byIds: payload.isPagination
          ? Object.assign(
              {},
              state.storeItems.byIds,
              mapKeys(payload.storeItems.data, 'id'),
            )
          : mapKeys(payload.storeItems.data, 'id'),
        ids: payload.isPagination
          ? uniq([
              ...state.storeItems.ids,
              ...payload.storeItems.data.map(storeItem => storeItem.id),
            ])
          : payload.storeItems.data.map(storeItem => storeItem.id),
        pagination: omit(payload.storeItems, ['data']),
      },
    }),

    [ActionTypes.GET_STORE_ITEM_SUCCESS]: (state, payload): StoreState => ({
      ...state,
      storeItems: {
        byIds: Object.assign({}, state.storeItems.byIds, {
          [payload.storeItem.id]: payload.storeItem,
        }),
        ids: [...state.storeItems.ids, payload.storeItem.id],
      },
    }),

    [ActionTypes.GET_STORE_SUCCESS]: (state, payload): StoreState => ({
      ...state,
      stores: {
        byIds: Object.assign({}, state.stores.byIds, {
          [payload.store.id]: payload.store,
        }),
        ids: uniq([...state.stores.ids, payload.store.id]),
        pagination: {},
      },
    }),

    [ActionTypes.GET_GRID_SUCCESS]: (state, payload): StoreState => ({
      ...state,
      gridItems: payload.gridItems,
    }),
  },
  initialState,
);

export default storeReducer;
