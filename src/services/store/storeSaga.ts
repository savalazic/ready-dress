import { takeEvery, call, put } from 'redux-saga/effects';
import {
  ActionTypes,
  ActionMap,
  getStoresSuccess,
  getStoresFailure,
  getStoreItemsSuccess,
  getStoreItemsFailure,
  getStoreItemFailure,
  getStoreItemSuccess,
  getStoreSuccess,
  getStoreFailure,
  getGridSuccess,
  getGridFailure,
} from './storeActions';
import storeApi from './storeApi';
import { ActionObject } from 'utils/redux';

function* getStores({
  payload: { params, isPagination },
}: ActionObject<ActionMap, ActionTypes.GET_STORES_REQUEST>) {
  const { response, error } = yield call(storeApi.getStores, params);

  if (response) {
    yield put(getStoresSuccess(response, isPagination));
  } else {
    yield put(getStoresFailure(error.response));
  }
}
// Get Items
function* getStoreItems({
  payload: { storeId, isPagination },
}: ActionObject<ActionMap, ActionTypes.GET_STORE_ITEMS_REQUEST>) {
  const { response, error } = yield call(
    storeApi.getItemsForStoreBranch,
    storeId,
  );

  if (response) {
    yield put(getStoreItemsSuccess(response, isPagination));
  } else {
    yield put(getStoreItemsFailure(error.response));
  }
}

// get single store item
function* getStoreItem({
  payload: { storeId, productId },
}: ActionObject<ActionMap, ActionTypes.GET_STORE_ITEM_REQUEST>) {
  const { response, error } = yield call(
    storeApi.getSingleItem,
    storeId,
    productId,
  );

  if (response) {
    yield put(getStoreItemSuccess(response.data));
  } else {
    yield put(getStoreItemFailure(error.response));
  }
}

// get single store
function* getStore({
  payload: { storeId },
}: ActionObject<ActionMap, ActionTypes.GET_STORE_REQUEST>) {
  const { response, error } = yield call(storeApi.getSingleStore, storeId);

  if (response) {
    yield put(getStoreSuccess(response.data));
  } else {
    yield put(getStoreFailure(error.response));
  }
}

// get grid
function* getGrid() {
  const { response, error } = yield call(storeApi.getGridData);

  if (response) {
    yield put(getGridSuccess(response.data));
  } else {
    yield put(getGridFailure(error.response));
  }
}

export default function* storeSaga() {
  yield takeEvery(ActionTypes.GET_STORES_REQUEST, getStores);
  yield takeEvery(ActionTypes.GET_STORE_ITEMS_REQUEST, getStoreItems);
  yield takeEvery(ActionTypes.GET_STORE_ITEM_REQUEST, getStoreItem);
  yield takeEvery(ActionTypes.GET_STORE_REQUEST, getStore);
  yield takeEvery(ActionTypes.GET_GRID_REQUEST, getGrid);
}
