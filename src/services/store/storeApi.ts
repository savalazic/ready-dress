import api from 'services/api';
import { StoreParams } from './storeReducer';

const getStores = ({
  searchQuery = '',
  lng = '',
  lat = '',
  radius,
  order = 'asc',
  page,
  per_page,
}: StoreParams) => {
  const params: any = {};

  if (searchQuery) {
    params['store[name_contains]'] = searchQuery;
  }
  if (lng && lat) {
    params.location = [lng, lat];
  }
  if (radius) {
    params.radius = radius.toFixed(1);
  }
  if (order) {
    params.order = order;
  }
  if (page) {
    params.page = page;
  }
  if (per_page) {
    params.per_page = per_page;
  }

  return api
    .get('store_branches', params)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));
};

const getItemsForStoreBranch = (storeId: number) => {
  return api
    .get(`store_branches/${storeId}/items`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));
};

const getSingleItem = (storeId: number, productId: number) => {
  return api
    .get(`store_branches/${storeId}/items/${productId}`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));
};

const getSingleStore = (storeId: number) => {
  return api
    .get(`store_branches/${storeId}`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));
};

const getGridData = () =>
  api
    .get('grid-data')
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

export default {
  getStores,
  getItemsForStoreBranch,
  getSingleItem,
  getSingleStore,
  getGridData,
};
