import { createSelector } from 'reselect';
import { createLoadingSelector } from 'utils/redux';
import { ActionTypes } from './storeActions';
import { State } from 'services/state';

export const getGridItemsSelector = (state: State) =>
  state.services.store.gridItems;

export const getGridItems = createSelector(
  [getGridItemsSelector],
  items => {
    return items
      .map(i => [
        ...i.items.map(x => ({ ...x, store_branch_id: i.store_branch_id })),
      ])
      .flat()
      .map(item => ({
        ...item,
        images: item.images.slice(0, 3).map(i => i.image_url),
      }))
      .slice(0, 12);
  },
);

export const getStoresByIdSelector = (state: State) =>
  state.services.store.stores.byIds;

export const getStoresIdsSelector = (state: State) =>
  state.services.store.stores.ids;

export const getStoresPaginationSelector = (state: State) =>
  state.services.store.stores.pagination;

export const getStoresSelector = createSelector(
  [getStoresByIdSelector, getStoresIdsSelector],
  (byId, ids) => ids.map(o => byId[o]),
);

// Items selectors
export const getStoreItemsByIdSelector = (state: State) =>
  state.services.store.storeItems.byIds;

export const getStoreItemsIdsSelector = (state: State) =>
  state.services.store.storeItems.ids;

export const getStoreItemsPaginationSelector = (state: State) =>
  state.services.store.storeItems.pagination;

export const getStoreItemsSelector = createSelector(
  [getStoreItemsByIdSelector, getStoreItemsIdsSelector],
  (byId, ids) => ids.map(o => byId[o]),
);

export const getStoresLoadingSelector = createLoadingSelector(
  ActionTypes.GET_STORES_REQUEST,
);

export const getStoreItemsLoadingSelector = createLoadingSelector(
  ActionTypes.GET_STORE_ITEMS_REQUEST,
);

export const getStoreItemLoadingSelector = createLoadingSelector(
  ActionTypes.GET_STORE_ITEM_REQUEST,
);

export const getGridLoadingSelector = createLoadingSelector(
  ActionTypes.GET_GRID_REQUEST,
);
