import { createAction } from 'utils/redux';
import { Store, StoreParams, StoreItem, GridItem } from './storeReducer';

export enum ActionTypes {
  GET_STORES_REQUEST = 'CUSTOMER/GET_STORES_REQUEST',
  GET_STORES_SUCCESS = 'CUSTOMER/GET_STORES_SUCCESS',
  GET_STORES_FAILURE = 'CUSTOMER/GET_STORES_FAILURE',

  GET_STORE_REQUEST = 'CUSTOMER/GET_STORE_REQUEST',
  GET_STORE_SUCCESS = 'CUSTOMER/GET_STORE_SUCCESS',
  GET_STORE_FAILURE = 'CUSTOMER/GET_STORE_FAILURE',

  GET_STORE_ITEMS_REQUEST = 'CUSTOMER/GET_STORE_ITEMS_REQUEST',
  GET_STORE_ITEMS_SUCCESS = 'CUSTOMER/GET_STORE_ITEMS_SUCCESS',
  GET_STORE_ITEMS_FAILURE = 'CUSTOMER/GET_STORE_ITEMS_FAILURE',

  GET_STORE_ITEM_REQUEST = 'CUSTOMER/GET_STORE_ITEM_REQUEST',
  GET_STORE_ITEM_SUCCESS = 'CUSTOMER/GET_STORE_ITEM_SUCCESS',
  GET_STORE_ITEM_FAILURE = 'CUSTOMER/GET_STORE_ITEM_FAILURE',

  GET_GRID_REQUEST = 'GET_GRID_REQUEST',
  GET_GRID_SUCCESS = 'GET_GRID_SUCCESS',
  GET_GRID_FAILURE = 'GET_GRID_FAILURE',
}

export interface ActionMap {
  [ActionTypes.GET_STORES_REQUEST]: {
    params: StoreParams;
    isPagination: boolean;
  };
  [ActionTypes.GET_STORES_SUCCESS]: {
    stores: { data: Store[] };
    isPagination: boolean;
  };
  [ActionTypes.GET_STORES_FAILURE]: any;

  [ActionTypes.GET_STORE_REQUEST]: { storeId: number };
  [ActionTypes.GET_STORE_SUCCESS]: { store: Store };
  [ActionTypes.GET_STORE_FAILURE]: any;

  // Get Store Items
  [ActionTypes.GET_STORE_ITEMS_REQUEST]: {
    storeId: number;
    isPagination: boolean;
  };

  [ActionTypes.GET_STORE_ITEMS_SUCCESS]: {
    storeItems: { data: StoreItem[] };
    isPagination: boolean;
  };
  [ActionTypes.GET_STORE_ITEMS_FAILURE]: any;

  // Get Store single
  [ActionTypes.GET_STORE_ITEM_REQUEST]: {
    storeId: number;
    productId: number;
  };
  [ActionTypes.GET_STORE_ITEM_SUCCESS]: {
    storeItem: StoreItem;
  };
  [ActionTypes.GET_STORE_ITEM_FAILURE]: any;

  // grid grid
  [ActionTypes.GET_GRID_REQUEST]: any;
  [ActionTypes.GET_GRID_SUCCESS]: {
    gridItems: GridItem[];
  };
  [ActionTypes.GET_GRID_FAILURE]: any;
}

export const getStoresRequest = (
  params: StoreParams = {},
  isPagination: boolean = false,
) =>
  createAction<ActionMap, ActionTypes.GET_STORES_REQUEST>(
    ActionTypes.GET_STORES_REQUEST,
  )({
    params,
    isPagination,
  });

export const getStoresSuccess = (
  stores: { data: Store[] },
  isPagination: boolean = false,
) =>
  createAction<ActionMap, ActionTypes.GET_STORES_SUCCESS>(
    ActionTypes.GET_STORES_SUCCESS,
  )({
    stores,
    isPagination,
  });

export const getStoresFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_STORES_FAILURE>(
    ActionTypes.GET_STORES_FAILURE,
  )({
    error,
  });

// get single store
export const getStoreRequest = (storeId: number) =>
  createAction<ActionMap, ActionTypes.GET_STORE_REQUEST>(
    ActionTypes.GET_STORE_REQUEST,
  )({
    storeId,
  });

export const getStoreSuccess = (store: Store) =>
  createAction<ActionMap, ActionTypes.GET_STORE_SUCCESS>(
    ActionTypes.GET_STORE_SUCCESS,
  )({
    store,
  });

export const getStoreFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_STORE_FAILURE>(
    ActionTypes.GET_STORE_FAILURE,
  )({
    error,
  });

// Get Store Items
export const getStoreItemsRequest = (storeId: number, isPagination: boolean) =>
  createAction<ActionMap, ActionTypes.GET_STORE_ITEMS_REQUEST>(
    ActionTypes.GET_STORE_ITEMS_REQUEST,
  )({
    storeId,
    isPagination,
  });

export const getStoreItemsSuccess = (
  storeItems: { data: StoreItem[] },
  isPagination: boolean,
) =>
  createAction<ActionMap, ActionTypes.GET_STORE_ITEMS_SUCCESS>(
    ActionTypes.GET_STORE_ITEMS_SUCCESS,
  )({
    storeItems,
    isPagination,
  });

export const getStoreItemsFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_STORE_ITEMS_FAILURE>(
    ActionTypes.GET_STORE_ITEMS_FAILURE,
  )({
    error,
  });

// get Single
export const getStoreItemRequest = (storeId: number, productId: number) =>
  createAction<ActionMap, ActionTypes.GET_STORE_ITEM_REQUEST>(
    ActionTypes.GET_STORE_ITEM_REQUEST,
  )({
    storeId,
    productId,
  });

export const getStoreItemSuccess = (storeItem: StoreItem) =>
  createAction<ActionMap, ActionTypes.GET_STORE_ITEM_SUCCESS>(
    ActionTypes.GET_STORE_ITEM_SUCCESS,
  )({
    storeItem,
  });

export const getStoreItemFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_STORE_ITEM_FAILURE>(
    ActionTypes.GET_STORE_ITEM_FAILURE,
  )({
    error,
  });

// get grid
export const getGridRequest = createAction<
  ActionMap,
  ActionTypes.GET_GRID_REQUEST
>(ActionTypes.GET_GRID_REQUEST);

export const getGridSuccess = (gridItems: GridItem[]) =>
  createAction<ActionMap, ActionTypes.GET_GRID_SUCCESS>(
    ActionTypes.GET_GRID_SUCCESS,
  )({
    gridItems,
  });

export const getGridFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_GRID_FAILURE>(
    ActionTypes.GET_GRID_FAILURE,
  )({
    error,
  });
