import { createAction } from 'utils/redux';

export enum ActionTypes {
  SHOW_NOTIFICATION = 'SHOW_NOTIFICATION',
  HIDE_NOTIFICATION = 'HIDE_NOTIFICATION',
  HIDE_ALL_NOTIFICATIONS = 'HIDE_ALL_NOTIFICATIONS',
}

export type NotificationStyleType = 'success' | 'error' | 'warning';

export interface ActionMap {
  [ActionTypes.SHOW_NOTIFICATION]: {
    message: string;
    duration: number;
    style: NotificationStyleType;
  };
  [ActionTypes.HIDE_NOTIFICATION]: {
    id: number;
  };
  [ActionTypes.HIDE_ALL_NOTIFICATIONS]: {};
}

export const showNotification = (
  message: string,
  duration: number,
  style: NotificationStyleType,
) =>
  createAction<ActionMap, ActionTypes.SHOW_NOTIFICATION>(
    ActionTypes.SHOW_NOTIFICATION,
  )({
    message,
    duration,
    style,
  });

export const hideNotification = (id: number) =>
  createAction<ActionMap, ActionTypes.HIDE_NOTIFICATION>(
    ActionTypes.HIDE_NOTIFICATION,
  )({ id });

export const hideAllNotifications = createAction<
  ActionMap,
  ActionTypes.HIDE_ALL_NOTIFICATIONS
>(ActionTypes.HIDE_ALL_NOTIFICATIONS);
