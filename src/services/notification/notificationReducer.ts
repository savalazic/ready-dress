import { createReducer } from 'utils/redux';
import {
  ActionMap,
  ActionTypes,
  NotificationStyleType,
} from './notificationActions';

export interface Notification {
  message: string;
  duration: number;
  style: NotificationStyleType;
  id: number;
}

export type Notifications = Notification[];

export interface NotificationState {
  notifications: Notifications;
}

const initialState: NotificationState = {
  notifications: [],
};

const notificationReducer = createReducer<NotificationState, ActionMap>(
  {
    [ActionTypes.SHOW_NOTIFICATION]: (state, payload): NotificationState => {
      return {
        ...state,
        notifications: [
          ...state.notifications,
          {
            id:
              state.notifications.length === 0
                ? 0
                : state.notifications[state.notifications.length - 1].id + 1,
            message: payload.message,
            duration: payload.duration,
            style: payload.style,
          },
        ],
      };
    },
    [ActionTypes.HIDE_NOTIFICATION]: (state, payload): NotificationState => {
      return {
        ...state,
        notifications: state.notifications.filter(
          (notification: Notification) => notification.id !== payload.id,
        ),
      };
    },
    [ActionTypes.HIDE_ALL_NOTIFICATIONS]: (state): NotificationState => {
      return initialState;
    },
  },
  initialState,
);

export default notificationReducer;
