import { State } from 'services/state';

export const getNotifications = (state: State) =>
  state.services.notification.notifications;
