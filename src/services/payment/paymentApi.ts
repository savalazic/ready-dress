import api from 'services/api';

const getCreditCard = () =>
  api
    .get('payment')
    .then(response => ({ response }))
    .catch(error => ({ error }));

const saveCreditCard = (token: string) =>
  api
    .post('payment', {
      token,
      amount: 1,
    })
    .then(response => ({ response }))
    .catch(error => ({ error }));

const removeCreditCard = (cardId: string) =>
  api
    .remove('payment', cardId)
    .then(response => ({ response }))
    .catch(error => ({ error }));

export default {
  getCreditCard,
  saveCreditCard,
  removeCreditCard,
};
