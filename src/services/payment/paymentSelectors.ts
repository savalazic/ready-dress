import { createLoadingSelector } from 'utils/redux';

import { ActionTypes } from './paymentActions';
import { State } from 'services/state';

export const getCreditCard = (state: State) => state.services.payment;

export const isCreditCardLoading = createLoadingSelector(
  ActionTypes.GET_CREDIT_CARD_REQUEST,
);

export const isDeleteCreditCardLoading = createLoadingSelector(
  ActionTypes.REMOVE_CREDIT_CARD_REQUEST,
);

export const isSaveCreditCardLoading = createLoadingSelector(
  ActionTypes.SAVE_CREDIT_CARD_REQUEST,
);
