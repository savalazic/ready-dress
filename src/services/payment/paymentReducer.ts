import { createReducer } from 'utils/redux';

import { ActionMap, ActionTypes } from './paymentActions';

export interface CreditCard {
  brand: string;
  country: string;
  exp_month: number;
  exp_year: number;
  id: string;
  last4: string;
}

// BE supports only one credit card
export type PaymentState = CreditCard | null;

const initialState: PaymentState = null;

const paymentReducer = createReducer<PaymentState, ActionMap>(
  {
    [ActionTypes.GET_CREDIT_CARD_SUCCESS]: (_, action) => action.creditCard,
    [ActionTypes.REMOVE_CREDIT_CARD_SUCCESS]: () => null,
  },
  initialState,
);

export default paymentReducer;
