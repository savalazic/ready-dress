import { takeEvery, call, put } from 'redux-saga/effects';
import { ActionObject } from 'utils/redux';

import {
  ActionTypes,
  ActionMap,
  saveCreditCardSuccess,
  saveCreditCardFailure,
  getCreditCardSuccess,
  getCreditCardFailure,
  removeCreditCardSuccess,
  removeCreditCardFailure,
  getCreditCardRequest,
} from './paymentActions';
import paymentApi from './paymentApi';

function* getCreditCard() {
  const { response, error } = yield call(paymentApi.getCreditCard);

  if (response) {
    // BE returns {id: 'no_card'} when user has no cards
    const creditCard =
      response.data && response.data.id !== 'no_card' ? response.data : null;
    yield put(getCreditCardSuccess(creditCard));
  } else {
    yield put(getCreditCardFailure(error.response));
  }
}

export function* saveCreditCard({
  payload: { token },
}: ActionObject<ActionMap, ActionTypes.SAVE_CREDIT_CARD_REQUEST>) {
  const { response, error } = yield call(paymentApi.saveCreditCard, token);

  if (response) {
    yield put(saveCreditCardSuccess(response.data));
    yield put(getCreditCardRequest());
  } else {
    yield put(saveCreditCardFailure(error.response));
  }
}

function* removeCreditCard({
  payload: { cardId },
}: ActionObject<ActionMap, ActionTypes.REMOVE_CREDIT_CARD_SUCCESS>) {
  const { response, error } = yield call(paymentApi.removeCreditCard, cardId);

  if (response) {
    yield put(removeCreditCardSuccess(cardId));
  } else {
    yield put(removeCreditCardFailure(error.response));
  }
}

export default function* paymentSaga() {
  yield takeEvery(ActionTypes.GET_CREDIT_CARD_REQUEST, getCreditCard);
  yield takeEvery(ActionTypes.SAVE_CREDIT_CARD_REQUEST, saveCreditCard);
  yield takeEvery(ActionTypes.REMOVE_CREDIT_CARD_REQUEST, removeCreditCard);
}
