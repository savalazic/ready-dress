import { createAction } from 'utils/redux';
import { CreditCard } from './paymentReducer';

export enum ActionTypes {
  GET_CREDIT_CARD_REQUEST = 'CUSTOMER/GET_CREDIT_CARD_REQUEST',
  GET_CREDIT_CARD_SUCCESS = 'CUSTOMER/GET_CREDIT_CARD_SUCCESS',
  GET_CREDIT_CARD_FAILURE = 'CUSTOMER/GET_CREDIT_CARD_FAILURE',

  SAVE_CREDIT_CARD_REQUEST = 'CUSTOMER/SAVE_CREDIT_CARD_REQUEST',
  SAVE_CREDIT_CARD_SUCCESS = 'CUSTOMER/SAVE_CREDIT_CARD_SUCCESS',
  SAVE_CREDIT_CARD_FAILURE = 'CUSTOMER/SAVE_CREDIT_CARD_FAILURE',

  REMOVE_CREDIT_CARD_REQUEST = 'CUSTOMER/REMOVE_CREDIT_CARD_REQUEST',
  REMOVE_CREDIT_CARD_SUCCESS = 'CUSTOMER/REMOVE_CREDIT_CARD_SUCCESS',
  REMOVE_CREDIT_CARD_FAILURE = 'CUSTOMER/REMOVE_CREDIT_CARD_FAILURE',
}

export interface ActionMap {
  [ActionTypes.GET_CREDIT_CARD_REQUEST]: any;
  [ActionTypes.GET_CREDIT_CARD_SUCCESS]: { creditCard: CreditCard };
  [ActionTypes.GET_CREDIT_CARD_FAILURE]: any;

  [ActionTypes.SAVE_CREDIT_CARD_REQUEST]: { token: string };
  [ActionTypes.SAVE_CREDIT_CARD_SUCCESS]: { creditCard: CreditCard };
  [ActionTypes.SAVE_CREDIT_CARD_FAILURE]: any;

  [ActionTypes.REMOVE_CREDIT_CARD_REQUEST]: { cardId: string };
  [ActionTypes.REMOVE_CREDIT_CARD_SUCCESS]: { cardId: string };
  [ActionTypes.REMOVE_CREDIT_CARD_FAILURE]: any;
}

export const getCreditCardRequest = () =>
  createAction<ActionMap, ActionTypes.GET_CREDIT_CARD_REQUEST>(
    ActionTypes.GET_CREDIT_CARD_REQUEST,
  )({});

export const getCreditCardSuccess = (creditCard: CreditCard) =>
  createAction<ActionMap, ActionTypes.GET_CREDIT_CARD_SUCCESS>(
    ActionTypes.GET_CREDIT_CARD_SUCCESS,
  )({
    creditCard,
  });

export const getCreditCardFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_CREDIT_CARD_FAILURE>(
    ActionTypes.GET_CREDIT_CARD_FAILURE,
  )({
    error,
  });

export const saveCreditCardRequest = (token: string) =>
  createAction<ActionMap, ActionTypes.SAVE_CREDIT_CARD_REQUEST>(
    ActionTypes.SAVE_CREDIT_CARD_REQUEST,
  )({ token });

export const saveCreditCardSuccess = (creditCard: CreditCard) =>
  createAction<ActionMap, ActionTypes.SAVE_CREDIT_CARD_SUCCESS>(
    ActionTypes.SAVE_CREDIT_CARD_SUCCESS,
  )({
    creditCard,
  });

export const saveCreditCardFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.SAVE_CREDIT_CARD_FAILURE>(
    ActionTypes.SAVE_CREDIT_CARD_FAILURE,
  )({
    error,
  });

export const removeCreditCardRequest = (cardId: string) =>
  createAction<ActionMap, ActionTypes.REMOVE_CREDIT_CARD_REQUEST>(
    ActionTypes.REMOVE_CREDIT_CARD_REQUEST,
  )({ cardId });

export const removeCreditCardSuccess = (cardId: string) =>
  createAction<ActionMap, ActionTypes.REMOVE_CREDIT_CARD_SUCCESS>(
    ActionTypes.REMOVE_CREDIT_CARD_SUCCESS,
  )({
    cardId,
  });

export const removeCreditCardFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.REMOVE_CREDIT_CARD_FAILURE>(
    ActionTypes.REMOVE_CREDIT_CARD_FAILURE,
  )({
    error,
  });
