import { JWT_STORAGE } from '../constants';

const LocalStorageService = {
  getAccessToken: () => localStorage.getItem(JWT_STORAGE),
  setAccessToken: (token: string) => localStorage.setItem(JWT_STORAGE, token),
  removeAccessToken: () => localStorage.removeItem(JWT_STORAGE),
};

export default LocalStorageService;
