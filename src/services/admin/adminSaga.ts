import { all, fork } from 'redux-saga/effects';

import storeSaga from 'services/admin/store/storeSaga';
import storeBranchSaga from 'services/admin/storeBranch/storeBranchSaga';
import storeManagerSaga from 'services/admin/storeManager/storeManagerSaga';
import userSaga from 'services/admin/user/userSaga';

export default function* adminSagas() {
  yield all([
    fork(storeSaga),
    fork(storeBranchSaga),
    fork(storeManagerSaga),
    fork(userSaga),
  ]);
}
