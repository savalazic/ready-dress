import map from 'lodash/map';
import { createSelector } from 'reselect';
import { createLoadingSelector } from 'utils/redux';
import { ActionTypes } from './userActions';
import { State } from 'services/state';

export const getUsersSelector = (state: State) =>
  state.services.admin.user.users;

export const getUsersDropdownArraySelector = createSelector(
  [getUsersSelector],
  users =>
    map(users, user => ({
      text: user.email,
      value: user.id,
    })),
);

export const getUsersLoadingSelector = createLoadingSelector(
  ActionTypes.GET_USERS_REQUEST,
);

export const getUserLoadingSelector = createLoadingSelector(
  ActionTypes.GET_USER_REQUEST,
);

export const getAddUserLoadingSelector = createLoadingSelector(
  ActionTypes.ADD_USER_REQUEST,
);

export const getEditUserLoadingSelector = createLoadingSelector(
  ActionTypes.EDIT_USER_REQUEST,
);

export const getRemoveUserLoading = createLoadingSelector(
  ActionTypes.REMOVE_USER_REQUEST,
);
