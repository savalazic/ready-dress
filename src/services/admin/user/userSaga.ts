import { takeEvery, call, put } from 'redux-saga/effects';
import {
  ActionTypes,
  ActionMap,
  getUsersSuccess,
  getUsersFailure,
  removeUserSuccess,
  removeUserFailure,
  addUserSuccess,
  addUserFailure,
  getUserSuccess,
  getUserFailure,
  editUserSuccess,
  editUserFailure,
} from './userActions';
import userApi from './userApi';
import { ActionObject } from 'utils/redux';
import { push } from 'connected-react-router';

function* getUsers() {
  const { response, error } = yield call(userApi.getUsers);

  if (response) {
    yield put(getUsersSuccess(response.data));
  } else {
    yield put(getUsersFailure(error.response));
  }
}

function* getUser({
  payload: { id },
}: ActionObject<ActionMap, ActionTypes.GET_USER_REQUEST>) {
  const { response, error } = yield call(userApi.getUser, id);

  if (response) {
    yield put(getUserSuccess(response.data));
  } else {
    yield put(getUserFailure(error.response));
  }
}

function* addUser({
  payload: { user },
}: ActionObject<ActionMap, ActionTypes.ADD_USER_REQUEST>) {
  const { response, error } = yield call(userApi.addUser, user);

  if (response) {
    yield put(addUserSuccess(response.data));
    yield put(push('/admin/users'));
  } else {
    yield put(addUserFailure(error.response));
  }
}

function* editUser({
  payload: { id, user },
}: ActionObject<ActionMap, ActionTypes.EDIT_USER_REQUEST>) {
  const { response, error } = yield call(userApi.editUser, id, user);

  if (response) {
    yield put(editUserSuccess(response.data));
    yield put(push('/admin/users'));
  } else {
    yield put(editUserFailure(error.response));
  }
}

function* removeUser({
  payload: { id },
}: ActionObject<ActionMap, ActionTypes.REMOVE_USER_REQUEST>) {
  const { response, error } = yield call(userApi.removeUser, id);

  if (response) {
    yield put(removeUserSuccess(id));
  } else {
    yield put(removeUserFailure(error.response));
  }
}

export default function* userSaga() {
  yield takeEvery(ActionTypes.GET_USERS_REQUEST, getUsers);
  yield takeEvery(ActionTypes.GET_USER_REQUEST, getUser);
  yield takeEvery(ActionTypes.ADD_USER_REQUEST, addUser);
  yield takeEvery(ActionTypes.EDIT_USER_REQUEST, editUser);
  yield takeEvery(ActionTypes.REMOVE_USER_REQUEST, removeUser);
}
