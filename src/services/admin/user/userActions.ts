import { createAction } from 'utils/redux';
import { User, UserFormValues } from './userReducer';

export enum ActionTypes {
  GET_USERS_REQUEST = 'ADMIN/GET_USERS_REQUEST',
  GET_USERS_SUCCESS = 'ADMIN/GET_USERS_SUCCESS',
  GET_USERS_FAILURE = 'ADMIN/GET_USERS_FAILURE',

  GET_USER_REQUEST = 'ADMIN/GET_USER_REQUEST',
  GET_USER_SUCCESS = 'ADMIN/GET_USER_SUCCESS',
  GET_USER_FAILURE = 'ADMIN/GET_USER_FAILURE',

  ADD_USER_REQUEST = 'ADMIN/ADD_USER_REQUEST',
  ADD_USER_SUCCESS = 'ADMIN/ADD_USER_SUCCESS',
  ADD_USER_FAILURE = 'ADMIN/ADD_USER_FAILURE',

  EDIT_USER_REQUEST = 'ADMIN/EDIT_USER_REQUEST',
  EDIT_USER_SUCCESS = 'ADMIN/EDIT_USER_SUCCESS',
  EDIT_USER_FAILURE = 'ADMIN/EDIT_USER_FAILURE',

  REMOVE_USER_REQUEST = 'ADMIN/REMOVE_USER_REQUEST',
  REMOVE_USER_SUCCESS = 'ADMIN/REMOVE_USER_SUCCESS',
  REMOVE_USER_FAILURE = 'ADMIN/REMOVE_USER_FAILURE',
}

export interface ActionMap {
  [ActionTypes.GET_USERS_REQUEST]: {};
  [ActionTypes.GET_USERS_SUCCESS]: {
    users: User[];
  };
  [ActionTypes.GET_USERS_FAILURE]: any;

  [ActionTypes.GET_USER_REQUEST]: { id: number };
  [ActionTypes.GET_USER_SUCCESS]: {
    user: User;
  };
  [ActionTypes.GET_USER_FAILURE]: any;

  [ActionTypes.ADD_USER_REQUEST]: { user: UserFormValues };
  [ActionTypes.ADD_USER_SUCCESS]: {
    user: User;
  };
  [ActionTypes.ADD_USER_FAILURE]: any;

  [ActionTypes.EDIT_USER_REQUEST]: { id: number; user: UserFormValues };
  [ActionTypes.EDIT_USER_SUCCESS]: {
    user: User;
  };
  [ActionTypes.EDIT_USER_FAILURE]: any;

  [ActionTypes.REMOVE_USER_REQUEST]: { id: number };
  [ActionTypes.REMOVE_USER_SUCCESS]: { id: number };
  [ActionTypes.REMOVE_USER_FAILURE]: any;
}

// get users
export const getUsersRequest = createAction<
  ActionMap,
  ActionTypes.GET_USERS_REQUEST
>(ActionTypes.GET_USERS_REQUEST);

export const getUsersSuccess = (users: User[]) =>
  createAction<ActionMap, ActionTypes.GET_USERS_SUCCESS>(
    ActionTypes.GET_USERS_SUCCESS,
  )({
    users,
  });

export const getUsersFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_USERS_FAILURE>(
    ActionTypes.GET_USERS_FAILURE,
  )({
    error,
  });

// get user
export const getUserRequest = (id: number) =>
  createAction<ActionMap, ActionTypes.GET_USER_REQUEST>(
    ActionTypes.GET_USER_REQUEST,
  )({
    id,
  });

export const getUserSuccess = (user: User) =>
  createAction<ActionMap, ActionTypes.GET_USER_SUCCESS>(
    ActionTypes.GET_USER_SUCCESS,
  )({
    user,
  });

export const getUserFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_USER_FAILURE>(
    ActionTypes.GET_USER_FAILURE,
  )({
    error,
  });

// add user
export const addUserRequest = (user: UserFormValues) =>
  createAction<ActionMap, ActionTypes.ADD_USER_REQUEST>(
    ActionTypes.ADD_USER_REQUEST,
  )({
    user,
  });

export const addUserSuccess = (user: User) =>
  createAction<ActionMap, ActionTypes.ADD_USER_SUCCESS>(
    ActionTypes.ADD_USER_SUCCESS,
  )({
    user,
  });

export const addUserFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.ADD_USER_FAILURE>(
    ActionTypes.ADD_USER_FAILURE,
  )({
    error,
  });

// edit user
export const editUserRequest = (id: number, user: UserFormValues) =>
  createAction<ActionMap, ActionTypes.EDIT_USER_REQUEST>(
    ActionTypes.EDIT_USER_REQUEST,
  )({
    id,
    user,
  });

export const editUserSuccess = (user: User) =>
  createAction<ActionMap, ActionTypes.EDIT_USER_SUCCESS>(
    ActionTypes.EDIT_USER_SUCCESS,
  )({
    user,
  });

export const editUserFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.EDIT_USER_FAILURE>(
    ActionTypes.EDIT_USER_FAILURE,
  )({
    error,
  });

// remove user
export const removeUserRequest = (id: number) =>
  createAction<ActionMap, ActionTypes.REMOVE_USER_REQUEST>(
    ActionTypes.REMOVE_USER_REQUEST,
  )({
    id,
  });

export const removeUserSuccess = (id: number) =>
  createAction<ActionMap, ActionTypes.REMOVE_USER_SUCCESS>(
    ActionTypes.REMOVE_USER_SUCCESS,
  )({
    id,
  });

export const removeUserFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.REMOVE_USER_FAILURE>(
    ActionTypes.REMOVE_USER_FAILURE,
  )({
    error,
  });
