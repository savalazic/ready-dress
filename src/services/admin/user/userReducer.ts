import toString from 'lodash/toString';
import mapKeys from 'lodash/mapKeys';
import { createReducer } from 'utils/redux';
import { ActionMap, ActionTypes } from './userActions';
import { omit } from 'utils/object';

export interface User {
  id: number;
  email: string;
  managed_by: number;
}

export interface UserFormValues {
  email: string;
  password: string;
  password_confirmation: string;
  role: string;
  first_name: string;
  last_name: string;
  phone_no: string;
  dob: Date;
  zipcode: string;
}

export interface UserState {
  users: {
    [id: number]: User;
  } | null;
}

const initialState = {
  users: null,
};

const userReducer = createReducer<UserState, ActionMap>(
  {
    [ActionTypes.GET_USERS_SUCCESS]: (state, payload): UserState => ({
      ...state,
      users: mapKeys(payload.users, 'id'),
    }),
    [ActionTypes.GET_USER_SUCCESS]: (state, payload): UserState => ({
      ...state,
      users: Object.assign({}, state.users, {
        [payload.user.id]: payload.user,
      }),
    }),
    [ActionTypes.ADD_USER_SUCCESS]: (state, payload): UserState => ({
      ...state,
      users: Object.assign({}, state.users, {
        [payload.user.id]: payload.user,
      }),
    }),
    [ActionTypes.EDIT_USER_SUCCESS]: (state, payload): UserState => ({
      ...state,
      users: Object.assign({}, state.users, {
        [payload.user.id]: payload.user,
      }),
    }),
    [ActionTypes.REMOVE_USER_SUCCESS]: (state, payload): UserState => ({
      ...state,
      users: omit(state.users, [toString(payload.id)]),
    }),
  },
  initialState,
);

export default userReducer;
