import api from 'services/api';
import { UserFormValues } from './userReducer';

const getUsers = () =>
  api
    .get('admin/users')
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const getUser = (id: number) =>
  api
    .get(`admin/users/${id}`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const addUser = (user: UserFormValues) =>
  api
    .post('admin/users', { user })
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const editUser = (id: number, user: UserFormValues) =>
  api
    .put(`admin/users/${id}`, { user })
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const removeUser = (id: number) =>
  api
    .remove('admin/users', id)
    .then(response => ({ response }))
    .catch(error => ({ error }));

export default { getUsers, getUser, addUser, editUser, removeUser };
