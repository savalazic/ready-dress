import { combineReducers } from 'redux';

import storeReducer, { StoreState } from './store/storeReducer';
import storeBranchReducer, {
  StoreBranchState,
} from './storeBranch/storeBranchReducer';
import storeManagerReducer, {
  StoreManagerState,
} from './storeManager/storeManagerReducer';
import userReducer, { UserState } from './user/userReducer';

export interface AdminState {
  store: StoreState;
  storeBranch: StoreBranchState;
  storeManager: StoreManagerState;
  user: UserState;
}

const adminReducer = combineReducers({
  store: storeReducer,
  storeBranch: storeBranchReducer,
  storeManager: storeManagerReducer,
  user: userReducer,
});

export default adminReducer;
