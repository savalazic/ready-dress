import { takeEvery, call, put } from 'redux-saga/effects';
import {
  ActionTypes,
  getStoresSuccess,
  getStoresFailure,
} from './storeActions';
import storeApi from './storeApi';

function* getStores() {
  const { response, error } = yield call(storeApi.getStores);

  if (response) {
    yield put(getStoresSuccess(response.data));
  } else {
    yield put(getStoresFailure(error.response));
  }
}

export default function* storeSaga() {
  yield takeEvery(ActionTypes.GET_STORES_REQUEST, getStores);
}
