import { createLoadingSelector } from 'utils/redux';
import { ActionTypes } from './storeActions';
import { State } from 'services/state';

export const getStoresSelector = (state: State) =>
  state.services.admin.store.stores;

export const getStoresLoadingSelector = createLoadingSelector(
  ActionTypes.GET_STORES_REQUEST,
);
