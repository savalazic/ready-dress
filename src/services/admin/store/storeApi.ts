import api from 'services/api';

const getStores = () =>
  api
    .get('admin/stores')
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

export default {
  getStores,
};
