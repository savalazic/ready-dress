import { createAction } from 'utils/redux';
import { Store } from './storeReducer';

export enum ActionTypes {
  GET_STORES_REQUEST = 'ADMIN/GET_STORES_REQUEST',
  GET_STORES_SUCCESS = 'ADMIN/GET_STORES_SUCCESS',
  GET_STORES_FAILURE = 'ADMIN/GET_STORES_FAILURE',
}

export interface ActionMap {
  [ActionTypes.GET_STORES_REQUEST]: any;
  [ActionTypes.GET_STORES_SUCCESS]: { stores: Store[] };
  [ActionTypes.GET_STORES_FAILURE]: any;
}

// get stores
export const getStoresRequest = createAction<
  ActionMap,
  ActionTypes.GET_STORES_REQUEST
>(ActionTypes.GET_STORES_REQUEST);

export const getStoresSuccess = (stores: Store[]) =>
  createAction<ActionMap, ActionTypes.GET_STORES_SUCCESS>(
    ActionTypes.GET_STORES_SUCCESS,
  )({
    stores,
  });

export const getStoresFailure = (error: any) =>
  createAction<ActionMap, ActionTypes.GET_STORES_FAILURE>(
    ActionTypes.GET_STORES_FAILURE,
  )({
    error,
  });
