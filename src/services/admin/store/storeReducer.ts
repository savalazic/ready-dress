import mapKeys from 'lodash/mapKeys';
import { createReducer } from 'utils/redux';
import { ActionTypes, ActionMap } from './storeActions';

export interface Store {
  id: number;
  name: string;
}

export interface StoreState {
  stores: {
    [id: number]: Store;
  } | null;
}

const initialState: StoreState = {
  stores: null,
};

const storeReducer = createReducer<StoreState, ActionMap>(
  {
    [ActionTypes.GET_STORES_SUCCESS]: (state, payload): StoreState => ({
      ...state,
      stores: mapKeys(payload.stores, 'id'),
    }),
  },
  initialState,
);

export default storeReducer;
