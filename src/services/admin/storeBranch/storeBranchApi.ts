import api from 'services/api';
import { StoreBranch } from './storeBranchReducer';

const getStoreBranches = (storeId: number) =>
  api
    .get(`admin/stores/${storeId}/store_branches`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const getStoreBranch = (storeId: number, storeBranchId: number) =>
  api
    .get(`admin/stores/${storeId}/store_branches/${storeBranchId}`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const addStoreBranch = (storeBranch: StoreBranch, storeId: number) =>
  api
    .post(`admin/stores/${storeId}/store_branches`, {
      store_branch: storeBranch,
    })
    .then(response => ({ response }))
    .catch(error => ({ error }));

const editStoreBranch = (
  storeBranch: StoreBranch,
  storeBranchId: number,
  storeId: number,
) =>
  api
    .put(`admin/stores/${storeId}/store_branches/${storeBranchId}`, {
      store_branch: storeBranch,
    })
    .then(response => ({ response }))
    .catch(error => ({ error }));

const removeStoreBranch = (storeId: number, storeBranchId: number) =>
  api
    .remove(`admin/stores/${storeId}/store_branches`, storeBranchId)
    .then(response => ({ response }))
    .catch(error => ({ error }));

export default {
  getStoreBranches,
  getStoreBranch,
  addStoreBranch,
  editStoreBranch,
  removeStoreBranch,
};
