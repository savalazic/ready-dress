import { createAction } from 'utils/redux';
import { StoreBranch } from './storeBranchReducer';

export enum ActionTypes {
  GET_STORE_BRANCHES_REQUEST = 'ADMIN/GET_STORE_BRANCHES_REQUEST',
  GET_STORE_BRANCHES_SUCCESS = 'ADMIN/GET_STORE_BRANCHES_SUCCESS',
  GET_STORE_BRANCHES_FAILURE = 'ADMIN/GET_STORE_BRANCHES_FAILURE',

  GET_STORE_BRANCH_REQUEST = 'ADMIN/GET_STORE_BRANCH_REQUEST',
  GET_STORE_BRANCH_SUCCESS = 'ADMIN/GET_STORE_BRANCH_SUCCESS',
  GET_STORE_BRANCH_FAILURE = 'ADMIN/GET_STORE_BRANCH_FAILURE',

  ADD_STORE_BRANCH_REQUEST = 'ADMIN/ADD_STORE_BRANCH_REQUEST',
  ADD_STORE_BRANCH_SUCCESS = 'ADMIN/ADD_STORE_BRANCH_SUCCESS',
  ADD_STORE_BRANCH_FAILURE = 'ADMIN/ADD_STORE_BRANCH_FAILURE',

  REMOVE_STORE_BRANCH_REQUEST = 'ADMIN/REMOVE_STORE_BRANCH_REQUEST',
  REMOVE_STORE_BRANCH_SUCCESS = 'ADMIN/REMOVE_STORE_BRANCH_SUCCESS',
  REMOVE_STORE_BRANCH_FAILURE = 'ADMIN/REMOVE_STORE_BRANCH_FAILURE',

  EDIT_STORE_BRANCH_REQUEST = 'ADMIN/EDIT_STORE_BRANCH_REQUEST',
  EDIT_STORE_BRANCH_SUCCESS = 'ADMIN/EDIT_STORE_BRANCH_SUCCESS',
  EDIT_STORE_BRANCH_FAILURE = 'ADMIN/EDIT_STORE_BRANCH_FAILURE',
}

export interface ActionMap {
  [ActionTypes.GET_STORE_BRANCHES_REQUEST]: {
    id: number;
  };
  [ActionTypes.GET_STORE_BRANCHES_SUCCESS]: {
    storeBranches: StoreBranch[];
  };
  [ActionTypes.GET_STORE_BRANCHES_FAILURE]: any;

  [ActionTypes.GET_STORE_BRANCH_REQUEST]: {
    storeId: number;
    storeBranchId: number;
  };
  [ActionTypes.GET_STORE_BRANCH_SUCCESS]: {
    storeBranch: StoreBranch;
  };
  [ActionTypes.GET_STORE_BRANCH_FAILURE]: any;

  [ActionTypes.ADD_STORE_BRANCH_REQUEST]: {
    storeBranch: StoreBranch;
    storeId: number;
  };
  [ActionTypes.ADD_STORE_BRANCH_SUCCESS]: {
    storeBranch: StoreBranch;
  };
  [ActionTypes.ADD_STORE_BRANCH_FAILURE]: any;

  [ActionTypes.REMOVE_STORE_BRANCH_REQUEST]: {
    storeId: number;
    storeBranchId: number;
  };
  [ActionTypes.REMOVE_STORE_BRANCH_SUCCESS]: {
    storeBranchId: number;
  };
  [ActionTypes.REMOVE_STORE_BRANCH_FAILURE]: any;

  [ActionTypes.EDIT_STORE_BRANCH_REQUEST]: {
    storeBranch: StoreBranch;
    storeId: number;
    storeBranchId: number;
  };
  [ActionTypes.EDIT_STORE_BRANCH_SUCCESS]: {
    storeBranch: StoreBranch;
  };
  [ActionTypes.EDIT_STORE_BRANCH_FAILURE]: any;
}

// get store branches
export const getStoreBranchesRequest = (id: number) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCHES_REQUEST>(
    ActionTypes.GET_STORE_BRANCHES_REQUEST,
  )({
    id,
  });

export const getStoreBranchesSuccess = (storeBranches: StoreBranch[]) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCHES_SUCCESS>(
    ActionTypes.GET_STORE_BRANCHES_SUCCESS,
  )({
    storeBranches,
  });

export const getStoreBranchesFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCHES_FAILURE>(
    ActionTypes.GET_STORE_BRANCHES_FAILURE,
  )({
    error,
  });

// get store branch
export const getStoreBranchRequest = (storeId: number, storeBranchId: number) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCH_REQUEST>(
    ActionTypes.GET_STORE_BRANCH_REQUEST,
  )({
    storeId,
    storeBranchId,
  });

export const getStoreBranchSuccess = (storeBranch: StoreBranch) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCH_SUCCESS>(
    ActionTypes.GET_STORE_BRANCH_SUCCESS,
  )({
    storeBranch,
  });

export const getStoreBranchFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_STORE_BRANCH_FAILURE>(
    ActionTypes.GET_STORE_BRANCH_FAILURE,
  )({
    error,
  });

// add store branch
export const addStoreBranchRequest = (
  storeBranch: StoreBranch,
  storeId: number,
) =>
  createAction<ActionMap, ActionTypes.ADD_STORE_BRANCH_REQUEST>(
    ActionTypes.ADD_STORE_BRANCH_REQUEST,
  )({
    storeBranch,
    storeId,
  });

export const addStoreBranchSuccess = (storeBranch: StoreBranch) =>
  createAction<ActionMap, ActionTypes.ADD_STORE_BRANCH_SUCCESS>(
    ActionTypes.ADD_STORE_BRANCH_SUCCESS,
  )({
    storeBranch,
  });

export const addStoreBranchFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.ADD_STORE_BRANCH_FAILURE>(
    ActionTypes.ADD_STORE_BRANCH_FAILURE,
  )({
    error,
  });

// remove store item
export const removeStoreBranchRequest = (
  storeId: number,
  storeBranchId: number,
) =>
  createAction<ActionMap, ActionTypes.REMOVE_STORE_BRANCH_REQUEST>(
    ActionTypes.REMOVE_STORE_BRANCH_REQUEST,
  )({
    storeId,
    storeBranchId,
  });

export const removeStoreBranchSuccess = (storeBranchId: number) =>
  createAction<ActionMap, ActionTypes.REMOVE_STORE_BRANCH_SUCCESS>(
    ActionTypes.REMOVE_STORE_BRANCH_SUCCESS,
  )({
    storeBranchId,
  });

export const removeStoreBranchFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.REMOVE_STORE_BRANCH_FAILURE>(
    ActionTypes.REMOVE_STORE_BRANCH_FAILURE,
  )({
    error,
  });

// edit store item
export const editStoreBranchRequest = (
  storeBranch: StoreBranch,
  storeId: number,
  storeBranchId: number,
) =>
  createAction<ActionMap, ActionTypes.EDIT_STORE_BRANCH_REQUEST>(
    ActionTypes.EDIT_STORE_BRANCH_REQUEST,
  )({
    storeBranch,
    storeId,
    storeBranchId,
  });

export const editStoreBranchSuccess = (storeBranch: StoreBranch) =>
  createAction<ActionMap, ActionTypes.EDIT_STORE_BRANCH_SUCCESS>(
    ActionTypes.EDIT_STORE_BRANCH_SUCCESS,
  )({
    storeBranch,
  });

export const editStoreBranchFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.EDIT_STORE_BRANCH_FAILURE>(
    ActionTypes.EDIT_STORE_BRANCH_FAILURE,
  )({
    error,
  });
