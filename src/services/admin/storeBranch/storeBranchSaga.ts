import { takeEvery, call, put, select } from 'redux-saga/effects';
import { ActionObject } from 'utils/redux';
import { push } from 'connected-react-router';
import { getPrevPath } from 'utils/string';
import {
  ActionTypes,
  ActionMap,
  getStoreBranchesSuccess,
  getStoreBranchesFailure,
  addStoreBranchSuccess,
  addStoreBranchFailure,
  removeStoreBranchSuccess,
  removeStoreBranchFailure,
  editStoreBranchFailure,
  editStoreBranchSuccess,
  getStoreBranchSuccess,
  getStoreBranchFailure,
} from './storeBranchActions';
import storeBranchApi from './storeBranchApi';
import { getStoreBranchesSelector } from './storeBranchSelectors';
import { showNotification } from 'services/notification/notificationActions';

function* getStoreBranches({
  payload: { id },
}: ActionObject<ActionMap, ActionTypes.GET_STORE_BRANCHES_REQUEST>) {
  const { response, error } = yield call(storeBranchApi.getStoreBranches, id);

  if (response) {
    yield put(getStoreBranchesSuccess(response.data));
  } else {
    yield put(getStoreBranchesFailure(error.response));
  }
}

function* getStoreBranch({
  payload: { storeBranchId, storeId },
}: ActionObject<ActionMap, ActionTypes.GET_STORE_BRANCH_REQUEST>) {
  const { response, error } = yield call(
    storeBranchApi.getStoreBranch,
    storeId,
    storeBranchId,
  );

  if (response) {
    yield put(getStoreBranchSuccess(response.data));
  } else {
    yield put(getStoreBranchFailure(error.response));
  }
}

function* addStoreBranch({
  payload: { storeBranch, storeId },
}: ActionObject<ActionMap, ActionTypes.ADD_STORE_BRANCH_REQUEST>) {
  const { response, error } = yield call(
    storeBranchApi.addStoreBranch,
    storeBranch,
    storeId,
  );

  if (response) {
    yield put(addStoreBranchSuccess(response.data));

    const currentPath = yield select(state => state.router.location.pathname);
    const prevPath = yield call(getPrevPath, currentPath, -1);

    yield put(push(prevPath));
  } else {
    yield put(addStoreBranchFailure(error.response));
  }
}

function* removeStoreBranch({
  payload: { storeId, storeBranchId },
}: ActionObject<ActionMap, ActionTypes.REMOVE_STORE_BRANCH_REQUEST>) {
  const { response, error } = yield call(
    storeBranchApi.removeStoreBranch,
    storeId,
    storeBranchId,
  );

  const storeBranches = yield select(getStoreBranchesSelector);
  const storeBranch = yield storeBranches[storeBranchId];

  if (response) {
    yield put(removeStoreBranchSuccess(storeBranchId));
    yield put(
      showNotification(
        `Store branch ${storeBranch.city} is removed`,
        3000,
        'success',
      ),
    );
  } else {
    yield put(removeStoreBranchFailure(error.response));
  }
}

function* editStoreBranch({
  payload: { storeBranch, storeBranchId, storeId },
}: ActionObject<ActionMap, ActionTypes.EDIT_STORE_BRANCH_REQUEST>) {
  const { response, error } = yield call(
    storeBranchApi.editStoreBranch,
    storeBranch,
    storeBranchId,
    storeId,
  );

  const storeBranches = yield select(getStoreBranchesSelector);
  const editedStoreBranch = yield storeBranches[storeBranchId];

  if (response) {
    yield put(editStoreBranchSuccess(response.data));

    yield put(
      showNotification(
        `Store branch ${editedStoreBranch.city} is edited`,
        3000,
        'success',
      ),
    );

    const currentPath = yield select(state => state.router.location.pathname);
    const prevPath = yield call(getPrevPath, currentPath, -2);

    yield put(push(prevPath));
  } else {
    yield put(editStoreBranchFailure(error.response));
  }
}

export default function* storeBranchSaga() {
  yield takeEvery(ActionTypes.GET_STORE_BRANCHES_REQUEST, getStoreBranches);
  yield takeEvery(ActionTypes.GET_STORE_BRANCH_REQUEST, getStoreBranch);
  yield takeEvery(ActionTypes.ADD_STORE_BRANCH_REQUEST, addStoreBranch);
  yield takeEvery(ActionTypes.REMOVE_STORE_BRANCH_REQUEST, removeStoreBranch);
  yield takeEvery(ActionTypes.EDIT_STORE_BRANCH_REQUEST, editStoreBranch);
}
