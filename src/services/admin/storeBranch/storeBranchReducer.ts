import toString from 'lodash/toString';
import mapKeys from 'lodash/mapKeys';
import { omit } from 'utils/object';
import { createReducer } from 'utils/redux';
import { ActionTypes, ActionMap } from './storeBranchActions';

export interface StoreBranch {
  id: number;
  city: string;
  phone_number: string;
  state: string;
  street_name: string;
  street_number: string;
}

export interface StoreBranchState {
  storeBranches: {
    [id: number]: StoreBranch;
  } | null;
}

const initialState: StoreBranchState = {
  storeBranches: null,
};

const storeBranchReducer = createReducer<StoreBranchState, ActionMap>(
  {
    [ActionTypes.GET_STORE_BRANCHES_SUCCESS]: (
      state,
      payload,
    ): StoreBranchState => ({
      ...state,
      storeBranches: mapKeys(payload.storeBranches, 'id'),
    }),
    [ActionTypes.GET_STORE_BRANCH_SUCCESS]: (
      state,
      payload,
    ): StoreBranchState => ({
      ...state,
      storeBranches: Object.assign({}, state.storeBranches, {
        [payload.storeBranch.id]: payload.storeBranch,
      }),
    }),
    [ActionTypes.ADD_STORE_BRANCH_SUCCESS]: (
      state,
      payload,
    ): StoreBranchState => ({
      ...state,
      storeBranches: Object.assign({}, state.storeBranches, {
        [payload.storeBranch.id]: payload.storeBranch,
      }),
    }),
    [ActionTypes.REMOVE_STORE_BRANCH_SUCCESS]: (
      state,
      payload,
    ): StoreBranchState => ({
      ...state,
      storeBranches: omit(state.storeBranches, [
        toString(payload.storeBranchId),
      ]),
    }),
    [ActionTypes.EDIT_STORE_BRANCH_SUCCESS]: (
      state,
      payload,
    ): StoreBranchState => ({
      ...state,
      storeBranches: Object.assign({}, state.storeBranches, {
        [payload.storeBranch.id]: payload.storeBranch,
      }),
    }),
  },
  initialState,
);

export default storeBranchReducer;
