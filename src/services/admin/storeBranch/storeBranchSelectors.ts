import { createLoadingSelector } from 'utils/redux';
import { ActionTypes } from './storeBranchActions';
import { State } from 'services/state';

export const getStoreBranchesSelector = (state: State) =>
  state.services.admin.storeBranch.storeBranches;

export const getStoreBranchesLoadingSelector = createLoadingSelector(
  ActionTypes.GET_STORE_BRANCHES_REQUEST,
);
export const getStoreBranchLoadingSelector = createLoadingSelector(
  ActionTypes.GET_STORE_BRANCH_REQUEST,
);
export const getAddStoreBranchLoadingSelector = createLoadingSelector(
  ActionTypes.ADD_STORE_BRANCH_REQUEST,
);
export const getEditStoreBranchLoadingSelector = createLoadingSelector(
  ActionTypes.EDIT_STORE_BRANCH_REQUEST,
);
export const getRemoveStoreBranchLoadingSelector = createLoadingSelector(
  ActionTypes.REMOVE_STORE_BRANCH_REQUEST,
);
