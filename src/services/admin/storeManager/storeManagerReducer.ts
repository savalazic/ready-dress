import toString from 'lodash/toString';
import mapKeys from 'lodash/mapKeys';
import { createReducer } from 'utils/redux';
import { omit } from 'utils/object';
import { ActionMap, ActionTypes } from './storeManagerActions';

export interface StoreManager {
  email: string;
  id: number;
  managed_by: number;
}

export interface StoreManagerState {
  storeManagers: {
    [id: number]: StoreManager;
  } | null;
}

const initialState = {
  storeManagers: null,
};

const storeManagerReducer = createReducer<StoreManagerState, ActionMap>(
  {
    [ActionTypes.GET_STORE_MANAGERS_SUCCESS]: (
      state,
      payload,
    ): StoreManagerState => ({
      ...state,
      storeManagers: mapKeys(payload.storeManagers, 'id'),
    }),
    [ActionTypes.ADD_STORE_MANAGER_SUCCESS]: (
      state,
      payload,
    ): StoreManagerState => ({
      ...state,
      storeManagers: Object.assign({}, state.storeManagers, {
        [payload.storeManager.id]: payload.storeManager,
      }),
    }),
    [ActionTypes.REMOVE_STORE_MANAGER_SUCCESS]: (
      state,
      payload,
    ): StoreManagerState => ({
      ...state,
      storeManagers: omit(state.storeManagers, [toString(payload.userId)]),
    }),
  },
  initialState,
);

export default storeManagerReducer;
