import { ActionObject } from 'utils/redux';
import { takeEvery, call, put } from 'redux-saga/effects';
import {
  ActionTypes,
  ActionMap,
  getStoreManagersSuccess,
  getStoreManagersFailure,
  addStoreManagerSuccess,
  addStoreManagerFailure,
  removeStoreManagerSuccess,
  removeStoreManagerFailure,
} from './storeManagerActions';
import storeManagerApi from './storeManagerApi';

function* getStoreManagers({
  payload: { storeId, storeBranchId },
}: ActionObject<ActionMap, ActionTypes.GET_STORE_MANAGERS_REQUEST>) {
  const { response, error } = yield call(
    storeManagerApi.getStoreManagers,
    storeId,
    storeBranchId,
  );

  if (response) {
    yield put(getStoreManagersSuccess(response.data));
  } else {
    yield put(getStoreManagersFailure(error.response));
  }
}

function* addStoreManager({
  payload: { userId, storeId, storeBranchId },
}: ActionObject<ActionMap, ActionTypes.ADD_STORE_MANAGER_REQUEST>) {
  const { response, error } = yield call(
    storeManagerApi.addStoreManager,
    userId,
    storeId,
    storeBranchId,
  );

  if (response) {
    yield put(addStoreManagerSuccess(response.data));
  } else {
    yield put(addStoreManagerFailure(error.response));
  }
}

function* removeStoreManager({
  payload: { userId, storeId, storeBranchId },
}: ActionObject<ActionMap, ActionTypes.REMOVE_STORE_MANAGER_REQUEST>) {
  const { response, error } = yield call(
    storeManagerApi.removeStoreManager,
    userId,
    storeId,
    storeBranchId,
  );

  if (response) {
    yield put(removeStoreManagerSuccess(userId));
  } else {
    yield put(removeStoreManagerFailure(error.response));
  }
}

export default function* storeManagerSaga() {
  yield takeEvery(ActionTypes.GET_STORE_MANAGERS_REQUEST, getStoreManagers);
  yield takeEvery(ActionTypes.ADD_STORE_MANAGER_REQUEST, addStoreManager);
  yield takeEvery(ActionTypes.REMOVE_STORE_MANAGER_REQUEST, removeStoreManager);
}
