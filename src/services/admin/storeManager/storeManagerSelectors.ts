import { createLoadingSelector } from 'utils/redux';
import { ActionTypes } from './storeManagerActions';
import { State } from 'services/state';

export const getStoreManagersSelector = (state: State) =>
  state.services.admin.storeManager.storeManagers;

export const getStoreManagersLoadingSelector = createLoadingSelector(
  ActionTypes.GET_STORE_MANAGERS_REQUEST,
);
export const getAddStoreManagerLoading = createLoadingSelector(
  ActionTypes.ADD_STORE_MANAGER_REQUEST,
);
export const getRemoveStoreManagerLoading = createLoadingSelector(
  ActionTypes.REMOVE_STORE_MANAGER_REQUEST,
);
