import api from 'services/api';

const getStoreManagers = (storeId: number, storeBranchId: number) =>
  api
    .get(`admin/stores/${storeId}/store_branches/${storeBranchId}/managers`)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const addStoreManager = (
  userId: number,
  storeId: number,
  storeBranchId: number,
) =>
  api
    .post(`admin/stores/${storeId}/store_branches/${storeBranchId}/managers`, {
      user_id: userId,
    })
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const removeStoreManager = (
  userId: number,
  storeId: number,
  storeBranchId: number,
) =>
  api
    .remove(
      `admin/stores/${storeId}/store_branches/${storeBranchId}/managers`,
      userId,
    )
    .then(response => ({ response }))
    .catch(error => ({ error }));

export default { getStoreManagers, addStoreManager, removeStoreManager };
