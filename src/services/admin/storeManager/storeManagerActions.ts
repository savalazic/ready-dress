import { createAction } from 'utils/redux';
import { StoreManager } from './storeManagerReducer';

export enum ActionTypes {
  GET_STORE_MANAGERS_REQUEST = 'ADMIN/GET_STORE_MANAGERS_REQUEST',
  GET_STORE_MANAGERS_SUCCESS = 'ADMIN/GET_STORE_MANAGERS_SUCCESS',
  GET_STORE_MANAGERS_FAILURE = 'ADMIN/GET_STORE_MANAGERS_FAILURE',

  ADD_STORE_MANAGER_REQUEST = 'ADMIN/ADD_STORE_MANAGER_REQUEST',
  ADD_STORE_MANAGER_SUCCESS = 'ADMIN/ADD_STORE_MANAGER_SUCCESS',
  ADD_STORE_MANAGER_FAILURE = 'ADMIN/ADD_STORE_MANAGER_FAILURE',

  REMOVE_STORE_MANAGER_REQUEST = 'ADMIN/REMOVE_STORE_MANAGER_REQUEST',
  REMOVE_STORE_MANAGER_SUCCESS = 'ADMIN/REMOVE_STORE_MANAGER_SUCCESS',
  REMOVE_STORE_MANAGER_FAILURE = 'ADMIN/REMOVE_STORE_MANAGER_FAILURE',
}

export interface ActionMap {
  [ActionTypes.GET_STORE_MANAGERS_REQUEST]: {
    storeId: number;
    storeBranchId: number;
  };
  [ActionTypes.GET_STORE_MANAGERS_SUCCESS]: {
    storeManagers: StoreManager[];
  };
  [ActionTypes.GET_STORE_MANAGERS_FAILURE]: any;

  [ActionTypes.ADD_STORE_MANAGER_REQUEST]: {
    userId: number;
    storeId: number;
    storeBranchId: number;
  };
  // @NOTE: CHECK THIS WHEN BACKEND WORKS
  [ActionTypes.ADD_STORE_MANAGER_SUCCESS]: {
    storeManager: StoreManager;
  };
  [ActionTypes.ADD_STORE_MANAGER_FAILURE]: any;

  [ActionTypes.REMOVE_STORE_MANAGER_REQUEST]: {
    userId: number;
    storeId: number;
    storeBranchId: number;
  };
  [ActionTypes.REMOVE_STORE_MANAGER_SUCCESS]: { userId: number };
  [ActionTypes.REMOVE_STORE_MANAGER_FAILURE]: any;
}

// get store managers
export const getStoreManagersRequest = (
  storeId: number,
  storeBranchId: number,
) =>
  createAction<ActionMap, ActionTypes.GET_STORE_MANAGERS_REQUEST>(
    ActionTypes.GET_STORE_MANAGERS_REQUEST,
  )({
    storeId,
    storeBranchId,
  });

export const getStoreManagersSuccess = (storeManagers: StoreManager[]) =>
  createAction<ActionMap, ActionTypes.GET_STORE_MANAGERS_SUCCESS>(
    ActionTypes.GET_STORE_MANAGERS_SUCCESS,
  )({
    storeManagers,
  });

export const getStoreManagersFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_STORE_MANAGERS_FAILURE>(
    ActionTypes.GET_STORE_MANAGERS_FAILURE,
  )({
    error,
  });

// add store manager
export const addStoreManagerRequest = (
  userId: number,
  storeId: number,
  storeBranchId: number,
) =>
  createAction<ActionMap, ActionTypes.ADD_STORE_MANAGER_REQUEST>(
    ActionTypes.ADD_STORE_MANAGER_REQUEST,
  )({
    userId,
    storeId,
    storeBranchId,
  });

export const addStoreManagerSuccess = (storeManager: StoreManager) =>
  createAction<ActionMap, ActionTypes.ADD_STORE_MANAGER_SUCCESS>(
    ActionTypes.ADD_STORE_MANAGER_SUCCESS,
  )({
    storeManager,
  });
export const addStoreManagerFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.ADD_STORE_MANAGER_FAILURE>(
    ActionTypes.ADD_STORE_MANAGER_FAILURE,
  )({
    error,
  });

// remove store manager
export const removeStoreManagerRequest = (
  userId: number,
  storeId: number,
  storeBranchId: number,
) =>
  createAction<ActionMap, ActionTypes.REMOVE_STORE_MANAGER_REQUEST>(
    ActionTypes.REMOVE_STORE_MANAGER_REQUEST,
  )({
    userId,
    storeId,
    storeBranchId,
  });

export const removeStoreManagerSuccess = (userId: number) =>
  createAction<ActionMap, ActionTypes.REMOVE_STORE_MANAGER_SUCCESS>(
    ActionTypes.REMOVE_STORE_MANAGER_SUCCESS,
  )({ userId });

export const removeStoreManagerFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.REMOVE_STORE_MANAGER_FAILURE>(
    ActionTypes.REMOVE_STORE_MANAGER_FAILURE,
  )({
    error,
  });
