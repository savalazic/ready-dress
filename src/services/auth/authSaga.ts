import { put, call, takeEvery, takeLatest } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import { ActionObject } from 'utils/redux';
import LocalStorageService from 'services/localStorage';
import { showNotification } from 'services/notification/notificationActions';

import authApi from './authApi';
import {
  ActionTypes,
  loginSuccess,
  loginFailure,
  ActionMap,
  registerFailure,
  loginFacebookFailure,
  getAuthUserRequest,
  getAuthUserFailure,
  getAuthUserSuccess,
  registerSuccess,
  passwordResetRequestSuccess,
  passwordResetRequestFailure,
  resetPasswordSuccess,
  resetPasswordFailure,
  updateAuthUserSuccess,
  updateAuthUserFailure,
} from './authActions';

export function* login({
  payload: { email, password },
}: ActionObject<ActionMap, ActionTypes.LOGIN_REQUEST>) {
  const { response, error } = yield call(authApi.login, email, password);

  if (response) {
    LocalStorageService.setAccessToken(response.data.jwt);
    yield put(loginSuccess(response.data));
    yield put(push('/'));
  } else {
    yield put(loginFailure(error.response));
  }
}

export function* loginFacebook({
  payload: { token },
}: ActionObject<ActionMap, ActionTypes.LOGIN_FACEBOOK_REQUEST>) {
  const { response, error } = yield call(authApi.loginFacebook, token);

  if (response) {
    LocalStorageService.setAccessToken(response.data.jwt);
    yield put(push('/'));
    yield put(getAuthUserRequest({}));
  } else {
    yield put(loginFacebookFailure(error));
  }
}

export function* register({
  payload: {
    email,
    password,
    password_confirmation,
    first_name,
    last_name,
    dob,
    phone_no,
    zipcode,
  },
}: ActionObject<ActionMap, ActionTypes.REGISTER_REQUEST>) {
  const { response, error } = yield call(
    authApi.register,
    email,
    password,
    password_confirmation,
    first_name,
    last_name,
    dob,
    phone_no,
    zipcode,
  );

  if (response) {
    yield put(push('/stores'));
    yield put(
      showNotification(
        'Successfully registered, please check your email to active your account',
        6000,
        'success',
      ),
    );
    yield put(registerSuccess({}));
  } else {
    yield put(registerFailure(error.response));
  }
}

export function* getAuthUser() {
  const { response, error } = yield call(authApi.getAuthUser);

  if (response) {
    yield put(getAuthUserSuccess(response.data));
  } else {
    yield put(getAuthUserFailure(error.response));
  }
}

export function* updateAuthUser({
  payload: user,
}: ActionObject<ActionMap, ActionTypes.UPDATE_AUTH_USER_REQUEST>) {
  const { response, error } = yield call(authApi.updateAuthUser, user);

  if (response) {
    yield put(updateAuthUserSuccess(response.data));
  } else {
    yield put(updateAuthUserFailure(error.response));
  }
}

export function* requestPasswordReset({
  payload: { email },
}: ActionObject<ActionMap, ActionTypes.PASSWORD_RESET_REQUEST_REQUEST>) {
  const { response, error } = yield call(authApi.requestResetPassword, email);

  if (response) {
    yield put(passwordResetRequestSuccess({}));
    yield put(
      showNotification(
        'Check your email inbox for a password reset link',
        6000,
        'success',
      ),
    );
  } else {
    yield put(passwordResetRequestFailure(error.response));
  }
}

export function* resetPassword({
  payload: { code, password },
}: ActionObject<ActionMap, ActionTypes.PASSWORD_RESET_REQUEST>) {
  const { response, error } = yield call(authApi.resetPassword, code, password);

  if (response) {
    yield put(resetPasswordSuccess({}));
    yield put(
      showNotification(
        'Your password has been reset successfully',
        6000,
        'success',
      ),
    );
  } else {
    yield put(resetPasswordFailure(error.response));
  }
}

export function* logout() {
  yield call(LocalStorageService.removeAccessToken);
  yield put(push('/'));
}

export default function* authSaga() {
  yield takeLatest(ActionTypes.REGISTER_REQUEST, register);
  yield takeEvery(ActionTypes.LOGIN_REQUEST, login);
  yield takeLatest(ActionTypes.LOGIN_FACEBOOK_REQUEST, loginFacebook);
  yield takeEvery(ActionTypes.GET_AUTH_USER_REQUEST, getAuthUser);
  yield takeEvery(ActionTypes.UPDATE_AUTH_USER_REQUEST, updateAuthUser);
  yield takeEvery(
    ActionTypes.PASSWORD_RESET_REQUEST_REQUEST,
    requestPasswordReset,
  );
  yield takeEvery(ActionTypes.PASSWORD_RESET_REQUEST, resetPassword);
  yield takeEvery(ActionTypes.LOGOUT, logout);
}
