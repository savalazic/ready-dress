import { State } from 'services/state';
import { createLoadingSelector } from 'utils/redux';
import { ActionTypes } from './authActions';

export const getAuth = (state: State) => state.services.auth;
export const getToken = (state: State) => state.services.auth.token;
export const getIsLoggedIn = (state: State): boolean =>
  !!state.services.auth.token || !!state.services.auth.user;

export const getAuthUser = (state: State) => state.services.auth.user;

export const getAuthUserRole = (state: State) =>
  state.services.auth.user && state.services.auth.user.role;

export const getAuthUserLoadingSelector = createLoadingSelector(
  ActionTypes.GET_AUTH_USER_REQUEST,
);

export const updateAuthUserLoadingSelector = createLoadingSelector(
  ActionTypes.UPDATE_AUTH_USER_REQUEST,
);

export const getLoginLoadingSelector = createLoadingSelector(
  ActionTypes.LOGIN_REQUEST,
);

export const getRegisterLoadingSelector = createLoadingSelector(
  ActionTypes.REGISTER_REQUEST,
);

export const getFacebookLoginLoadingSelector = createLoadingSelector(
  ActionTypes.LOGIN_FACEBOOK_REQUEST,
);

export const getRequestPasswordLoadingSelector = createLoadingSelector(
  ActionTypes.PASSWORD_RESET_REQUEST_REQUEST,
);

export const getResetPasswordLoadingSelector = createLoadingSelector(
  ActionTypes.PASSWORD_RESET_REQUEST,
);
