import { createReducer } from 'utils/redux';
import { ActionTypes, ActionMap } from './authActions';

// @TODO: check available roles
export type UserRoleTypes =
  | 'store_manager'
  | 'store_branch_manager'
  | 'customer';

export enum UserRoles {
  storeManager = 'store_manager',
  storeBranchManager = 'store_branch_manager',
  customer = 'customer',
}

export interface AuthUser {
  id: number;
  email: string;
  role: UserRoleTypes;
  first_name: string;
  last_name: string;
}

export interface AuthState {
  token: string | null;
  user: AuthUser | null;
}

export const initialState: AuthState = { token: null, user: null };

const authReducer = createReducer<AuthState, ActionMap>(
  {
    [ActionTypes.LOGIN_SUCCESS]: (state, payload) => ({
      ...state,
      token: payload.token,
    }),
    [ActionTypes.LOGIN_FACEBOOK_SUCCESS]: (state, payload) => ({
      ...state,
      user: payload.user,
    }),
    [ActionTypes.REGISTER_SUCCESS]: state => ({
      ...state,
    }),
    [ActionTypes.GET_AUTH_USER_SUCCESS]: (state, payload) => ({
      ...state,
      user: payload.user,
    }),
    [ActionTypes.UPDATE_AUTH_USER_SUCCESS]: (state, payload) => ({
      ...state,
      user: payload.user,
    }),
    [ActionTypes.LOGOUT]: () => initialState,
  },
  initialState,
);

export default authReducer;
