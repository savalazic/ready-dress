import { AuthUser } from './authReducer';
import { createAction } from 'utils/redux';

export enum ActionTypes {
  LOGIN_REQUEST = 'LOGIN_REQUEST',
  LOGIN_SUCCESS = 'LOGIN_SUCCESS',
  LOGIN_FAILURE = 'LOGIN_FAILURE',

  LOGIN_FACEBOOK_REQUEST = 'LOGIN_FACEBOOK_REQUEST',
  LOGIN_FACEBOOK_SUCCESS = 'LOGIN_FACEBOOK_SUCCESS',
  LOGIN_FACEBOOK_FAILURE = 'LOGIN_FACEBOOK_FAILURE',

  REGISTER_REQUEST = 'REGISTER_REQUEST',
  REGISTER_SUCCESS = 'REGISTER_SUCCESS',
  REGISTER_FAILURE = 'REGISTER_FAILURE',

  GET_AUTH_USER_REQUEST = 'GET_AUTH_USER_REQUEST',
  GET_AUTH_USER_SUCCESS = 'GET_AUTH_USER_SUCCESS',
  GET_AUTH_USER_FAILURE = 'GET_AUTH_USER_FAILURE',

  UPDATE_AUTH_USER_REQUEST = 'UPDATE_AUTH_USER_REQUEST',
  UPDATE_AUTH_USER_SUCCESS = 'UPDATE_AUTH_USER_SUCCESS',
  UPDATE_AUTH_USER_FAILURE = 'UPDATE_AUTH_USER_FAILURE',

  PASSWORD_RESET_REQUEST_REQUEST = 'PASSWORD_RESET_REQUEST_REQUEST',
  PASSWORD_RESET_REQUEST_SUCCESS = 'PASSWORD_RESET_REQUEST_SUCCESS',
  PASSWORD_RESET_REQUEST_FAILURE = 'PASSWORD_RESET_REQUEST_FAILURE',

  PASSWORD_RESET_REQUEST = 'PASSWORD_RESET_REQUEST',
  PASSWORD_RESET_SUCCESS = 'PASSWORD_RESET_SUCCESS',
  PASSWORD_RESET_FAILURE = 'PASSWORD_RESET_FAILURE',

  LOGOUT = 'LOGOUT',
}

export interface ActionMap {
  [ActionTypes.LOGIN_REQUEST]: {
    email: string;
    password: string;
  };
  [ActionTypes.LOGIN_SUCCESS]: { token: string };
  [ActionTypes.LOGIN_FAILURE]: any;
  [ActionTypes.LOGIN_FACEBOOK_REQUEST]: { token: string };
  [ActionTypes.LOGIN_FACEBOOK_SUCCESS]: { user: any };
  [ActionTypes.LOGIN_FACEBOOK_FAILURE]: { error: string };
  [ActionTypes.REGISTER_REQUEST]: {
    first_name: string;
    last_name: string;
    dob: any;
    phone_no: string;
    zipcode: string;
    email: string;
    password: string;
    password_confirmation: string;
  };
  [ActionTypes.REGISTER_SUCCESS]: any;
  [ActionTypes.REGISTER_FAILURE]: any;

  [ActionTypes.GET_AUTH_USER_REQUEST]: any;
  [ActionTypes.GET_AUTH_USER_SUCCESS]: {
    user: AuthUser;
  };
  [ActionTypes.GET_AUTH_USER_FAILURE]: any;

  [ActionTypes.UPDATE_AUTH_USER_REQUEST]: any;
  [ActionTypes.UPDATE_AUTH_USER_SUCCESS]: {
    user: AuthUser;
  };
  [ActionTypes.UPDATE_AUTH_USER_FAILURE]: any;

  [ActionTypes.PASSWORD_RESET_REQUEST_REQUEST]: {
    email: string;
  };
  [ActionTypes.PASSWORD_RESET_REQUEST_SUCCESS]: any;
  [ActionTypes.PASSWORD_RESET_REQUEST_FAILURE]: any;

  [ActionTypes.PASSWORD_RESET_REQUEST]: {
    code: string;
    password: string;
  };
  [ActionTypes.PASSWORD_RESET_SUCCESS]: any;
  [ActionTypes.PASSWORD_RESET_FAILURE]: any;

  [ActionTypes.LOGOUT]: any;
}

export const loginRequest = (email: string, password: string) =>
  createAction<ActionMap, ActionTypes.LOGIN_REQUEST>(ActionTypes.LOGIN_REQUEST)(
    {
      email,
      password,
    },
  );

export const loginSuccess = (token: string) =>
  createAction<ActionMap, ActionTypes.LOGIN_SUCCESS>(ActionTypes.LOGIN_SUCCESS)(
    {
      token,
    },
  );

export const loginFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.LOGIN_FAILURE>(ActionTypes.LOGIN_FAILURE)(
    {
      error,
    },
  );

export const loginFacebook = (token: string) =>
  createAction<ActionMap, ActionTypes.LOGIN_FACEBOOK_REQUEST>(
    ActionTypes.LOGIN_FACEBOOK_REQUEST,
  )({
    token,
  });

export const loginFacebookSuccess = (user: object) =>
  createAction<ActionMap, ActionTypes.LOGIN_FACEBOOK_SUCCESS>(
    ActionTypes.LOGIN_FACEBOOK_SUCCESS,
  )({
    user,
  });

export const loginFacebookFailure = (error: string) => ({
  type: ActionTypes.LOGIN_FACEBOOK_FAILURE,
  payload: { error },
});

export const registerRequest = (
  email: string,
  password: string,
  password_confirmation: string,
  first_name: string,
  last_name: string,
  dob: any,
  phone_no: string,
  zipcode: string,
) =>
  createAction<ActionMap, ActionTypes.REGISTER_REQUEST>(
    ActionTypes.REGISTER_REQUEST,
  )({
    email,
    password,
    password_confirmation,
    first_name,
    last_name,
    dob,
    phone_no,
    zipcode,
  });

export const registerSuccess = createAction<
  ActionMap,
  ActionTypes.REGISTER_SUCCESS
>(ActionTypes.REGISTER_SUCCESS);

export const registerFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.REGISTER_FAILURE>(
    ActionTypes.REGISTER_FAILURE,
  )({
    error,
  });

export const getAuthUserRequest = createAction<
  ActionMap,
  ActionTypes.GET_AUTH_USER_REQUEST
>(ActionTypes.GET_AUTH_USER_REQUEST);

export const getAuthUserSuccess = (user: AuthUser) =>
  createAction<ActionMap, ActionTypes.GET_AUTH_USER_SUCCESS>(
    ActionTypes.GET_AUTH_USER_SUCCESS,
  )({
    user,
  });

export const getAuthUserFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.GET_AUTH_USER_FAILURE>(
    ActionTypes.GET_AUTH_USER_FAILURE,
  )({
    error,
  });

export const updateAuthUserRequest = (user: AuthUser) =>
  createAction<ActionMap, ActionTypes.UPDATE_AUTH_USER_REQUEST>(
    ActionTypes.UPDATE_AUTH_USER_REQUEST,
  )({
    user,
  });

export const updateAuthUserSuccess = (user: AuthUser) =>
  createAction<ActionMap, ActionTypes.UPDATE_AUTH_USER_SUCCESS>(
    ActionTypes.UPDATE_AUTH_USER_SUCCESS,
  )({
    user,
  });

export const updateAuthUserFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.UPDATE_AUTH_USER_FAILURE>(
    ActionTypes.UPDATE_AUTH_USER_FAILURE,
  )({
    error,
  });

export const passwordResetRequestRequest = (email: string) =>
  createAction<ActionMap, ActionTypes.PASSWORD_RESET_REQUEST_REQUEST>(
    ActionTypes.PASSWORD_RESET_REQUEST_REQUEST,
  )({
    email,
  });

export const passwordResetRequestSuccess = createAction<
  ActionMap,
  ActionTypes.PASSWORD_RESET_REQUEST_SUCCESS
>(ActionTypes.PASSWORD_RESET_REQUEST_SUCCESS);

export const passwordResetRequestFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.PASSWORD_RESET_REQUEST_FAILURE>(
    ActionTypes.PASSWORD_RESET_REQUEST_FAILURE,
  )({
    error,
  });

export const resetPasswordRequest = (code: string, password: string) =>
  createAction<ActionMap, ActionTypes.PASSWORD_RESET_REQUEST>(
    ActionTypes.PASSWORD_RESET_REQUEST,
  )({
    code,
    password,
  });

export const resetPasswordSuccess = createAction<
  ActionMap,
  ActionTypes.PASSWORD_RESET_SUCCESS
>(ActionTypes.PASSWORD_RESET_SUCCESS);

export const resetPasswordFailure = (error: string) =>
  createAction<ActionMap, ActionTypes.PASSWORD_RESET_FAILURE>(
    ActionTypes.PASSWORD_RESET_FAILURE,
  )({
    error,
  });

export const logout = () =>
  createAction<ActionMap, ActionTypes.LOGOUT>(ActionTypes.LOGOUT)({});
