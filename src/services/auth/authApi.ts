import api from 'services/api';

const login = (email: string, password: string) => {
  return api
    .post(
      'sign_in',
      { email, password },
      {
        headers: {
          noAuth: true,
        },
      },
    )
    .then(response => ({ response }))
    .catch(error => ({ error }));
};

const loginFacebook = (token: string) =>
  api
    .post(
      'facebook_sign_in',
      {},
      { params: { token }, headers: { noAuth: true } },
    )
    .then(response => ({ response }))
    .catch(error => ({ error }));

const register = (
  email: string,
  password: string,
  password_confirmation: string,
  first_name: string,
  last_name: string,
  dob: any,
  phone_no: string,
  zipcode: string,
) => {
  return api
    .post(
      'sign_up',
      {
        user: {
          email,
          password,
          password_confirmation,
          first_name,
          last_name,
          dob,
          phone_no,
          zipcode,
        },
      },
      { headers: { noAuth: true } },
    )
    .then(response => ({ response }))
    .catch(error => ({ error }));
};

const getAuthUser = () =>
  api
    .get('me')
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const updateAuthUser = (user: any) =>
  api
    .put('me', user)
    .then(({ data }) => ({ response: data }))
    .catch(error => ({ error }));

const requestResetPassword = (email: string) =>
  api
    .get('password_reset_request', { email })
    .then(response => ({ response }))
    .catch(error => ({ error }));

const resetPassword = (code: string, password: string) =>
  api
    .post('password_reset', {
      code,
      password,
    })
    .then(response => ({ response }))
    .catch(error => ({ error }));

export default {
  login,
  register,
  loginFacebook,
  getAuthUser,
  updateAuthUser,
  requestResetPassword,
  resetPassword,
};
