import axios from 'axios';
import { omit } from 'utils/object';

import { API_ROOT } from '../config';
import LocalStorageService from './localStorage';

const defaultOptions = {
  headers: {
    'Content-Type': 'application/json',
  },
};

const getHeaders = ({ headers = {} }: any) => ({
  ...defaultOptions.headers,
  ...headers,
});

export const createQueryString = (params: any) => {
  const paramsKeys = Object.entries(params)
    .filter(param => param[1] !== undefined)
    .map(param => param[0]);
  let queryString = '';
  paramsKeys.forEach((key, index) => {
    queryString += index === 0 ? '?' : '&';
    if (Array.isArray(params[key])) {
      params[key].forEach((el: any, i: any) => {
        if (i > 0) {
          queryString += '&';
        }
        queryString += `${key}[]=${encodeURIComponent(el)}`;
      });
    } else {
      queryString += `${key}=${encodeURIComponent(params[key])}`;
    }
  });
  return queryString;
};

const axiosInstance = axios.create();
axiosInstance.interceptors.request.use(
  config => {
    if (config.headers.noAuth) {
      return { ...config, headers: omit(config.headers, ['noAuth']) };
    }

    const request = config;

    const accessToken = LocalStorageService.getAccessToken();
    if (accessToken) {
      request.headers.common.Authorization = `Bearer ${accessToken}`;
    }

    return request;
  },
  err => Promise.reject(err),
);

axiosInstance.interceptors.response.use(
  response => response,
  error => {
    const { status } = error.response;
    if (status === 401) {
      // @TODO: handle unauthorized
    }
    return Promise.reject(error);
  },
);

const get = (
  resourceUrl: string,
  params?: object,
  resourceId?: string | number,
  options = {},
) => {
  const id = resourceId ? `/${resourceId}` : '';
  const queryParams = params ? createQueryString(params) : '';
  const url = `${API_ROOT}/${resourceUrl + id + queryParams}`;

  return axiosInstance.get(url, options);
};

const post = (resourceUrl: string, newResource: object, options = {}) => {
  const url = `${API_ROOT}/${resourceUrl}`;
  return axiosInstance.post(url, newResource, {
    ...options,
    headers: getHeaders(options),
  });
};

const put = (resourceUrl: string, newResource?: object, options = {}) => {
  const url = `${API_ROOT}/${resourceUrl}`;
  return axiosInstance.put(url, newResource, {
    ...options,
    headers: getHeaders(options),
  });
};

const remove = (resourceUrl: string, resourceId: string | number) => {
  const url = `${API_ROOT}/${resourceUrl}/${resourceId || ''}`;
  return axiosInstance.delete(url);
};

export default {
  get,
  post,
  put,
  remove,
};
