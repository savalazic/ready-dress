import toString from 'lodash/toString';
import reduce from 'lodash/reduce';
import { createSelector } from 'reselect';
import { State } from 'services/state';

import { getStoresSelector } from 'services/store/storeSelectors';

export const getCartItemsSelector = (state: State) => state.services.cart.items;

export const getCartItemsCountSelector = createSelector(
  [getCartItemsSelector],
  cartItems => reduce(cartItems, (acc, val) => acc + val.data.length, 0),
);

export const getStoresForCartSelector = createSelector(
  [getStoresSelector, getCartItemsSelector],
  (stores, cart) =>
    stores.filter((store: any) =>
      Object.keys(cart).includes(toString(store.id)),
    ),
);
