import toString from 'lodash/toString';
import has from 'lodash/has';
import { createReducer } from 'utils/redux';
import { ActionMap, ActionTypes } from './cartActions';
import { StoreItem } from 'services/store/storeReducer';
import {
  ActionMap as AppointmentActionMap,
  ActionTypes as AppointmentActionTypes,
} from 'services/appointment/appointmentActions';
import { omit } from 'utils/object';

export interface CartStoreItem extends StoreItem {
  selectedColor?: number;
}

export interface CartState {
  items: {
    [storeId: number]: {
      id: number;
      name: string;
      data: CartStoreItem[];
      subtotal: number;
    };
  };
}

const initialState: CartState = {
  items: {},
};

const cartReducer = createReducer<CartState, ActionMap & AppointmentActionMap>(
  {
    [ActionTypes.ADD_ITEM]: (state, payload): CartState => {
      if (!has(state.items, payload.storeId)) {
        return {
          ...state,
          items: Object.assign({}, state.items, {
            [payload.storeId]: {
              ...state.items[payload.storeId],
              data: [{ ...payload.item, selectedColor: payload.selectedColor }],
              subtotal: payload.item.price_in_centum,
            },
          }),
        };
      }

      const newCartItems = [
        ...state.items[payload.storeId].data,
        { ...payload.item, selectedColor: payload.selectedColor },
      ];

      return {
        ...state,
        items: Object.assign({}, state.items, {
          [payload.storeId]: {
            ...state.items[payload.storeId],
            data: newCartItems,
            subtotal: newCartItems.reduce(
              (acc, val) => acc + val.price_in_centum,
              0,
            ),
          },
        }),
      };
    },
    [ActionTypes.REMOVE_ITEM]: (state, payload): CartState => {
      if (state.items[payload.storeId].data.length === 1) {
        return {
          ...state,
          items: omit(state.items, [toString(payload.storeId)]),
        };
      }

      const newCartItems = state.items[payload.storeId].data.filter(
        item => item.id !== payload.id,
      );

      return {
        ...state,
        items: Object.assign({}, state.items, {
          [payload.storeId]: {
            ...state.items[payload.storeId],
            data: newCartItems,
            subtotal: newCartItems.reduce(
              (acc, val) => acc + val.price_in_centum,
              0,
            ),
          },
        }),
      };
    },
    [AppointmentActionTypes.SAVE_CC_AND_CREATE_APPOINTMENT_SUCCESS]: (
      state,
    ): CartState => ({
      ...state,
      items: {},
    }),
  },
  initialState,
);

export default cartReducer;
