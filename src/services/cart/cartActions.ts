import { createAction } from 'utils/redux';
import { StoreItem } from 'services/store/storeReducer';

export enum ActionTypes {
  ADD_ITEM = 'CART/ADD_ITEM',
  REMOVE_ITEM = 'CART/REMOVE_ITEM',
}

export interface ActionMap {
  [ActionTypes.ADD_ITEM]: {
    item: StoreItem;
    storeId: number;
    selectedColor: number;
  };
  [ActionTypes.REMOVE_ITEM]: { id: number; storeId: number };
}

export const addItemToCart = (
  item: StoreItem,
  storeId: number,
  selectedColor: number,
) =>
  createAction<ActionMap, ActionTypes.ADD_ITEM>(ActionTypes.ADD_ITEM)({
    item,
    storeId,
    selectedColor,
  });

export const removeItemFromCart = (id: number, storeId: number) =>
  createAction<ActionMap, ActionTypes.REMOVE_ITEM>(ActionTypes.REMOVE_ITEM)({
    id,
    storeId,
  });
