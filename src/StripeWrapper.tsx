import React, { Component } from 'react';

import { StripeProvider } from 'react-stripe-elements';

interface Props {
  apiKey?: string;
}

class StripeWrapper extends Component<Props> {
  state = {
    stripe: null,
  };

  _mounted: boolean = false;

  componentDidMount() {
    this._mounted = true;
    const { apiKey } = this.props;

    const stripeJs = document.createElement('script');
    stripeJs.src = 'https://js.stripe.com/v3/';
    stripeJs.async = true;
    stripeJs.onload = () => {
      if (this._mounted) {
        this.setState({
          stripe: window.Stripe(apiKey),
        });
      }
    };
    // tslint:disable-next-line: no-unused-expression
    document.body && document.body.appendChild(stripeJs);
  }
  render() {
    // this.state.stripe will either be null or a Stripe instance
    // depending on whether Stripe.js has loaded.
    return (
      <StripeProvider stripe={this.state.stripe}>
        {this.props.children}
      </StripeProvider>
    );
  }
}

export default StripeWrapper;
