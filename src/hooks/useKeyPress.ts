import { useEffect } from 'react';

const useKeyPress = (targetKey: number, handler: () => void) => {
  const downHandler = ({ keyCode }: any) => {
    if (keyCode === targetKey) {
      handler();
    }
  };

  useEffect(() => {
    window.addEventListener('keydown', downHandler);

    // Remove event listeners on cleanup
    return () => {
      window.removeEventListener('keydown', downHandler);
    };
  }, []);
};

export default useKeyPress;
