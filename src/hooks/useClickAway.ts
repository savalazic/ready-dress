import { RefObject, useEffect } from 'react';

export const on = (obj: any, ...args: any[]) => obj.addEventListener(...args);

export const off = (obj: any, ...args: any[]) =>
  obj.removeEventListener(...args);

const defaultEvents = ['mousedown', 'click', 'touchstart'];

const useClickAway = (
  ref: RefObject<HTMLElement | null>,
  onClickAway: (event: KeyboardEvent) => void,
  events: string[] = defaultEvents,
) => {
  const handler = (event: any) => {
    const { current: el } = ref;
    if (el && !el.contains(event.target)) {
      onClickAway(event);
    }
  };

  useEffect(() => {
    for (const eventName of events) {
      on(document, eventName, handler);
    }
    return () => {
      for (const eventName of events) {
        off(document, eventName, handler);
      }
    };
  });
};

export default useClickAway;
