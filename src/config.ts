export const API_ROOT = process.env.REACT_APP_BACKEND_HOST;
export const FACEBOOK_APP_ID = process.env.REACT_APP_FACEBOOK_APP_ID || '';
export const STRIPE_PUB_KEY = process.env.REACT_APP_STRIPE_PUB_KEY;
export const STRIPE_SECRET_KEY = process.env.REACT_APP_STRIPE_SECRET_KEY;
