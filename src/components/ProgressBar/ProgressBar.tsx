import React from 'react';
import { Progress, ProgressProps } from 'semantic-ui-react';
import cx from 'classnames';
import './ProgressBar.scss';

const ProgressBar = ({ className, percent, ...props }: ProgressProps) => (
  <Progress
    percent={percent}
    className={cx('ProgressBar', className)}
    {...props}
  />
);

export default ProgressBar;
