import React from 'react';

interface ImageFile extends File {
  preview: string;
  id: number;
}

interface Props {
  image: ImageFile;
  onClick: (img: ImageFile) => void;
  hoverText?: string;
}

const ImagePreview = ({ image, onClick, hoverText }: Props) => (
  <div key={image.preview} className="ImagesUpload__image">
    <div className="ImagesUpload__overlay" onClick={() => onClick(image)}>
      <span>{hoverText || 'Click to remove image'}</span>
    </div>
    <img key={image.preview} src={image.preview} alt="preview" />
  </div>
);

export default ImagePreview;
