import cls from 'classnames';
import React from 'react';

import './Section.scss';

interface Props extends React.HTMLAttributes<HTMLDivElement> {
  children: React.ReactNode;
}

const Section = ({ children, ...rest }: Props) => {
  return <section {...rest}>{children}</section>;
};

type Align = 'left' | 'right';

interface HeadingProps extends React.HTMLAttributes<HTMLDivElement> {
  children?: React.ReactNode;
  className?: string;
  align?: Align;
  width?: string;
}

export const SectionHeading = ({
  children,
  className = '',
  align = 'left',
  width = 'auto',
  ...rest
}: HeadingProps) => (
  <div
    className={cls('SectionHeading', className, {
      'text-right': align === 'right',
    })}
    style={{ maxWidth: width }}
    {...rest}
  >
    <span>{children}</span>
  </div>
);

Section.Heading = SectionHeading;

export default Section;
