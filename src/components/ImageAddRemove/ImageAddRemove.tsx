import React, { useCallback, useState, useEffect } from 'react';
import cx from 'classnames';
import { FieldProps } from 'formik';
import { useDropzone } from 'react-dropzone';

import './ImageAddRemove.scss';

interface Props extends FieldProps {
  label?: string;
  disabled?: boolean;
  accept?: string;
  maxSize?: number;
  initialImages: any[];
  handleImageRemove: (id: number) => void;
  handleImageAdd: (image: File) => void;
}

interface ImageFile extends File {
  preview: string;
  id: number;
}

const ImageAddRemove = ({
  label,
  accept = 'image/*',
  field,
  form: { touched, errors, setFieldValue, values },
  disabled = false,
  maxSize = 3000000,
  initialImages = [],
  handleImageRemove,
  handleImageAdd,
  ...props
}: Props) => {
  const [images, setImages] = useState(initialImages);

  useEffect(() => {
    return () => {
      // Make sure to revoke the data URIs to avoid memory leaks
      images.forEach((image: ImageFile) => URL.revokeObjectURL(image.preview));
    };
  }, [images]);

  const onDrop = useCallback(
    acceptedFiles => {
      setImages([
        ...images,
        ...acceptedFiles.map((image: ImageFile) =>
          Object.assign(image, {
            preview: URL.createObjectURL(image),
          }),
        ),
      ]);

      handleImageAdd(acceptedFiles[0]);

      setFieldValue(field.name, values[field.name].concat(acceptedFiles));
    },
    [field.name, handleImageAdd, images, setFieldValue, values],
  );

  const onRemove = (image: ImageFile) => {
    handleImageRemove(image.id);

    setImages(images.filter((img: ImageFile) => img.preview !== image.preview));
    setFieldValue(
      field.name,
      values[field.name].filter(
        (img: ImageFile) => img.preview !== image.preview,
      ),
    );
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept,
    disabled,
    maxSize,
    multiple: false,
  });

  return (
    <div className="ImageAddRemove">
      <div {...getRootProps()}>
        <div
          className={cx('ImageAddRemove__drop-field', {
            disabled,
          })}
        >
          <input {...getInputProps()} {...props} />
          {isDragActive ? (
            <p>Drop the files here...</p>
          ) : (
            <p>Drag 'n' drop file here, or click to select file</p>
          )}
        </div>
        <label htmlFor={field.name}>{label}</label>
      </div>
      <div>
        {images.length > 0 && (
          <div className="ImageAddRemove__images">
            {images.map((image: any) => (
              <div key={image.preview} className="ImageAddRemove__image">
                <div
                  className="ImageAddRemove__overlay"
                  onClick={() => onRemove(image)}
                >
                  <span>Click to remove image</span>
                </div>
                <img key={image.preview} src={image.preview} alt="preview" />
              </div>
            ))}
          </div>
        )}
      </div>
      {touched[field.name] && errors[field.name] && (
        <div className="ImageAddRemove__error">{errors[field.name]}</div>
      )}
    </div>
  );
};

export default ImageAddRemove;
