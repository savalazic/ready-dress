import isUndefined from 'lodash/isUndefined';
import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { State } from 'services/state';
import { UserRoleTypes } from 'services/auth/authReducer';
import {
  getIsLoggedIn,
  getAuthUserRole,
  getAuthUserLoadingSelector,
} from 'services/auth/authSelectors';

interface Props extends RouteProps {
  isLoggedIn: boolean;
  role: UserRoleTypes;
  userRole: UserRoleTypes | null;
  component: React.ComponentType<any>;
  path: string;
  isUserLoading: boolean;
  isModal?: boolean;
}

const PrivateRoute = ({
  component: Component,
  role = 'customer',
  isLoggedIn,
  userRole,
  isUserLoading,
  path,
  isModal,
  ...rest
}: Props) => {
  const renderComponent = (props: any) =>
    isUndefined(isUserLoading) || isUserLoading ? (
      <></>
    ) : isLoggedIn && role === userRole ? (
      <Component {...props} />
    ) : (
      <Redirect
        to={{
          pathname: '/login',
          state: { isModal: true, backToRoot: true },
        }}
      />
    );
  return <Route path={path} {...rest} render={renderComponent} />;
};

const mapStateToProps = (state: State) => ({
  isLoggedIn: getIsLoggedIn(state),
  userRole: getAuthUserRole(state),
  isUserLoading: getAuthUserLoadingSelector(state),
});

export default connect(mapStateToProps)(PrivateRoute);
