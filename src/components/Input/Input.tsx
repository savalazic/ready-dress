import React from 'react';
import { Input as SemanticInput } from 'semantic-ui-react';
import { FieldProps } from 'formik';
import './Input.scss';

interface Props extends FieldProps {
  label?: string;
  disabled?: boolean;
}

const Input = ({
  label,
  field,
  form: { touched, errors },
  disabled,
  ...props
}: Props) => (
  <div className="Input">
    <SemanticInput
      error={!!errors[field.name]}
      {...field}
      {...props}
      disabled={disabled}
    />
    <label htmlFor={field.name}>{label}</label>
    {touched[field.name] && errors[field.name] && (
      <div className="Input__error">{errors[field.name]}</div>
    )}
  </div>
);

export default Input;
