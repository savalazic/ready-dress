import { Component } from 'react';

interface Props {
  page?: number;
  perPage?: number;
  onLoadMore: (page: number, per_page: number) => void;
  children: any;
}

interface State {
  page: number;
  per_page: number;
}

class Pagination extends Component<Props, State> {
  initialState = {
    page: this.props.page || 1,
    per_page: this.props.perPage || 4,
  };

  state = this.initialState;

  loadMore = () => {
    this.setState(
      prevState => ({
        page: prevState.page + 1,
      }),
      () => {
        this.props.onLoadMore(this.state.page, this.state.per_page);
      },
    );
  };

  render() {
    return this.props.children({
      page: this.state.page,
      per_page: this.state.per_page,
      loadMore: this.loadMore,
    });
  }
}

export default Pagination;
