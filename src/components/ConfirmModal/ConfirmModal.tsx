import React from 'react';
import { Confirm } from 'semantic-ui-react';
import cx from 'classnames';

import './ConfirmModal.scss';

interface Props {
  isModalOpen: boolean;
  handleClose: () => void;
  handleConfirm: () => void;
  confirmButtonText: string;
  contentText: string;
  isLoading?: boolean;
}

const ConfirmModal = ({
  isModalOpen,
  handleClose,
  confirmButtonText,
  contentText,
  handleConfirm,
  isLoading = false,
}: Props) => {
  return (
    <Confirm
      className={cx('ConfirmModal', { 'ConfirmModal--isLoading': isLoading })}
      open={isModalOpen}
      confirmButton={confirmButtonText}
      content={contentText}
      onCancel={handleClose}
      onConfirm={handleConfirm}
    />
  );
};

export default ConfirmModal;
