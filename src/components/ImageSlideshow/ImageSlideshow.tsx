import React, { Component } from 'react';
import Card from 'components/Card';

import './ImageSlideshow.scss';

interface Props extends React.HTMLAttributes<HTMLDivElement> {
  images: string[];
}

interface State {
  imageWidth: number;
}

class ImageSlideshow extends Component<Props, State> {
  public state = { imageWidth: 200 };

  private imageRef: React.RefObject<any> = React.createRef();

  public componentDidMount() {
    window.addEventListener('resize', this.handleResize);

    this.setState({
      imageWidth: this.imageRef.current.offsetParent.clientWidth,
    });
  }

  public handleResize = () => {
    this.setState({
      imageWidth: this.imageRef.current.offsetParent.clientWidth,
    });
  };

  public componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  public render() {
    const { images, ...rest } = this.props;

    return (
      <div
        ref={this.imageRef}
        className="ImageSlideshow"
        style={{ width: this.state.imageWidth * images.length }}
        {...rest}
      >
        {images.map((image, index) => (
          <Card
            key={index}
            style={{
              display: 'inline-block',
              width: this.state.imageWidth,
              height: '100%',
            }}
          >
            <Card.Image className="ImageSlideshow__image" image={image} />
          </Card>
        ))}
      </div>
    );
  }
}

export default ImageSlideshow;
