import cx from 'classnames';
import React from 'react';
import './PrimaryButton.scss';
import Button from '../Button';
import { ButtonProps } from 'semantic-ui-react';

const PrimaryButton = ({ className, children, ...props }: ButtonProps) => (
  <Button className={cx('PrimaryButton', className)} {...props}>
    {children}
  </Button>
);

export default PrimaryButton;
