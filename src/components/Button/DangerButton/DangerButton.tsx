import cx from 'classnames';
import React from 'react';
import './DangerButton.scss';
import Button from '../Button';
import { ButtonProps } from 'semantic-ui-react';

const DangerButton = ({ className, children, ...props }: ButtonProps) => (
  <Button className={cx('DangerButton', className)} {...props}>
    {children}
  </Button>
);

export default DangerButton;
