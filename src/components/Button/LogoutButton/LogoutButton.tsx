import React from 'react';
import { connect } from 'react-redux';
import { logout } from 'services/auth/authActions';
import { ButtonProps } from 'semantic-ui-react';

import Button from '../Button';

const LogoutButton = ({
  handleLogout,
  onClick,
  ...rest
}: ButtonProps & typeof actions) => {
  return (
    <Button
      {...rest}
      onClick={e => {
        if (onClick) {
          onClick(e, rest);
        }
        handleLogout();
      }}
    >
      Logout
    </Button>
  );
};

const actions = {
  handleLogout: logout,
};

export default connect(
  null,
  actions,
)(LogoutButton);
