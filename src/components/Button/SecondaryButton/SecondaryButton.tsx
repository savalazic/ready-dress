import cx from 'classnames';
import React from 'react';
import './SecondaryButton.scss';
import Button from '../Button';
import { ButtonProps } from 'semantic-ui-react';

const SecondaryButton = ({ className, children, ...props }: ButtonProps) => (
  <Button className={cx('SecondaryButton', className)} {...props}>
    {children}
  </Button>
);

export default SecondaryButton;
