import React from 'react';
import { Button as SemanticButton, ButtonProps } from 'semantic-ui-react';
import './Button.scss';
import cx from 'classnames';

const Button = ({ className, type, children, ...props }: ButtonProps) => (
  <SemanticButton type={type} className={cx('Button', className)} {...props}>
    {children}
  </SemanticButton>
);

export default Button;
