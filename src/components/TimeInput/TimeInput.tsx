import React from 'react';
import { TimeInput as SemanticTimeInput } from 'semantic-ui-calendar-react';
import { FieldProps } from 'formik';
import './TimeInput.scss';

interface Props extends FieldProps {
  label?: string;
  disabled?: string;
  inlineEditing?: boolean;
}

const TimeInput = ({
  label,
  field,
  form: { touched, errors, setFieldValue, setFieldTouched },
  disabled,
  inlineEditing = false,
  ...props
}: Props) => {
  return (
    <div className="TimeInput">
      <SemanticTimeInput
        {...field}
        {...props}
        value={field.value}
        disableMinute
        timeFormat="AMPM"
        disable={disabled}
        onKeyPress={(e: any) => {
          if (!inlineEditing) {
            e.preventDefault();
          }
        }}
        onChange={(e: any, { value }: any) => {
          setFieldTouched(field.name, true);
          setFieldValue(field.name, value);
        }}
      />
      <label htmlFor={field.name}>{label}</label>
      {touched[field.name] && errors[field.name] && (
        <div className="TimeInput__error">{errors[field.name]}</div>
      )}
    </div>
  );
};

export default TimeInput;
