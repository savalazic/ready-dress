import React, { useState } from 'react';
import ReactCrop, { Crop } from 'react-image-crop';

import BaseModal from 'components/BaseModal';
import Button, { DangerButton, PrimaryButton } from 'components/Button';
import { Modal } from 'semantic-ui-react';
import ImagePreview from 'components/ImagePreview';

import 'react-image-crop/dist/ReactCrop.css';

import './ImageCrop.scss';

const initialCrop: Crop = {
  aspect: 1,
};

interface ImageFile extends File {
  preview: string;
  id: number;
}

interface Props {
  image: ImageFile;
  onCrop: (oldImg: ImageFile, newImg: string) => void;
  onDelete: (img: ImageFile) => void;
}

function getCroppedImg(img: HTMLImageElement, crop: Required<Crop>) {
  const canvas = document.createElement('canvas');
  const scaleX = img.naturalWidth / img.width;
  const scaleY = img.naturalHeight / img.height;
  canvas.width = crop.width;
  canvas.height = crop.height;
  const ctx = canvas.getContext('2d');

  ctx!.drawImage(
    img,
    crop.x * scaleX,
    crop.y * scaleY,
    crop.width * scaleX,
    crop.height * scaleY,
    0,
    0,
    crop.width,
    crop.height,
  );

  return canvas.toDataURL('image/jpeg');
}

// @NOTE
// function to be used when editing crossorigin images
// (ie image with src = https://asdf.aws.com/whatever or whatever url from internet)
// it does not work, and I don't know why
// so the implementation is that you cannot edit crop once you upload it - not a good solution
// maybe we need to configure something on aws or maybe something else will work
// also note that <ReactCrop /> does have crossorigin attribute that should make this obsolete
// but as you've guessed it also does not work, and the image cannot be loaded that way

// async function copyImage(image: HTMLImageElement) {
//   const copy = new Image();
//   copy.crossOrigin = 'anonymous';
//   copy.src = image.src;
//   return new Promise<HTMLImageElement>(resolve => {
//     copy.onload = () => resolve(copy);
//   });
// }

const ImageCrop = ({ image, onCrop, onDelete }: Props) => {
  const [crop, setCrop] = useState(initialCrop);
  const [img, setImg] = useState<HTMLImageElement | null>(null);
  const [loading, setLoading] = useState(false);

  // See @NOTE above
  const canCrop = !image.preview.startsWith('http');

  const saveCrop = () => {
    if (img) {
      setLoading(true);
      // const newImg = await copyImage(img);
      const croppedImage = getCroppedImg(img, crop as Required<Crop>);
      setLoading(false);
      onCrop(image, croppedImage);
    }
  };

  if (!canCrop) {
    return (
      <div className="ImageCrop">
        <ImagePreview image={image} onClick={() => onDelete(image)} />
      </div>
    );
  }

  return (
    <BaseModal>
      {({ isModalOpen, openModal, closeModal }) => (
        <>
          <div className="ImageCrop">
            <ImagePreview
              image={image}
              onClick={openModal}
              hoverText="Click to crop image"
            />
          </div>
          <Modal open={isModalOpen} onClose={closeModal} closeIcon>
            <Modal.Content>
              <div className="ImageCrop__crop-container">
                <ReactCrop
                  src={image.preview}
                  crop={crop}
                  onChange={newCrop => setCrop(newCrop)}
                  onImageLoaded={htmlImg => setImg(htmlImg)}
                />
              </div>
            </Modal.Content>
            <Modal.Actions>
              <div className="ImageCrop__buttons">
                <DangerButton
                  className="ImageCrop__button"
                  onClick={() => onDelete(image)}
                >
                  Delete
                </DangerButton>
                <Button className="ImageCrop__button" onClick={closeModal}>
                  Cancel
                </Button>
                <PrimaryButton
                  className="ImageCrop__button"
                  loading={loading}
                  onClick={() => {
                    saveCrop();
                    closeModal();
                  }}
                >
                  Save
                </PrimaryButton>
              </div>
            </Modal.Actions>
          </Modal>
        </>
      )}
    </BaseModal>
  );
};
export default ImageCrop;
