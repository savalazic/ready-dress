import React from 'react';
import { DateInput as SemanticDateInput } from 'semantic-ui-calendar-react';
import { FieldProps } from 'formik';
import moment from 'moment';
import './DateInput.scss';

interface Props extends FieldProps {
  label?: string;
  disabled?: string;
  inlineEditing?: boolean;
}

const DateInput = ({
  label,
  field,
  form: { touched, errors, setFieldValue, setFieldTouched },
  disabled,
  inlineEditing = false,
  ...props
}: Props) => {
  return (
    <div className="DateInput">
      <SemanticDateInput
        {...field}
        {...props}
        value={field.value}
        disable={disabled}
        onKeyPress={(e: any) => {
          if (!inlineEditing) {
            e.preventDefault();
          }
        }}
        onChange={(e: any, { value }: any) => {
          setFieldTouched(field.name, true);
          setFieldValue(field.name, moment(value, 'DD-MM-YYYY'));
        }}
      />
      <label htmlFor={field.name}>{label}</label>
      {touched[field.name] && errors[field.name] && (
        <div className="DateInput__error">{errors[field.name]}</div>
      )}
    </div>
  );
};

export default DateInput;
