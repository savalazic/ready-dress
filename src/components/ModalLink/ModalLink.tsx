import React from 'react';
import cx from 'classnames';
import { NavLink, NavLinkProps } from 'react-router-dom';

import './ModalLink.scss';

export interface ModalLinkProps extends NavLinkProps {
  to: string;
  disabled?: boolean;
}

const ModalLink = ({
  to,
  className,
  disabled = false,
  ...rest
}: ModalLinkProps) => (
  <NavLink
    className={cx('ModalLink', className, {
      disabled,
    })}
    to={{
      pathname: to,
      state: { isModal: true },
    }}
    {...rest}
  />
);

export default ModalLink;
