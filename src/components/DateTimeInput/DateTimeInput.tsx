import React from 'react';
import { DateTimeInput as SemanticDateTimeInput } from 'semantic-ui-calendar-react';
import { FieldProps } from 'formik';
import moment from 'moment';
// import './DateInput.scss';

interface Props extends FieldProps {
  label?: string;
  disabled?: string;
  inlineEditing?: boolean;
  formatInput?: string;
  timeFormat?: 'ampm' | 'AMPM' | '24';
}

const DateInput = ({
  label,
  field,
  form: { touched, errors, setFieldValue },
  disabled,
  inlineEditing = false,
  formatInput = 'YYYY-MM-DD HH:mm',
  ...props
}: Props) => {
  return (
    <div className="DateInput">
      <SemanticDateTimeInput
        {...field}
        {...props}
        value={field.value}
        disable={disabled}
        dateFormat="YYYY-MM-DD"
        onClear={() => setFieldValue(field.name, null)}
        onKeyPress={(e: any) => {
          if (!inlineEditing) {
            e.preventDefault();
          }
        }}
        onChange={(e: any, { value }: any) => {
          setFieldValue(
            field.name,
            moment(value, formatInput).format(formatInput),
          );
        }}
      />
      <label htmlFor={field.name}>{label}</label>
      {errors[field.name] && (
        <div className="DateInput__error">{errors[field.name]}</div>
      )}
    </div>
  );
};

export default DateInput;
