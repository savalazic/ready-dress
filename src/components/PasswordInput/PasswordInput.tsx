import React from 'react';
import { FieldProps } from 'formik';

import Input from 'components/Input';

const PasswordInput = ({ field, ...props }: FieldProps) => (
  // @ts-ignore
  <Input {...props} field={{ ...field, type: 'password' }} />
);

export default PasswordInput;
