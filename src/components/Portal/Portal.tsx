import React, { Component } from 'react';
import { createPortal } from 'react-dom';

interface PortalProps {
  children: React.ReactNode;
}

class Portal extends Component<PortalProps> {
  render() {
    return createPortal(this.props.children, document.body);
  }
}

export default Portal;
