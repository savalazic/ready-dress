import cls from 'classnames';
import React, { Component, HTMLAttributes } from 'react';

import Hanger from 'components/Hanger';

import './Card.scss';

interface Props extends HTMLAttributes<HTMLDivElement> {
  onClick?: () => {};
  size?: number;
  className?: string;
  children?: React.ReactNode;
}

interface ImageProps extends HTMLAttributes<HTMLDivElement> {
  image: string;
}

interface BodyProps extends HTMLAttributes<HTMLDivElement> {
  children: React.ReactNode;
}

class Card extends Component<Props> {
  public static Image = ({ image, ...rest }: ImageProps) => (
    <div
      className="Card__image"
      style={{ backgroundImage: `url(${image})` }}
      {...rest}
    >
      <div className="Card__overlay">
        <Hanger />
      </div>
    </div>
  );

  public static Body = ({ children, ...rest }: BodyProps) => (
    <div className="Card__body" {...rest}>
      {children}
    </div>
  );

  public render() {
    const { onClick, size, children, className, ...rest } = this.props;

    return (
      <div
        className={cls('Card', className)}
        onClick={onClick}
        style={{ width: size, height: size }}
        {...rest}
      >
        {React.Children.map(children, (child: any) =>
          React.cloneElement(child, { size }),
        )}
      </div>
    );
  }
}

export default Card;
