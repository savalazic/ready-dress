import React from 'react';
import { connect } from 'react-redux';
import { UserRoleTypes } from 'services/auth/authReducer';
import { getAuthUserRole } from 'services/auth/authSelectors';
import { State } from 'services/state';

interface Props {
  role: UserRoleTypes;
  userRole: UserRoleTypes | null;
  children: React.ReactNode | null;
}

const CanView = ({ role = 'customer', userRole, children }: Props) => {
  if (role === userRole) {
    return <React.Fragment>{children}</React.Fragment>;
  }

  return null;
};

const mapStateToProps = (state: State) => ({
  userRole: getAuthUserRole(state),
});

export default connect(mapStateToProps)(CanView);
