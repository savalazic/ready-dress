import React, { useState } from 'react';
import { FieldProps } from 'formik';
import { Input as SemanticInput } from 'semantic-ui-react';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
  geocodeByPlaceId,
} from 'react-places-autocomplete';

import './PlacesAutocomplete.scss';
import { findPlacePropertyByType, PlaceProperty } from './helpers';

interface IProps extends FieldProps {
  label?: string;
  placeholder?: string;
  initialAddress?: string;
}

const PlacesAutocompleteInput: React.SFC<IProps> = ({
  field: { name, value, ...field },
  form: { touched, errors, setFieldTouched, setFieldError, setFieldValue },
  placeholder,
  label,
  initialAddress = '',
  ...rest
}) => {
  const [address, setAddress] = useState(value);
  const [isLoading, setLoading] = useState(false);

  const handleChange = (value: any) => {
    setAddress(value);
    setFieldValue(name, '');
    setFieldTouched(name);
  };

  const handleSelect = (address: string, placeId: string) => {
    setAddress(address);

    setLoading(true);

    const promises = Promise.all([
      geocodeByAddress(address).then(results => getLatLng(results[0])),
      geocodeByPlaceId(placeId),
    ]);

    promises
      .then(([geoPosition, geoData]: any) => {
        const placeData = {
          address,
          street_name: findPlacePropertyByType(
            geoData[0].address_components,
            PlaceProperty.StreetName,
          ),
          street_number: findPlacePropertyByType(
            geoData[0].address_components,
            PlaceProperty.StreeNumber,
          ),
          city: findPlacePropertyByType(
            geoData[0].address_components,
            PlaceProperty.City,
          ),
          state: findPlacePropertyByType(
            geoData[0].address_components,
            PlaceProperty.State,
          ),
          lon: geoPosition.lng,
          lat: geoPosition.lat,
        };

        setLoading(false);
        setFieldValue(name, placeData);
      })
      .catch(() => {
        setFieldError(name, 'Error');
        setLoading(false);
      });
  };

  const handleError = () => {
    setFieldError(name, 'No place found');
  };

  return (
    <PlacesAutocomplete
      value={address}
      onChange={handleChange}
      onSelect={handleSelect}
      onError={handleError}
      {...rest}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <div className="PlacesAutocomplete Input">
          <SemanticInput
            {...getInputProps({
              placeholder: 'Search Places ...',
            })}
            loading={loading}
            disabled={isLoading}
            error={!!errors[name]}
          />
          <label htmlFor={name}>{label}</label>
          {touched[name] && errors[name] && (
            <div className="Input__error">{errors[name]}</div>
          )}
          <div className="PlacesAutocomplete__dropdown">
            {suggestions.map((suggestion: any) => {
              const className = suggestion.active
                ? 'PlacesAutocomplete__item PlacesAutocomplete__item--active'
                : 'PlacesAutocomplete__item';
              return (
                <div
                  {...getSuggestionItemProps(suggestion, {
                    className,
                  })}
                  key={suggestion.id}
                >
                  <span>{suggestion.description}</span>
                </div>
              );
            })}
          </div>
        </div>
      )}
    </PlacesAutocomplete>
  );
};

export default PlacesAutocompleteInput;
