import find from 'lodash/find';

export enum PlaceProperty {
  StreetName = 'route',
  StreeNumber = 'street_number',
  City = 'locality',
  State = 'country',
}

export const findPlacePropertyByType = (
  addressComponents: any[],
  type: PlaceProperty,
) => {
  const foundAddress = find(addressComponents, (address: any) =>
    address.types.includes(type),
  );

  if (foundAddress) {
    return foundAddress.long_name;
  }

  return null;
};
