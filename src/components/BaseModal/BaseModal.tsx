import { useState } from 'react';

interface Props {
  children: (params: {
    isModalOpen: boolean;
    openModal: () => void;
    closeModal: () => void;
  }) => any;
}

const BaseModal = ({ children }: Props) => {
  const [isModalOpen, setModalOpen] = useState(false);

  const openModal = () => setModalOpen(true);
  const closeModal = () => setModalOpen(false);

  return children({
    isModalOpen,
    openModal,
    closeModal,
  });
};

export default BaseModal;
