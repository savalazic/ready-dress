import React from 'react';
import cx from 'classnames';
import { Dimmer, Loader } from 'semantic-ui-react';

import './LoadingContainer.scss';

interface Props {
  isLoading: boolean;
  className?: string;
  children?: React.ReactNode;
}

const LoadingContainer = ({ isLoading, className, children }: Props) => (
  <div className={cx(className, { LoadingContainer: isLoading })}>
    <Dimmer active={isLoading} inverted>
      <Loader />
    </Dimmer>
    {children}
  </div>
);

export default LoadingContainer;
