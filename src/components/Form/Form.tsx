import React from 'react';
import { Form as FormikForm, FormikFormProps } from 'formik';

const Form = (props: FormikFormProps) => <FormikForm {...props} />;

export default Form;
