import React, { useCallback, useState, useEffect } from 'react';
import cx from 'classnames';
import { FieldProps } from 'formik';
import { useDropzone } from 'react-dropzone';

import './ImagesUpload.scss';
import ImageCrop from 'components/ImageCrop';
import ImagePreview from 'components/ImagePreview';

interface Props extends FieldProps {
  label?: string;
  disabled?: boolean;
  accept?: string;
  maxSize?: number;
  initialImages: any[];
  allowCrop?: boolean;
}

interface ImageFile extends File {
  preview: string;
}

const ImagesUpload = ({
  label,
  accept = 'image/*',
  field,
  form: { touched, errors, setFieldValue, values },
  disabled = false,
  maxSize = 3000000,
  initialImages = [],
  allowCrop,
  ...props
}: Props) => {
  const [images, setImages] = useState(initialImages);

  useEffect(() => {
    return () => {
      // Make sure to revoke the data URIs to avoid memory leaks
      images.forEach((image: ImageFile) => URL.revokeObjectURL(image.preview));
    };
  }, [images]);

  const onDrop = useCallback(
    acceptedFiles => {
      setImages([
        ...images,
        ...acceptedFiles.map((image: ImageFile) =>
          Object.assign(image, {
            preview: URL.createObjectURL(image),
          }),
        ),
      ]);

      setFieldValue(field.name, values[field.name].concat(acceptedFiles));
    },
    [field.name, images, setFieldValue, values],
  );

  const onRemove = (image: ImageFile) => {
    setImages(images.filter((img: ImageFile) => img.preview !== image.preview));
    setFieldValue(
      field.name,
      values[field.name].filter(
        (img: ImageFile) => img.preview !== image.preview,
      ),
    );
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept,
    disabled,
    maxSize,
  });

  const onCrop = (oldImage: ImageFile, newImg: string) => {
    const replaceImg = (img: ImageFile) =>
      oldImage.preview === img.preview ? { ...img, preview: newImg } : img;
    setImages(images.map(replaceImg));
    setFieldValue(field.name, values[field.name].map(replaceImg));
  };

  return (
    <div className="ImagesUpload">
      <div {...getRootProps()}>
        <div
          className={cx('ImagesUpload__drop-field', {
            disabled,
          })}
        >
          <input {...getInputProps()} {...props} />
          {isDragActive ? (
            <p>Drop the files here...</p>
          ) : (
            <p>Drag 'n' drop some files here, or click to select files</p>
          )}
        </div>
        <label htmlFor={field.name}>{label}</label>
      </div>
      <div>
        {images.length > 0 && (
          <div className="ImagesUpload__images">
            {images.map((image: any) =>
              allowCrop ? (
                <ImageCrop
                  image={image}
                  onCrop={onCrop}
                  onDelete={onRemove}
                  key={image.preview}
                />
              ) : (
                <ImagePreview
                  key={image.preview}
                  image={image}
                  onClick={onRemove}
                />
              ),
            )}
          </div>
        )}
      </div>
      {touched[field.name] && errors[field.name] && (
        <div className="ImagesUpload__error">{errors[field.name]}</div>
      )}
    </div>
  );
};

export default ImagesUpload;
