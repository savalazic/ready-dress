import React from 'react';
import { Dropdown as SemanticDropdown } from 'semantic-ui-react';
import { FieldProps } from 'formik';

import './DropdownInput.scss';

interface Props extends FieldProps {
  label?: string;
  disabled?: boolean;
  options: any;
}

const DropdownInput = ({
  label,
  field,
  form: { touched, errors, setFieldValue },
  options,
  disabled,
  ...props
}: Props) => (
  <div className="DropdownInput">
    <SemanticDropdown
      selection
      options={options}
      error={!!errors[field.name]}
      value={field.value}
      onChange={(e: any, { value }: any) => setFieldValue(field.name, value)}
      disabled={disabled}
      {...props}
    />
    <label htmlFor={field.name}>{label}</label>
  </div>
);

export default DropdownInput;
