import React from 'react';
import { TextArea as SemanticTextArea } from 'semantic-ui-react';
import { FieldProps } from 'formik';

import './TextArea.scss';

interface Props extends FieldProps {
  label?: string;
  disabled?: boolean;
}

const TextArea = ({
  label,
  field,
  form: { touched, errors },
  disabled,
  ...props
}: Props) => (
  <div className="TextArea">
    <SemanticTextArea
      error={!!errors[field.name]}
      {...field}
      {...props}
      disabled={disabled}
    />
    <label htmlFor={field.name}>{label}</label>
    {touched[field.name] && errors[field.name] && (
      <div className="TextArea__error">{errors[field.name]}</div>
    )}
  </div>
);

export default TextArea;
