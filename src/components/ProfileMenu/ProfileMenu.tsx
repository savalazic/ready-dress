import React from 'react';
import { Dropdown, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';

import { getAuthUser } from 'services/auth/authSelectors';
import { logout } from 'services/auth/authActions';
import { State } from 'services/state';

import './ProfileMenu.scss';
import ModalLink from 'components/ModalLink';

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;
type Props = DispatchProps & StateProps;

const ProfileMenu = ({ user, handleLogout }: Props) => {
  if (user === null) {
    return null;
  }

  const trigger = (
    <div className="Nav__menuItem Nav__profile">
      <Icon name="user" />
      <span>Hi, {user.first_name}</span>
    </div>
  );

  return (
    <Dropdown className="ProfileMenu__dropdown" trigger={trigger} icon={null}>
      <Dropdown.Menu>
        {user.role === 'store_branch_manager' && (
          <Dropdown.Item as={ModalLink} to="/menu/manager">
            Menu
          </Dropdown.Item>
        )}
        {user.role === 'customer' && (
          <Dropdown.Item as={ModalLink} to="/menu/customer">
            Menu
          </Dropdown.Item>
        )}
        <Dropdown.Item onClick={handleLogout}>Logout</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};

const actions = {
  handleLogout: logout,
};

const mapStateToProps = (state: State) => ({
  user: getAuthUser(state),
});

export default connect(
  mapStateToProps,
  actions,
)(ProfileMenu);
