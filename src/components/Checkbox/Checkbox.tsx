import React from 'react';
import { Checkbox as SemanticCheckbox } from 'semantic-ui-react';
import { FieldProps } from 'formik';
import './Checkbox.scss';

interface Props extends FieldProps {
  label?: string;
  disabled?: boolean;
}

const Checkbox = ({
  label,
  field,
  form: { touched, errors, setFieldValue },
  disabled,
  ...props
}: Props) => (
  <div className="Checkbox">
    <SemanticCheckbox
      onClick={(e: any, { value }: any) => {
        setFieldValue(field.name, !value);
      }}
      error={!!errors[field.name]}
      {...field}
      {...props}
      disabled={disabled}
    />
    <label htmlFor={field.name}>{label}</label>
    {touched[field.name] && errors[field.name] && (
      <div className="Checkbox__error">{errors[field.name]}</div>
    )}
  </div>
);

export default Checkbox;
