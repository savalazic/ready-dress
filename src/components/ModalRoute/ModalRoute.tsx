import React from 'react';
import { Route, RouteProps } from 'react-router-dom';
import HangerModal from 'components/HangerModal';
import { usePreviousLocation } from 'components/ModalSwitch/ModalSwitch';

interface Props extends RouteProps {
  component: React.ComponentType<any>;
  hasBack?: boolean;
}

const ModalRoute = ({ component: Component, hasBack, ...rest }: Props) => {
  const previousLocation = usePreviousLocation();
  return (
    <Route
      {...rest}
      render={({
        history,
        location: { state = {}, ...location },
        ...restComponentProps
      }) => {
        const componentProps = {
          history,
          location: { ...location, state: { ...state } },
          ...restComponentProps,
        };
        if (state.isModal) {
          return (
            // @ts-ignore
            <HangerModal
              open
              hasBack={hasBack}
              onClose={() => {
                if (previousLocation) {
                  history.push(previousLocation);
                } else {
                  history.push('/');
                }
                // history.replace(location.pathname)
              }}
              onBack={history.goBack}
            >
              <Component {...componentProps} />
            </HangerModal>
          );
        }
        return <Component {...componentProps} />;
      }}
    />
  );
};

export default ModalRoute;
