import React from 'react';

const CheckMarkIcon = () => {
  return (
    <svg
      width="29"
      height="21"
      viewBox="0 0 29 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.90319 19.8202L1.17259 10.7416C0.94247 10.5015 0.94247 10.1136 1.17259 9.87353C1.40341 9.63422 1.77718 9.6335 2.0073 9.87353L10.3202 18.5184L26.9927 1.18003C27.2235 0.939991 27.5966 0.939991 27.8274 1.18003C28.0575 1.41934 28.0575 1.80804 27.8274 2.04807L10.7372 19.8202C10.6221 19.9398 10.4715 20 10.3202 20C10.1689 20 10.0182 19.9398 9.90319 19.8202Z"
        fill="white"
        stroke="white"
        strokeWidth="2"
      />
    </svg>
  );
};

export default CheckMarkIcon;
