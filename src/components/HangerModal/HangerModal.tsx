import React, { useRef } from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';

import Hanger from 'components/Hanger';
import ProfileMenu from 'components/ProfileMenu';
import Portal from 'components/Portal';

import { State } from 'services/state';
import { getAuthUser } from 'services/auth/authSelectors';

import './HangerModal.scss';
import HangerModalBar from './HangerModalBar';
import useClickAway from 'hooks/useClickAway';
import useKeyPress from 'hooks/useKeyPress';
import BackIcon from 'components/BackIcon';
import { RouteComponentProps } from 'react-router';

const ESC_KEY = 27;

interface HangerModalProps extends RouteComponentProps {
  hasBack?: boolean;
  open: boolean;
  className?: string;
  children: React.ReactNode;
  onClose: () => void;
  onBack: () => void;
}

type StateProps = ReturnType<typeof mapStateToProps>;
type Props = StateProps & HangerModalProps;

const HangerModal = ({
  open,
  onClose,
  className,
  children,
  user,
  hasBack,
  onBack,
}: Props) => {
  const modalRef = useRef<HTMLDivElement>(null);
  const backRef = useRef<HTMLDivElement>(null);
  const modalBackdropRef = useRef<HTMLDivElement>(null);

  useClickAway(modalRef, (e: any) => {
    // don't hide modal if we don't click on backdrop
    if (
      modalBackdropRef.current &&
      !modalBackdropRef.current.contains(e.target)
    ) {
      return;
    }
    if (!hasBack) {
      onClose();
    }
    if (backRef.current && !backRef.current.contains(e.target)) {
      onClose();
    }
  });

  // TODO: a way to avoid hiding if the modal is not focused
  useKeyPress(ESC_KEY, () => {
    onClose();
  });

  return (
    <Portal>
      {open && (
        <div className={cx('HangerModal', className)} ref={modalBackdropRef}>
          <Hanger width="110" height="40" />
          {hasBack && (
            <div
              ref={backRef}
              onClick={onBack}
              className="HangerModal__back-icon"
            >
              <BackIcon />
            </div>
          )}
          <div ref={modalRef}>
            <HangerModalBar className="HangerModal__bar">
              {user && <ProfileMenu />}
            </HangerModalBar>
            <div
              className={cx(
                'HangerModal__content',
                className && `${className}__content`,
              )}
            >
              {children}
            </div>
          </div>
        </div>
      )}
    </Portal>
  );
};

const mapStateToProps = (state: State) => ({
  user: getAuthUser(state),
});

export default connect(mapStateToProps)(HangerModal);
