import React from 'react';
import cx from 'classnames';

interface Props {
  className?: string;
  children?: React.ReactNode;
}

const HangerModalBar = ({ className, children }: Props) => {
  return (
    <div className={cx(className)}>
      <svg
        width="50000"
        height="23"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M-0.5 11.5H5000"
          stroke="url(#paint0_linear)"
          strokeWidth="23"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <defs>
          <linearGradient
            id="paint0_linear"
            x1="1279"
            y1="13"
            x2="1279"
            y2="10"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#555555" />
            <stop offset="0.653111" stopColor="white" />
            <stop offset="1" stopColor="#979797" />
          </linearGradient>
          <linearGradient
            id="paint-blue_linear"
            x1="1279"
            y1="13"
            x2="1279"
            y2="10"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#3cd1dc" />
            <stop offset="0.653111" stopColor="white" />
            <stop offset="1" stopColor="#3cd1dc" />
          </linearGradient>
        </defs>
      </svg>
      {children}
    </div>
  );
};

export default HangerModalBar;
