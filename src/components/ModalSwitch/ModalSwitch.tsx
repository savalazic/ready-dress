import React, { Component, useContext } from 'react';
import { Switch, RouteComponentProps, withRouter } from 'react-router';
import { Location } from 'history';
// import AllModalRoutes from 'App/Main/AllModalRoutes';

const ModalContext = React.createContext('/');

export const usePreviousLocation = () => useContext(ModalContext);

interface IState {
  location: string;
}

class ModalSwitch extends Component<RouteComponentProps, IState> {
  state = {
    location: '',
  };

  previousLocation: Location<any> | undefined = undefined;

  getIsModalState(): boolean {
    const { location } = this.props;
    return location && location.state && location.state.isModal;
  }

  getLocationState(): Location<any> | undefined {
    const { location } = this.props;

    const isModal = this.getIsModalState();

    // if the route is modal we should display previous route
    if (isModal) {
      return this.previousLocation;
    }

    // update to new location
    if (this.previousLocation !== location) {
      // TODO: @savalazic @vojvodics check if we can put previous location in state
      // I'm not sure if it can be asyncronous
      // It is also called and updated on render, maybe we should put it to getDerivedStateFromProps
      this.previousLocation = location;
      this.setState({ location: location.pathname });
    }

    return this.previousLocation;
  }

  render() {
    const location = this.getLocationState();
    const isModal = this.getIsModalState();

    return (
      <ModalContext.Provider value={this.state.location}>
        <Switch location={location}>
          {React.Children.map(
            this.props.children as React.ReactElement<any>,
            child => {
              // @NOTE: need to check this logic
              // if (
              //   child.type === AllModalRoutes.prototype.constructor &&
              //   !isModal
              // ) {
              //   return null;
              // }
              return child;
            },
          )}
        </Switch>
        {isModal &&
          // for the case of arriving on that route for the first time
          this.previousLocation !== undefined &&
          this.props.children}
      </ModalContext.Provider>
    );
  }
}

export default withRouter(ModalSwitch);
