import includes from 'lodash/includes';
import transform from 'lodash/transform';
import isEqual from 'lodash/isEqual';
import isObject from 'lodash/isObject';

export function omit(originalObject: any, keys: string[] = []) {
  const newObject: any = {};
  for (const key in originalObject) {
    if (includes(keys, key)) {
      continue;
    }
    if (!Object.prototype.hasOwnProperty.call(originalObject, key)) {
      continue;
    }
    newObject[key] = originalObject[key];
  }
  return newObject;
}

export function difference(object: any, base: any) {
  function changes(obj: any, baze: any) {
    return transform(obj, (acc: any, value: any, key: any) => {
      if (!isEqual(value, baze[key])) {
        acc[key] =
          isObject(value) && isObject(baze[key])
            ? changes(value, baze[key])
            : value;
      }
    });
  }
  return changes(object, base);
}

export function flatten(data: any): any {
  if (!data) {
    throw Error(
      `flattenObj function expects an Object, received ${typeof data}`,
    );
  }
  return Object.keys(data).reduce((acc, curr) => {
    const objValue = data[curr];
    const ret =
      objValue && objValue instanceof Object
        ? flatten(objValue)
        : { [curr]: objValue };
    return Object.assign(acc, ret);
  }, {});
}

export function unflatten(data: any) {
  const result = {};
  // tslint:disable-next-line: forin
  for (const i in data) {
    const keys = i.split('.');
    keys.reduce((r: any, e: any, j: any) => {
      return (
        r[e] ||
        (r[e] = isNaN(Number(keys[j + 1]))
          ? // tslint:disable-next-line: triple-equals
            keys.length - 1 == j
            ? data[i]
            : {}
          : [])
      );
    }, result);
  }
  return result;
}
