export const getPrevPath = (currentPath: string, depth: number) =>
  currentPath
    .split('/')
    .slice(0, depth)
    .join('/');

export const fileToBase64 = (
  file: File & { preview?: string },
): Promise<string> =>
  new Promise((resolve, reject) => {
    if (file.preview && file.preview.startsWith('data:')) {
      resolve(file.preview.split(',')[1]);
    } else {
      const reader: any = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result.split(',')[1]);
      reader.onerror = (error: any) => reject(error);
    }
  });
