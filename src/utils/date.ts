export function formatDate(date: Date) {
  return new Intl.DateTimeFormat('en-US', {
    day: '2-digit',
    month: 'short',
  }).format(date);
}

export function formatTime(date: Date) {
  return new Intl.DateTimeFormat('en-US', { hour: '2-digit' }).format(date);
}
