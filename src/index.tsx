import 'intersection-observer'; // polyfill
import 'semantic-ui-css/semantic.min.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import { Loader } from 'semantic-ui-react';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';

import { STRIPE_PUB_KEY } from 'config';

import { ActionTypes } from 'services/auth/authActions';
import LocalStorageService from 'services/localStorage';

import App from './App';
import Notifications from './App/Notifications';

import './index.scss';

import createReducers from './reducers';
import sagas from './sagas';
import StripeWrapper from './StripeWrapper';

const sagaMiddleware = createSagaMiddleware();

const history = createBrowserHistory();

const store = createStore(
  createReducers(history),
  {},
  composeWithDevTools(
    applyMiddleware(routerMiddleware(history), sagaMiddleware),
  ),
);

const persistore = persistStore(store);

sagaMiddleware.run(sagas);

const token = LocalStorageService.getAccessToken();
if (token) {
  store.dispatch({ type: ActionTypes.LOGIN_SUCCESS, payload: { token } });
}

const RootApp = () => (
  <StripeWrapper apiKey={STRIPE_PUB_KEY}>
    <Provider store={store}>
      <PersistGate persistor={persistore} loading={<Loader />}>
        <ConnectedRouter history={history}>
          <App />
          <Notifications />
        </ConnectedRouter>
      </PersistGate>
    </Provider>
  </StripeWrapper>
);

ReactDOM.render(<RootApp />, document.getElementById('root'));

// @ts-ignore
if (module.hot) {
  // @ts-ignore
  module.hot.accept('./App', () => {
    ReactDOM.render(<RootApp />, document.getElementById('root'));
  });
}
