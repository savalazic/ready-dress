import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Container, Icon } from 'semantic-ui-react';
import cx from 'classnames';

import './Cart.scss';
import logo from 'assets/images/logo.svg';

import { State } from 'services/state';
import {
  getCartItemsSelector,
  getCartItemsCountSelector,
  getStoresForCartSelector,
} from 'services/cart/cartSelectors';
import { getStoreRequest } from 'services/store/storeActions';
import useToggle from 'hooks/useToggle';

import { SecondaryButton } from 'components/Button';
import ModalLink from 'components/ModalLink';

import CartList from './CartList';
import useKeyPress from 'hooks/useKeyPress';
import { getIsLoggedIn } from 'services/auth/authSelectors';

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;

type Props = DispatchProps & StateProps;

const ESC_KEY = 27;

const Cart = ({
  cartItems,
  cartItemsCount,
  cartStores,
  isLoggedIn,
  getStoreRequest,
}: Props) => {
  const [opened, toggle] = useToggle(false);

  useEffect(() => {
    // tslint:disable-next-line: forin
    for (const id in cartItems) {
      getStoreRequest(+id);
    }
  }, [cartItems, getStoreRequest]);

  useKeyPress(ESC_KEY, () => {
    toggle(false);
  });

  return (
    <div
      className={cx('Cart', {
        'Cart--opened': opened,
      })}
    >
      <div className="Cart__backdrop" onClick={toggle} />
      <Container className="Cart__container">
        {opened && <CartList items={cartItems} stores={cartStores} />}
        <img className="Cart__logo" src={logo} alt="logo" />
        <div className="Cart__bag" onClick={toggle}>
          <Icon name="shopping bag" />
          <div className="Cart__bag-count">{cartItemsCount}</div>
        </div>
        {isLoggedIn ? (
          <ModalLink to="/appointment/date" disabled={cartItemsCount === 0}>
            <SecondaryButton size="huge">Ready to try out</SecondaryButton>
          </ModalLink>
        ) : (
          <ModalLink to="/login" disabled={cartItemsCount === 0}>
            <SecondaryButton size="huge">Ready to try out</SecondaryButton>
          </ModalLink>
        )}
      </Container>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  cartItems: getCartItemsSelector(state),
  cartItemsCount: getCartItemsCountSelector(state),
  cartStores: getStoresForCartSelector(state),
  isLoggedIn: getIsLoggedIn(state),
});

const actions: any = {
  getStoreRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(Cart);
