import React from 'react';
import { Icon } from 'semantic-ui-react';

import './CartListItem.scss';

import defaultImage from 'assets/images/No_Image_Available.jpg';

interface IProps {
  image: string;
  title: string;
  price: number;
  onItemRemove: (id: number, storeId: number) => void;
}

type Props = IProps;

const CartListItem = ({
  title,
  image = defaultImage,
  price,
  onItemRemove,
}: Props) => {
  return (
    <div className="CartListItem">
      <img src={image} alt="cart item" />
      <p className="CartListItem__title">{title}</p>
      <p className="CartListItem__price">Price: ${price}</p>
      <Icon name="remove circle" onClick={onItemRemove} />
    </div>
  );
};

export default CartListItem;
