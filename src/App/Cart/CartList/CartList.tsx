import map from 'lodash/map';
import size from 'lodash/size';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Menu } from 'semantic-ui-react';

import './CartList.scss';

import CartListItem from './CartListItem';
import { StoreItem, Store } from 'services/store/storeReducer';
import { changeAppointment } from 'services/appointment/appointmentActions';
import { removeItemFromCart } from 'services/cart/cartActions';

interface IProps {
  items: {
    [storeId: number]: {
      id: number;
      name: string;
      data: StoreItem[];
      subtotal: number;
    };
  };
  stores: Store[];
}

type DispatchProps = typeof actions;

type Props = DispatchProps & IProps;

const CartList = ({
  items,
  stores,
  changeAppointment,
  removeItemFromCart,
}: Props) => {
  const [selectedBranchId, setSelectedBranchId] = useState<number>(
    +Object.keys(items)[0],
  );

  return (
    <div className="CartList">
      <div className="CartList__header">
        <span>ReadyDress fitting room</span>
        <span>
          Subtotal: $
          {items[selectedBranchId] ? items[selectedBranchId].subtotal : '0'}
        </span>
      </div>
      <div className="CartList__menu">
        <Menu pointing secondary>
          {map(stores, (store: Store) => (
            <Menu.Item
              key={String(store.id)}
              id={store.id}
              name={store.store_name}
              active={selectedBranchId === store.id}
              onClick={() => {
                setSelectedBranchId(store.id);
                changeAppointment({ storeBranchId: store.id });
              }}
            />
          ))}
        </Menu>
      </div>
      <div className="CartList__main">
        {size(items) === 0 && <p>There is no products in cart</p>}
        {items[selectedBranchId] &&
          items[selectedBranchId].data.length === 0 && (
            <p>There is no products in cart</p>
          )}
        {map(
          items[selectedBranchId] && items[selectedBranchId].data,
          (item: StoreItem) => (
            <CartListItem
              key={item.id}
              title={item.title}
              price={item.price_in_centum}
              image={item.images[0] && item.images[0].url}
              onItemRemove={() => removeItemFromCart(item.id, selectedBranchId)}
            />
          ),
        )}
      </div>
    </div>
  );
};

const actions = {
  changeAppointment,
  removeItemFromCart,
};

export default connect(
  null,
  actions,
)(CartList);
