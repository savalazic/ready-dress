import React from 'react';
import { connect } from 'react-redux';

import './Notifications.scss';

import { State } from 'services/state';
import { hideNotification } from 'services/notification/notificationActions';
import { Notification } from 'services/notification/notificationReducer';
import { getNotifications } from 'services/notification/notificationSelectors';

import NotificationCard from './NotificationCard';

interface Props {
  notifications: Notification[];
  hideNotification: (id: number) => void;
}

// tslint:disable-next-line: no-shadowed-variable
const Notifications = ({ notifications, hideNotification }: Props) => {
  return (
    <div className="Notifications">
      {notifications.map((notification: Notification) => (
        <NotificationCard
          key={notification.id}
          id={notification.id}
          message={notification.message}
          duration={notification.duration}
          style={notification.style}
          onClick={hideNotification}
          onDurationEnd={hideNotification}
        />
      ))}
    </div>
  );
};

const actions = {
  hideNotification,
};

const mapStateToProps = (state: State) => ({
  notifications: getNotifications(state),
});

export default connect(
  mapStateToProps,
  actions,
)(Notifications);
