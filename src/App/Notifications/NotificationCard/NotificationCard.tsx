import React, { useEffect, memo } from 'react';
import cx from 'classnames';

import './NotificationCard.scss';

interface Props {
  message: string;
  id: number;
  duration: number;
  style: string;
  onClick: (id: number) => void;
  onDurationEnd: (id: number) => void;
}

const NotificationCard = memo(
  ({ message, id, duration, style, onClick, onDurationEnd }: Props) => {
    useEffect(() => {
      setTimeout(() => onDurationEnd(id), duration);
    });

    return (
      <div
        className={cx('NotificationCard', style)}
        onClick={() => onClick(id)}
      >
        <span>{message}</span>
      </div>
    );
  },
);

export default NotificationCard;
