import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { State } from 'services/state';
import { getAuthUserRequest } from 'services/auth/authActions';
import {
  getIsLoggedIn,
  getAuthUserLoadingSelector,
} from 'services/auth/authSelectors';

import Nav from './Nav';
import Main from './Main';
import Footer from './Footer';
import Cart from './Cart';
import LoadingContainer from 'components/LoadingContainer';

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;

type Props = DispatchProps & StateProps;

const App = ({ getAuthUserRequest, isLoggedIn, isUserLoading }: Props) => {
  useEffect(() => {
    if (isLoggedIn) {
      getAuthUserRequest({});
    }
  }, [isLoggedIn, getAuthUserRequest]);

  return (
    <React.Fragment>
      <LoadingContainer className="App" isLoading={isUserLoading}>
        <Nav />
        <Main />
        <Footer />
        <Cart />
      </LoadingContainer>
    </React.Fragment>
  );
};

const mapStateToProps = (state: State) => ({
  isLoggedIn: getIsLoggedIn(state),
  isUserLoading: getAuthUserLoadingSelector(state),
});

const actions = {
  getAuthUserRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(App);
