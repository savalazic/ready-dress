import React from 'react';
import { Link } from 'react-router-dom';

interface FooterItem {
  title: string;
  link: string;
}

interface Props {
  heading: string;
  items: FooterItem[];
}

const FooterList = ({ heading, items }: Props) => {
  return (
    <div>
      <h3 className="Footer__heading">{heading}</h3>
      <ul className="Footer__list">
        {items.map((item: FooterItem) => (
          <li className="Footer__item" key={item.title}>
            <Link to={item.link}>{item.title}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default FooterList;
