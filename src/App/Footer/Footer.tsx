import React from 'react';
import { Grid, Image, Icon } from 'semantic-ui-react';

import './Footer.scss';
import FooterList from './FooterList';
import logo from 'assets/images/logo.svg';

const questionItems = [
  { title: 'FAQ', link: '/faq' },
  { title: 'Data Privacy', link: '/data-privacy' },
  { title: 'Contact Us', link: '/contact' },
];
const company = [
  { title: 'About us', link: '/about-us' },
  { title: 'Press', link: '/press' },
];
const stores = [{ title: 'Return Policies', link: '/return-policies' }];
const mobileApp = [
  { title: 'Download the app and iOS and Android (coming soon!)', link: '/' },
];

const Footer = () => {
  return (
    <footer className="Footer">
      <Grid container stackable>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={8}>
            <Grid>
              <Grid.Row columns={2}>
                <Grid.Column>
                  <Image className="Footer__logo" src={logo} alt="logo" />
                </Grid.Column>
                <Grid.Column mobile={16} tablet={16} computer={8}>
                  <a
                    href="https://www.facebook.com/thereadydress"
                    className="Footer__social__link"
                  >
                    <Icon name="facebook" size="big" link={true} />
                  </a>
                  <a
                    href="https://www.instagram.com/thereadydress"
                    className="Footer__social__link"
                  >
                    <Icon name="instagram" size="big" link={true} />
                  </a>

                  <a
                    href="https://twitter.com/ReadyDress"
                    className="Footer__link"
                  >
                    Follow us
                  </a>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={16} computer={8}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} tablet={16} computer={4}>
                  <FooterList heading="Questions?" items={questionItems} />
                </Grid.Column>
                <Grid.Column mobile={16} tablet={16} computer={4}>
                  <FooterList heading="The Company" items={company} />
                </Grid.Column>
                <Grid.Column mobile={16} tablet={16} computer={4}>
                  <FooterList heading="Store" items={stores} />
                </Grid.Column>
                <Grid.Column mobile={16} tablet={16} computer={4}>
                  <FooterList heading="Mobile App" items={mobileApp} />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </footer>
  );
};

export default Footer;
