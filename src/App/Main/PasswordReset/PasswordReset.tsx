import React from 'react';
import { Container } from 'semantic-ui-react';
import { Location } from 'history';
import { Formik, Field } from 'formik';
import { object, string } from 'yup';
import { connect } from 'react-redux';

import './PasswordReset.scss';

import Form from 'components/Form';
import { resetPasswordRequest } from 'services/auth/authActions';
import { PrimaryButton } from 'components/Button';
import { State } from 'services/state';
import { getResetPasswordLoadingSelector } from 'services/auth/authSelectors';
import PasswordInput from 'components/PasswordInput';

interface Props {
  resetPasswordRequest: (code: string, password: string) => {};
  isPasswordResetLoading: boolean;
  location: Location;
}

interface FormValues {
  password: string;
}

const validationSchema = object().shape({
  password: string()
    .min(8, 'Minimum 8 characters')
    .required('Required'),
});

const PasswordReset = ({
  resetPasswordRequest,
  isPasswordResetLoading,
  location,
}: Props) => {
  const handleSubmit = (values: FormValues) => {
    const queryParams = new URLSearchParams(location.search);
    const code = queryParams.get('code');

    if (code) {
      resetPasswordRequest(code, values.password);
    }
  };

  return (
    <Container>
      <h2 className="PasswordReset__heading">Reset password</h2>
      <Formik
        initialValues={{ password: '' }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        {() => (
          <Form className="PasswordReset__form">
            <Field
              component={PasswordInput}
              name="password"
              label="Password"
              id="password"
              disabled={isPasswordResetLoading}
            />
            <PrimaryButton type="submit" loading={isPasswordResetLoading}>
              Submit
            </PrimaryButton>
          </Form>
        )}
      </Formik>
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  isPasswordResetLoading: getResetPasswordLoadingSelector(state),
});

const actions: any = {
  resetPasswordRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(PasswordReset);
