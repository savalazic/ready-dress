import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { State } from 'services/state';
import { getStoresByIdSelector } from 'services/store/storeSelectors';

import { SecondaryButton, PrimaryButton } from 'components/Button';
import ModalLink from 'components/ModalLink';

import { FormValues } from '../Appointment';

import './AppointmentReview.scss';

interface IProps {
  values: FormValues;
}
type StateProps = ReturnType<typeof mapStateToProps>;
type Props = StateProps & IProps;

const AppointmentReview = ({ values, stores }: Props) => {
  const storeInfo = stores[values.storeBranchId];

  if (!storeInfo) {
    return <p>No store information available</p>;
  }

  return (
    <div className="AppointmentReview">
      <h2>Let's review and confirm your details</h2>

      <div className="AppointmentReview__info">
        <p>Date: {moment(values.date).format('MMM Do YY')}</p>
        <p>
          Time: {values.timeFrom} - {values.timeTo}
        </p>
        <p>Store name: {storeInfo.store_name}</p>
        <p>
          Store address: {storeInfo.street_name}, {storeInfo.street_number},{' '}
          {storeInfo.city}
        </p>
        <p>
          Store phone number:{' '}
          <a href={`tel:${storeInfo.phone_number}`}>{storeInfo.phone_number}</a>
        </p>
      </div>

      <ModalLink to="/appointment/credit-card">
        <SecondaryButton fluid>Yes! I'll be there</SecondaryButton>
      </ModalLink>
      <ModalLink to="/appointment/date">
        <PrimaryButton fluid>No, I need to adjust something</PrimaryButton>
      </ModalLink>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  stores: getStoresByIdSelector(state),
});

export default connect(mapStateToProps)(AppointmentReview);
