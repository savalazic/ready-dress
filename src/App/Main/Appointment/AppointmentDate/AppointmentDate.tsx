import React from 'react';
import { Field } from 'formik';

import './AppointmentDate.scss';

import ModalLink from 'components/ModalLink';
import { SecondaryButton } from 'components/Button';
import DateInput from 'components/DateInput';
import TimeInput from 'components/TimeInput';

interface Props {
  isValid: boolean;
}

const AppointmentDate = ({ isValid }: Props) => {
  return (
    <div className="AppointmentDate">
      <h2>You already look amazing!</h2>
      <h3>Appointment Details</h3>

      <Field
        component={DateInput}
        id="date"
        name="date"
        label="Date"
        minDate={new Date()}
      />

      <div className="AppointmentDate__time">
        <div className="AppointmentDate__time-fields">
          <Field
            component={TimeInput}
            id="timeFrom"
            name="timeFrom"
            placeholder="From"
          />
          <Field
            component={TimeInput}
            id="timeTo"
            name="timeTo"
            placeholder="To"
          />
        </div>
        <span className="AppointmentDate__label">Time</span>
      </div>

      <p className="AppointmentDate__hint">
        **Show up to your store within the chosen hour
      </p>

      <ModalLink to="/appointment/review" disabled={!isValid}>
        <SecondaryButton size="huge" fluid>
          It's a date!
        </SecondaryButton>
      </ModalLink>
    </div>
  );
};

export default AppointmentDate;
