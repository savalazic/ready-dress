import React, { useState } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  injectStripe,
  CardElement,
  ReactStripeElements,
} from 'react-stripe-elements';

import './AppointmentCreditCard.scss';

import { showNotification } from 'services/notification/notificationActions';
import { saveCreditCardRequest } from 'services/payment/paymentActions';
import { saveCcAndCreateAppointment } from 'services/appointment/appointmentActions';

import { SecondaryButton } from 'components/Button';
import LoadingContainer from 'components/LoadingContainer';

interface IProps extends ReactStripeElements.InjectedStripeProps {
  handleSubmit: (values: any) => void;
  isSubmitting: boolean;
  values: any;
}
type DispatchProps = typeof actions;
type Props = DispatchProps & IProps;

const AppointmentCreditCard = ({
  isSubmitting,
  stripe,
  showNotification,
  values,
  saveCcAndCreateAppointment,
}: Props) => {
  const [loading, setLoading] = useState(false);

  const handleCreditCartSubmission = async (e: React.ChangeEvent<any>) => {
    setLoading(true);

    // @ts-ignore
    const { token, error } = await stripe.createToken();

    const dateStr = values.date
      // @ts-ignore
      .toISOString(true)
      .split('T')
      .shift();

    const timeAndDateFrom = moment(`${dateStr} ${values.timeFrom}`).toDate();
    const timeAndDateTo = moment(`${dateStr} ${values.timeTo}`).toDate();

    const appointmentValues = {
      from: timeAndDateFrom,
      to: timeAndDateTo,
      items: values.items,
      storeBranchId: values.storeBranchId,
    };

    if (token) {
      setLoading(false);
      saveCcAndCreateAppointment(token.id, appointmentValues);
    } else {
      // @ts-ignore
      showNotification(error.message, 3000, 'error');
      setLoading(false);
    }
  };

  return (
    <LoadingContainer
      className="AppointmentCreditCard"
      isLoading={isSubmitting || loading}
    >
      <h2>Card info</h2>
      <CardElement />

      <p className="AppointmentCreditCard__hint">
        You will be only charged a fee of $3 if you don't show up or cancel
        within the three hours before your appointment
      </p>
      <SecondaryButton onClick={handleCreditCartSubmission} fluid>
        Ready Set Dress!
      </SecondaryButton>
    </LoadingContainer>
  );
};

const actions = {
  showNotification,
  saveCreditCardRequest,
  saveCcAndCreateAppointment,
};

export default connect(
  null,
  actions,
)(injectStripe(AppointmentCreditCard));
