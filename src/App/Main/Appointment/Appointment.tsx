import React from 'react';
import { connect } from 'react-redux';
import { Formik, FormikErrors } from 'formik';
import moment from 'moment';
import { object, string } from 'yup';
import { Switch } from 'react-router-dom';

import Form from 'components/Form';
import ModalRoute from 'components/ModalRoute';
import AppointmentDate from './AppointmentDate';
import AppointmentReview from './AppointmentReview';
import { State } from 'services/state';
import {
  getCurrentAppointmentBranchId,
  getSaveCcAndCreateAppointmentLoading,
} from 'services/appointment/appointmentSelectors';
import {
  createAppointmentRequest,
  saveCcAndCreateAppointment,
} from 'services/appointment/appointmentActions';
import { getCartItemsSelector } from 'services/cart/cartSelectors';
import CreditCards from '../CustomerMenu/CreditCards';

// const dateFormat = 'YYYY-MM-DD';
const timeFormat = 'hh:mm A';
const dateTimeFormat = 'YYYY-MM-DD HH:mm';

export interface FormValues {
  storeBranchId: number;
  date: Date;
  timeFrom: string;
  timeTo: string;
  items: any[];
}

const validate = (values: FormValues) => {
  const errors: FormikErrors<FormValues> = {};

  if (
    moment(values.timeFrom, timeFormat).isSameOrAfter(
      moment(values.timeTo, timeFormat),
    )
  ) {
    errors.timeFrom = 'From date must be before to date';
  }

  const dateStr = values.date
    // @ts-ignore
    .toISOString(true)
    .split('T')
    .shift();

  const timeAndDateFrom = moment(`${dateStr} ${values.timeFrom}`).toDate();

  if (
    values.timeFrom &&
    moment(timeAndDateFrom, dateTimeFormat).isBefore(
      moment(new Date(), dateTimeFormat),
    )
  ) {
    errors.timeFrom = 'Time cannot be in the past';
  }

  return errors;
};

const validationSchema = object().shape({
  date: string().required('Required'),
  timeFrom: string().required('Required'),
  timeTo: string().required('Required'),
});

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;

type Props = DispatchProps & StateProps;

const Appointment = ({
  currentAppointmentBranchId,
  cartItems,
  isAppointmentCreationLoading,
  createAppointmentRequest,
}: Props) => {
  const handleSubmit = (values: FormValues) => {
    const dateStr = values.date
      // @ts-ignore
      .toISOString(true)
      .split('T')
      .shift();

    const timeAndDateFrom = moment(
      `${dateStr} ${values.timeFrom.split(' ')[0]}`,
    ).toDate();
    const timeAndDateTo = moment(
      `${dateStr} ${values.timeTo.split(' ')[0]}`,
    ).toDate();

    createAppointmentRequest(
      values.storeBranchId,
      timeAndDateFrom,
      timeAndDateTo,
      values.items,
    );
  };

  if (!currentAppointmentBranchId) {
    return null;
  }

  return (
    <div>
      <Formik
        initialValues={{
          storeBranchId: currentAppointmentBranchId,
          date: new Date(),
          timeFrom: '',
          timeTo: '',
          items: cartItems[currentAppointmentBranchId].data.map(item => ({
            id: item.id,
            color_sample_id: item.selectedColor,
          })),
        }}
        validate={validate}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ handleSubmit, isValid, values }) => (
          <Form>
            <Switch>
              <ModalRoute
                component={() => <AppointmentDate isValid={isValid} />}
                path="/appointment/date"
              />
              <ModalRoute
                component={() => <AppointmentReview values={values} />}
                path="/appointment/review"
                hasBack
              />
              <ModalRoute
                component={() => (
                  <CreditCards editMode={false} onComplete={handleSubmit} />
                )}
                path="/appointment/credit-card"
                hasBack
              />
            </Switch>
          </Form>
        )}
      </Formik>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  cartItems: getCartItemsSelector(state),
  currentAppointmentBranchId: getCurrentAppointmentBranchId(state),
  isAppointmentCreationLoading: getSaveCcAndCreateAppointmentLoading(state),
});

const actions = {
  createAppointmentRequest,
  saveCcAndCreateAppointment,
};

export default connect(
  mapStateToProps,
  actions,
)(Appointment);
