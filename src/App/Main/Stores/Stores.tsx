import React from 'react';
import { Route, RouteComponentProps } from 'react-router-dom';
import StoreProducts from './StoreProducts';
import StoreSingleProduct from './StoreSingleProduct';
import Store from './Store';

const Stores = ({ match }: RouteComponentProps) => {
  return (
    <div className="Stores">
      <Route exact path={`${match.path}`} component={Store} />
      <Route
        exact
        path={`${match.path}/:storeId/products`}
        component={StoreProducts}
      />
      <Route
        path={`${match.path}/:storeId/products/:productId`}
        component={StoreSingleProduct}
      />
    </div>
  );
};

export default Stores;
