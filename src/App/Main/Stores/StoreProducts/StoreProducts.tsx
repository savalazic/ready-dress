import React, { useEffect } from 'react';
import Section from 'components/Section';
import { RouteComponentProps } from 'react-router-dom';

import StoreProductsList from './StoreProductsList';
import { connect } from 'react-redux';
import { getStoreItemsRequest } from 'services/store/storeActions';
import {
  getStoreItemsLoadingSelector,
  getStoreItemsSelector,
  getStoreItemsPaginationSelector,
} from 'services/store/storeSelectors';
import { State } from 'services/state';
import { StoreItem } from 'services/store/storeReducer';
import LoadingContainer from 'components/LoadingContainer';

interface MatchParams {
  storeId: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  getStoreItemsRequest: (storeId: number, pagination: boolean) => void;
  storeItems: StoreItem[];
  isStoreItemsLoading: boolean;
  // storesPagination: IPagination;
}

const StoreProducts = ({
  getStoreItemsRequest,
  isStoreItemsLoading,
  storeItems,
  match,
}: Props) => {
  useEffect(() => {
    getStoreItemsRequest(+match.params.storeId, false);
  }, []);
  return (
    <Section className="StoreProducts">
      <div className="StoreProducts__main">
        <LoadingContainer
          isLoading={isStoreItemsLoading}
          className="Stores__main"
        >
          <StoreProductsList items={storeItems} />
        </LoadingContainer>
      </div>
    </Section>
  );
};

const actions: any = {
  getStoreItemsRequest,
};

const mapStateToProps = (state: State) => ({
  isStoreItemsLoading: getStoreItemsLoadingSelector(state),
  storeItems: getStoreItemsSelector(state),
  storeItemsPagination: getStoreItemsPaginationSelector(state),
});

export default connect(
  mapStateToProps,
  actions,
)(StoreProducts);
