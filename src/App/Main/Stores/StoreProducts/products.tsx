import mapKeys from 'lodash/mapKeys';
const mocks = [
  {
    id: 1,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 2,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 3,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 4,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 5,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 6,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 7,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 8,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 9,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 10,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 11,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
  {
    id: 12,
    storeId: 1,
    title: 'ASOS DESIGN hoodie in pink',
    price: 35,
  },
];
export const products = mapKeys(mocks, 'id');
