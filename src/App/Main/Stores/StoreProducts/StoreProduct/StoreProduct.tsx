import React from 'react';
import Card from 'components/Card';
import placeHolderImage from 'assets/images/No_Image_Available.jpg';

interface Item {
  id: number;
  title: string;
  price_in_centum: number;
  image: string;
}
const StoreProduct = ({
  id,
  title,
  price_in_centum,
  image = placeHolderImage,
}: Item) => (
  <Card>
    <Card.Image image={image} />
    <Card.Body>
      <div className="Card__title">
        <h3>{title}</h3>
      </div>
      <p className="Card__price">{price_in_centum}$</p>
    </Card.Body>
  </Card>
);

export default StoreProduct;
