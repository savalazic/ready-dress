import React from 'react';
import StoreProduct from '../StoreProduct';
import './StoreProductsList.scss';
import { withRouter, RouteComponentProps, Link } from 'react-router-dom';
import map from 'lodash/map';
import { StoreItem } from 'services/store/storeReducer';

export interface Props extends RouteComponentProps {
  items: StoreItem[];
}

const StoreProductsList = ({ match, items }: Props) => {
  return (
    <div className="StoreProductsList__row">
      {items.length === 0 && 'No Products in store'}
      {map(items, (item: StoreItem) => (
        <Link to={`${match.url}/${item.id}`}>
          <StoreProduct
            key={String(item.id)}
            id={item.id}
            title={item.title}
            price_in_centum={item.price_in_centum}
            image={item.images[0] && item.images[0].url}
          />
        </Link>
      ))}
    </div>
  );
};

export default withRouter(StoreProductsList);
