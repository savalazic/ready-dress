import React from 'react';

import './StoreSingleProductDetails.scss';

interface Props {
  image?: string;
  description: string;
}

const StoreSingleProductDetails: React.FC<Props> = ({ image, description }) => {
  return (
    <div className="StoreSingleProductDetails">
      <img className="StoreSingleProductDetails__image" src={image} />
      <h2 className="StoreSingleProductDetails__title">Details</h2>
      <p className="StoreSingleProductDetails__descirption">{description}</p>
    </div>
  );
};

export default StoreSingleProductDetails;
