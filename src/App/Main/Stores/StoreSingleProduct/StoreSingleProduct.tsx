import React, { Component } from 'react';
import Section from 'components/Section';
import { Container } from 'semantic-ui-react';
import './StoreSingleProduct.scss';
import { PrimaryButton } from 'components/Button';
import { State } from 'services/state';
import has from 'lodash/has';
import defaultImage from 'assets/images/No_Image_Available.jpg';

import { StoreItem, Store, StoreMap } from 'services/store/storeReducer';
import {
  getStoreItemRequest,
  getStoreRequest,
} from 'services/store/storeActions';
import { addItemToCart, removeItemFromCart } from 'services/cart/cartActions';
import { getCartItemsSelector } from 'services/cart/cartSelectors';

import {
  getStoreItemLoadingSelector,
  getStoreItemsByIdSelector,
  getStoresByIdSelector,
} from 'services/store/storeSelectors';

import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import LoadingContainer from 'components/LoadingContainer';

import StoreSingleProductDetails from './StoreSingleProductDetails';

interface IRoute {
  storeId: string;
  productId: string;
}
interface Props {
  getStoreItemRequest(storeId: number, productId: number): () => StoreItem;
  getStoreRequest(storeId: number): () => Store;
  getStoreItemsRequest(storeId: number): () => StoreItem[];
  addItemToCart(
    item: StoreItem,
    storeId: number,
    selectedColor?: number,
  ): () => void;
  removeItemFromCart(itemId: number, storeId: number): () => void;
  storeItems: { [id: number]: StoreItem };
  isStoreItemLoading: boolean;
  cartItems: {
    [storeId: number]: {
      id: number;
      name: string;
      data: StoreItem[];
      subtotal: number;
    };
  };
  stores: StoreMap | null;
}
class StoreSingleProduct extends Component<
  Props & RouteComponentProps<IRoute>
> {
  state = {
    selectedImageIndex: 0,
    productSizes: ['xs', 's', 'm', 'l', 'xl', 'xxl'],
    selectedSize: '',
    selectedColor: -1,
    showColorError: false,
  };

  changeImage = (e: any) => {
    this.setState({
      selectedImageIndex: e.target.dataset.index,
    });
  };

  selectSize = (e: any) => {
    this.setState({
      selectedSize: e.target.dataset.size,
    });
  };

  componentDidMount() {
    if (!has(this.props.storeItems, +this.props.match.params.productId)) {
      this.props.getStoreItemRequest(
        +this.props.match.params.storeId,
        +this.props.match.params.productId,
      );
    }

    if (!has(this.props.stores, +this.props.match.params.storeId)) {
      this.props.getStoreRequest(+this.props.match.params.storeId);
    }
  }

  handleSelectColor = (e: any) => {
    this.setState({ selectedColor: +e.target.dataset.color });
  };

  handleAddToCart = () => {
    const { addItemToCart, match, storeItems } = this.props;
    const { selectedColor } = this.state;
    const currentItem = storeItems[+this.props.match.params.productId];

    const [colorSample] = (currentItem && currentItem.color_samples) || [];

    const defaultColorSample = colorSample ? colorSample.id : undefined;

    const selectedColorSample =
      currentItem &&
      currentItem.color_samples.map(({ id }) => id).includes(selectedColor)
        ? selectedColor
        : defaultColorSample;

    addItemToCart(
      storeItems[+match.params.productId],
      +match.params.storeId,
      selectedColorSample,
    );
  };

  handleRemoveFromCart = () => {
    const { removeItemFromCart, match } = this.props;

    removeItemFromCart(+match.params.productId, +match.params.storeId);
  };

  render() {
    const { isStoreItemLoading, storeItems, cartItems, match } = this.props;
    const {
      selectedImageIndex,
      productSizes,
      selectedSize,
      selectedColor,
      showColorError,
    } = this.state;

    const currentItem = storeItems[+this.props.match.params.productId];

    const selectedImg =
      currentItem && currentItem.images.length > 0
        ? currentItem.images[selectedImageIndex].url
        : defaultImage;

    const sideImages =
      currentItem &&
      currentItem.images.map((image, index) => (
        <span onClick={this.changeImage} key={String(index)}>
          <img src={image.url} alt="Product" data-index={index} width="80" />
        </span>
      ));

    const firstImage =
      currentItem && currentItem.images[0] && currentItem.images[0].url;

    const colors = (currentItem && currentItem.color_samples) || [];

    const isInCart =
      currentItem &&
      has(cartItems, +match.params.storeId) &&
      cartItems[+match.params.storeId].data.find(
        (item: StoreItem) => item.id === +match.params.productId,
      );

    if (isStoreItemLoading) {
      return <LoadingContainer isLoading />;
    }

    if (!currentItem && !isStoreItemLoading) {
      return <h2>Product doesn't exist</h2>;
    }

    return (
      <Section className="SingleProduct">
        <Container>
          <LoadingContainer
            isLoading={isStoreItemLoading}
            className="SingleProduct__row"
          >
            {currentItem && currentItem.images.length > 1 && (
              <div className="SingleProduct__side-images">{sideImages}</div>
            )}
            <div className="SingleProduct__img-wrap">
              <img src={selectedImg} alt="product" />
            </div>

            <div className="SingleProduct__description">
              <h2>{currentItem.title}</h2>
              <span className="SingleProduct__price">
                {currentItem.price_in_centum}$
              </span>
              <div className="SingleProduct__colors">
                {colors.map(({ id, url }) => (
                  <img
                    key={id}
                    className={`SingleProduct__color-container ${
                      selectedColor === id ? 'selected' : ''
                    }`}
                    data-color={id}
                    onClick={this.handleSelectColor}
                    src={url}
                    alt="color"
                  />
                ))}
                {showColorError && (
                  <div className="SingleProduct__color-error">
                    Please select color
                  </div>
                )}
              </div>
              {/* <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maxime
                impedit eligendi reiciendis velit amet sint esse, error numquam.
                Ipsum quasi ab non molestias nisi. Pariatur, eos eveniet. Illo,
                totam repellendus?
              </p> */}
              <div className="SingleProduct__sizes">
                {productSizes.map(size => (
                  <span
                    className={size === selectedSize ? 'selected' : ''}
                    key={size}
                    data-size={size}
                    onClick={this.selectSize}
                  >
                    {size}
                  </span>
                ))}
              </div>
              <div>
                <PrimaryButton
                  onClick={
                    !isInCart ? this.handleAddToCart : this.handleRemoveFromCart
                  }
                >
                  {!isInCart ? 'Add to cart' : 'Remove from cart'}
                </PrimaryButton>
              </div>
            </div>
          </LoadingContainer>
        </Container>
        <StoreSingleProductDetails
          image={firstImage}
          description={currentItem && currentItem.description}
        />
      </Section>
    );
  }
}
const mapStateToProps = (state: State) => ({
  isStoreItemLoading: getStoreItemLoadingSelector(state),
  storeItems: getStoreItemsByIdSelector(state),
  cartItems: getCartItemsSelector(state),
  stores: getStoresByIdSelector(state),
});

const actions: any = {
  getStoreItemRequest,
  addItemToCart,
  removeItemFromCart,
  getStoreRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(StoreSingleProduct);
