import React from 'react';
import Card from 'components/Card';
import './StoreCard.scss';

interface Props {
  store_name: string;
  image_url: string;
}

const StoreCard = ({ store_name, image_url }: Props) => (
  <Card>
    <Card.Image image={image_url} />
    <Card.Body>
      <div className="Card__title">
        <h3>{store_name}</h3>
      </div>
      {/* <p className="Card__description">{description}</p> */}
    </Card.Body>
  </Card>
);

export default StoreCard;
