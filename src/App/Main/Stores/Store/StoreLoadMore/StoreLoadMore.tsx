import React from 'react';
import Button from 'components/Button';
import './StoreLoadMore.scss';
// import ProgressBar from 'components/ProgressBar';
import { Container } from 'semantic-ui-react';

interface Props {
  title?: string;
  onClick: any;
  disabled: boolean;
}
const StoreLoadMore = ({ title, onClick, disabled }: Props) => {
  return (
    <Container>
      <div className="StoreLoadMore">
        {/* <p className="StoreLoadMore__title">
          You've viewed 16 of {totalCount} {title}
        </p> */}
        {/* <ProgressBar percent={51} /> */}
        <Button disabled={disabled} className="btn--loadMore" onClick={onClick}>
          Load more
        </Button>
      </div>
    </Container>
  );
};

export default StoreLoadMore;
