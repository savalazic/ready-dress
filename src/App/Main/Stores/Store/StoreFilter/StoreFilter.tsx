import debounce from 'lodash/debounce';
import React, { useState, useEffect } from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';
import Button from 'components/Button';
import { Input, Icon } from 'semantic-ui-react';
import './StoreFilter.scss';

import { getStoresRequest } from 'services/store/storeActions';
import { getStoresLoadingSelector } from 'services/store/storeSelectors';
import { StoreParams } from 'services/store/storeReducer';
import { State } from 'services/state';
import useGeolocation from 'hooks/useGeolocation';

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;
type Props = DispatchProps & StateProps;

const StoresFilter = ({ getStoresRequest, isLoadingFilter }: Props) => {
  const location = useGeolocation();

  const [params, setFilterParams] = useState<StoreParams>({});

  useEffect(() => {
    if (location.longitude && location.latitude) {
      setFilterParams({
        ...params,
        lng: location.longitude,
        lat: location.latitude,
        radius: 100,
      });
    }
  }, [location]);

  const incrementDistance = () => {
    setFilterParams({
      ...params,
      // @ts-ignore
      radius: params.radius + 1,
    });
    if (params.radius !== 0) {
      getStoresRequest({ ...params, radius: params.radius });
    }
  };

  const decrementDistance = () => {
    // @ts-ignore
    if (params.radius > 0) {
      setFilterParams({
        ...params,
        // @ts-ignore
        radius: params.radius - 1,
      });
    }

    if (params.radius !== 0) {
      getStoresRequest({ ...params, radius: params.radius });
    }
  };

  const handleDistanceChange = (event: any) => {
    event.preventDefault();
  };

  const handleSearchChange = debounce(searchQuery => {
    setFilterParams({
      ...params,
      searchQuery,
    });
    getStoresRequest({ ...params, searchQuery });
  }, 500);

  const handleAscChange = () => {
    setFilterParams({
      ...params,
      order: 'asc',
    });
    getStoresRequest({ ...params, order: 'asc' });
  };

  const handleDescChange = () => {
    setFilterParams({
      ...params,
      order: 'desc',
    });
    getStoresRequest({ ...params, order: 'desc' });
  };

  return (
    <React.Fragment>
      <div className="Stores__filter">
        <div className="Stores__search">
          <Icon className="icon-search" name="search" />
          <span>Search</span>
          <Input
            className="input-search"
            type="text"
            disabled={isLoadingFilter}
            onChange={(e: any) => handleSearchChange(e.target.value)}
          />
        </div>

        <div className="Stores__sort">
          <Button
            disabled={isLoadingFilter}
            className={cx({ 'is-active': params.order === 'asc' })}
            onClick={handleAscChange}
          >
            A-Z
          </Button>
          <Button
            disabled={isLoadingFilter}
            className={cx({ 'is-active': params.order === 'desc' })}
            onClick={handleDescChange}
          >
            Z-A
          </Button>
        </div>

        {params.radius && !location.loading && (
          <div className="Stores__distance">
            <div className="Stores__distance__wrap">
              <Icon name="binoculars" />
              <span>Distance (mi)</span>
            </div>
            <Input
              type="number"
              className="input--distance"
              disabled={isLoadingFilter}
              value={params.radius}
              onChange={handleDistanceChange}
            />
            <Button
              className="button--distance"
              disabled={isLoadingFilter}
              onClick={incrementDistance}
            >
              +
            </Button>
            <Button
              className="button--distance"
              disabled={isLoadingFilter}
              onClick={decrementDistance}
            >
              -
            </Button>
          </div>
        )}
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = (state: State) => ({
  isLoadingFilter: getStoresLoadingSelector(state),
});

const actions: any = {
  getStoresRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(StoresFilter);
