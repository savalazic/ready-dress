import map from 'lodash/map';
import chunk from 'lodash/chunk';
import React from 'react';

import { Link, withRouter, RouteComponentProps } from 'react-router-dom';

import './StoreList.scss';

import StoreCard from '../StoreCard';
import { Store } from 'services/store/storeReducer';

interface MatchParams {
  storeId: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  items: Store[];
}
const StoreList = ({ items, match }: Props) => {
  const itemsRow = chunk(items, 4);

  if (items.length === 0) {
    return <div className="StoreList__row">No stores found</div>;
  }

  return (
    <React.Fragment>
      {map(itemsRow, (items: Store[][], index: number) => (
        <div key={index} className="StoreList__row--wrap">
          <div className="StoreList__row">
            {map(items, (item: Store) => (
              <Link key={item.id} to={`${match.url}/${item.id}/products`}>
                <StoreCard
                  store_name={item.store_name}
                  image_url={item.image_url}
                />
              </Link>
            ))}
          </div>
        </div>
      ))}
    </React.Fragment>
  );
};

export default withRouter(StoreList);
