import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import StoreFilter from './StoreFilter';
import StoreList from './StoreList';
import StoreLoadMore from './StoreLoadMore';
import Section from 'components/Section';
import { State } from 'services/state';
import { getStoresRequest } from 'services/store/storeActions';
import {
  getStoresLoadingSelector,
  getStoresSelector,
  getStoresPaginationSelector,
} from 'services/store/storeSelectors';
import {
  StoreParams,
  Store as IStore,
  Pagination as IPagination,
} from 'services/store/storeReducer';

import LoadingContainer from 'components/LoadingContainer';
import Pagination from 'components/Pagination';

interface Props {
  getStoresRequest: (params?: StoreParams, isPagination?: boolean) => void;
  isStoresLoading: boolean;
  stores: IStore[];
  storesPagination: IPagination;
}

const PAGINATION = {
  per_page: 8,
  page: 1,
};

const Store = ({
  getStoresRequest,
  stores,
  isStoresLoading,
  storesPagination,
}: Props) => {
  useEffect(() => {
    getStoresRequest({ page: PAGINATION.page, per_page: PAGINATION.per_page });
  }, []);

  const handleLoadMore = (page: number, per_page: number) => {
    getStoresRequest({ page, per_page }, true);
  };

  return (
    <Section className="Store">
      <StoreFilter />
      <Section.Heading width="950px" />
      <Pagination onLoadMore={handleLoadMore} perPage={PAGINATION.per_page}>
        {({
          loadMore,
        }: {
          loadMore: (page: number, per_page: number) => void;
        }) => (
          <React.Fragment>
            <LoadingContainer
              isLoading={isStoresLoading}
              className="Stores__main"
            >
              <StoreList items={stores} />
            </LoadingContainer>
            <StoreLoadMore
              disabled={!storesPagination.has_next}
              onClick={loadMore}
            />
          </React.Fragment>
        )}
      </Pagination>
    </Section>
  );
};

const mapStateToProps = (state: State) => ({
  isStoresLoading: getStoresLoadingSelector(state),
  stores: getStoresSelector(state),
  storesPagination: getStoresPaginationSelector(state),
});

const actions: any = {
  getStoresRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(Store);
