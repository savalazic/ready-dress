import React from 'react';
import { Container } from 'semantic-ui-react';
import Section from 'components/Section';

const About = () => {
  return (
    <Section>
      <Container>
        <h2>About us</h2>
        <p>
          ReadyDress is modernizing and bringing much needed convenience to the
          in-person shopping retail industry. We allow you to enjoy the pros of
          both online and store-front shopping while eliminating the cons!
          You’ll be hooked.
        </p>
        <p>
          ReadyDress was founded in 2019 by Hayley Radosevich-Nelson when she
          walked into a retailer and grew increasingly frustrated at the
          logistics of store-front shopping – there were so many people, it was
          time consuming, and couldn’t she couldn’t find anything that she saw
          online! “There’s got to be a better way,” she said to a friend. A
          couple months later, ReadyDress came into fruition and it’s been a
          magical journey ever since!
        </p>
      </Container>
    </Section>
  );
};

export default About;
