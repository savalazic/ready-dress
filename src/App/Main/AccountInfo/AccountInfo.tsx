import React from 'react';
import { connect } from 'react-redux';

import { Formik, Field } from 'formik';

import './AccountInfo.scss';

import LoadingContainer from 'components/LoadingContainer';
import Form from 'components/Form';
import Input from 'components/Input';
import { PrimaryButton } from 'components/Button';
import {
  getAuthUser,
  getAuthUserLoadingSelector,
  updateAuthUserLoadingSelector,
} from 'services/auth/authSelectors';
import { State } from 'services/state';
import DateInput from 'components/DateInput';
import { updateAuthUserRequest } from 'services/auth/authActions';

interface IProps {
  user: any;
  isUserLoading: boolean;
  isUpdateUserLoading: boolean;
}

type DispatchProps = typeof actions;
type Props = DispatchProps & IProps;

const AccountInfo: React.SFC<Props> = ({
  user,
  isUserLoading,
  updateAuthUserRequest,
  isUpdateUserLoading,
}) => {
  const handleSumbit = (values: any) => {
    updateAuthUserRequest(values);
  };

  if (!user) {
    return <h2>User doesn't exist</h2>;
  }

  return (
    <div className="AccountInfo">
      <h2>Account Info</h2>
      <LoadingContainer isLoading={isUserLoading || isUpdateUserLoading}>
        <Formik
          initialValues={{
            first_name: user.first_name,
            last_name: user.last_name,
            phone: user.phone,
            zipcode: user.zipcode,
            dob: user.dob,
          }}
          onSubmit={handleSumbit}
        >
          {() => (
            <Form>
              <Field
                component={Input}
                name="first_name"
                id="first_name"
                placeholder="First Name"
                label="First Name"
              />
              <Field
                component={Input}
                name="last_name"
                id="last_name"
                placeholder="Last Name"
                label="Last Name"
              />
              <Field
                component={Input}
                name="phone"
                id="phone"
                placeholder="Phone Number"
                label="Phone Number"
              />
              <Field
                component={Input}
                name="zipcode"
                id="zipcode"
                placeholder="Zip Code"
                label="Zip Code"
              />
              <Field
                component={DateInput}
                name="dob"
                id="dob"
                placeholder="Birthdate"
                label="Birthdate"
              />
              <PrimaryButton loading={false} type="submit">
                Submit
              </PrimaryButton>
            </Form>
          )}
        </Formik>
      </LoadingContainer>
    </div>
  );
};

const actions = {
  updateAuthUserRequest,
};

const mapStateToProps = (state: State) => ({
  user: getAuthUser(state),
  isUserLoading: getAuthUserLoadingSelector(state),
  isUpdateUserLoading: updateAuthUserLoadingSelector(state),
});

export default connect(
  mapStateToProps,
  actions,
)(AccountInfo);
