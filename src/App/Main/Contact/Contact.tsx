import React from 'react';
import { Container } from 'semantic-ui-react';
import Section from 'components/Section';

const Contat = () => {
  return (
    <Section>
      <Container>
        <h2>Contat us</h2>
        <p>
          Questions? Concerns? Feedback? Please email{' '}
          <a href="mailto:hayley@ready-dress.com" target="_blank">
            hayley@ready-dress.com
          </a>{' '}
        </p>
      </Container>
    </Section>
  );
};

export default Contat;
