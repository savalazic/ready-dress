import React from 'react';
import { Container } from 'semantic-ui-react';
import { Formik, Field, FormikErrors } from 'formik';
import { object, string } from 'yup';
import { connect } from 'react-redux';

import './PasswordResetRequest.scss';

import { isEmailValid } from 'utils/validation';
import Form from 'components/Form';
import Input from 'components/Input';
import { passwordResetRequestRequest } from 'services/auth/authActions';
import { PrimaryButton } from 'components/Button';
import { State } from 'services/state';
import { getRequestPasswordLoadingSelector } from 'services/auth/authSelectors';

interface Props {
  passwordResetRequestRequest: (email: string) => {};
  isRequestPasswordResetLoading: boolean;
}

interface FormValues {
  email: string;
}

const validationSchema = object().shape({
  email: string().required('Required'),
});

const validate = (values: FormValues) => {
  const errors: FormikErrors<FormValues> = {};

  if (!isEmailValid(values.email)) {
    errors.email = 'Email not valid';
  }
  return errors;
};

const PasswordResetRequest = ({
  passwordResetRequestRequest,
  isRequestPasswordResetLoading,
}: Props) => {
  const handleSubmit = (values: FormValues) => {
    passwordResetRequestRequest(values.email);
  };

  return (
    <Container>
      <h2 className="PasswordResetRequest__heading">Forgot password?</h2>
      <Formik
        initialValues={{ email: '' }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        validate={validate}
      >
        {() => (
          <Form className="PasswordResetRequest__form">
            <Field
              component={Input}
              name="email"
              label="Email"
              id="email"
              disabled={isRequestPasswordResetLoading}
            />
            <PrimaryButton
              type="submit"
              loading={isRequestPasswordResetLoading}
            >
              Submit
            </PrimaryButton>
          </Form>
        )}
      </Formik>
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  isRequestPasswordResetLoading: getRequestPasswordLoadingSelector(state),
});

const actions: any = {
  passwordResetRequestRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(PasswordResetRequest);
