import React from 'react';
import { Container } from 'semantic-ui-react';
import { Formik, Field, FormikErrors } from 'formik';
import { object, string } from 'yup';
import { connect } from 'react-redux';

import { isEmailValid } from 'utils/validation';
import Form from 'components/Form';
import Input from 'components/Input';
import PasswordInput from 'components/PasswordInput';
import { loginRequest } from 'services/auth/authActions';
import { PrimaryButton } from 'components/Button';
import './Login.scss';
import { State } from 'services/state';
import { getLoginLoadingSelector } from 'services/auth/authSelectors';
import ModalLink from 'components/ModalLink';

interface Props {
  loginRequest: (email: string, password: string) => {};
  isLoginLoading: boolean;
}

interface FormValues {
  email: string;
  password: string;
}

const validationSchema = object().shape({
  email: string().required('Required'),
  password: string().required('Required'),
});

const validate = (values: FormValues) => {
  const errors: FormikErrors<FormValues> = {};

  if (!isEmailValid(values.email)) {
    errors.email = 'Email not valid';
  }
  return errors;
};

const Login = ({ loginRequest, isLoginLoading }: Props) => {
  const handleSubmit = (values: FormValues) => {
    loginRequest(values.email, values.password);
  };

  return (
    <Container>
      <h2 className="Login__heading">Login</h2>
      <Formik
        initialValues={{ email: '', password: '' }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        validate={validate}
      >
        {() => (
          <Form className="Login__form">
            <Field
              component={Input}
              name="email"
              label="Email"
              id="email"
              disabled={isLoginLoading}
            />
            <Field
              component={PasswordInput}
              name="password"
              label="Password"
              id="password"
              disabled={isLoginLoading}
            />
            <PrimaryButton type="submit" loading={isLoginLoading}>
              Submit
            </PrimaryButton>
          </Form>
        )}
      </Formik>
      <ModalLink to="/password_request" className="Login__forgot-password">
        <span>Forgot your password?</span>
      </ModalLink>
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  isLoginLoading: getLoginLoadingSelector(state),
});

const actions: any = {
  loginRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(Login);
