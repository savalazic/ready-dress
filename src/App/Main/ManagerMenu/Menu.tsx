import React from 'react';

import ModalLink from 'components/ModalLink';
import { PrimaryButton, SecondaryButton } from 'components/Button';
import LogoutButton from 'components/Button/LogoutButton';

import './Menu.scss';

const MenuLink = ({ to, children }: any) => (
  <PrimaryButton className="Menu__item" as={ModalLink} to={to} fluid>
    {children}
  </PrimaryButton>
);

const Menu = () => {
  return (
    <div className="Menu">
      <MenuLink to="/menu/manager/account-info">Account Info</MenuLink>
      <MenuLink to="/menu/manager/appointment-requests">
        Appointment Requests
      </MenuLink>
      <MenuLink to="/menu/manager/upcoming-appointments">
        Upcoming appointments
      </MenuLink>
      <MenuLink to="/menu/manager/past-appointments">
        Past appointments
      </MenuLink>
      <MenuLink to="/menu/manager/today-appointments">
        Today's appointments
      </MenuLink>
      <LogoutButton className="Menu__item" as={SecondaryButton} fluid />
    </div>
  );
};

export default Menu;
