import React from 'react';

import ModalRoute from 'components/ModalRoute';

import Menu from '../Menu';

import AppointmentRequestsItems from '../AppointmentRequestsItems';
import PastAppointments from '../PastAppointments';

import TodayAppointments from '../TodayAppointments';
import StoreBranchList from 'App/Main/Merchant/StoreBranches/StoreBranchList';
import { withRouter } from 'react-router';
import FutureAppointments from '../FutureAppointments';
import AccountInfo from 'App/Main/AccountInfo';

const MenuBranchesList = withRouter(props => (
  <StoreBranchList {...props} isModal />
));

const MenuRouter = () => {
  return (
    <>
      <ModalRoute component={Menu} exact path="/menu/manager/" />
      <ModalRoute
        component={AccountInfo}
        path="/menu/manager/account-info"
        hasBack
      />
      <ModalRoute
        path="/menu/manager/appointment-requests"
        exact
        hasBack
        component={MenuBranchesList}
      />
      <ModalRoute
        component={AppointmentRequestsItems}
        path="/menu/manager/appointment-requests/:id/items"
        hasBack
      />
      <ModalRoute
        component={MenuBranchesList}
        path="/menu/manager/past-appointments"
        hasBack
        exact
      />
      <ModalRoute
        component={PastAppointments}
        path="/menu/manager/past-appointments/:id/items"
        hasBack
      />
      <ModalRoute
        component={MenuBranchesList}
        path="/menu/manager/upcoming-appointments"
        hasBack
        exact
      />
      <ModalRoute
        component={FutureAppointments}
        hasBack
        path="/menu/manager/upcoming-appointments/:id/items"
      />
      <ModalRoute
        component={MenuBranchesList}
        hasBack
        path="/menu/manager/today-appointments"
        exact
      />
      <ModalRoute
        component={TodayAppointments}
        path="/menu/manager/today-appointments/:id/items"
        hasBack
      />
    </>
  );
};

export default MenuRouter;
