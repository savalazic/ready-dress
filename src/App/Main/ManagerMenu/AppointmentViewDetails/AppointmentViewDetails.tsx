import React from 'react';

import BaseModal from 'components/BaseModal';
import { AddIcon } from 'components/Icons';
import { Modal } from 'semantic-ui-react';
import { StoreItem } from 'services/merchant/storeItem/storeItemReducer';

import defaultImage from 'assets/images/No_Image_Available.jpg';

interface AppointmentViewDetailsProps {
  items: StoreItem[];
}
const AppointmentViewDetails = ({ items }: AppointmentViewDetailsProps) => {
  return (
    <div className="AppointmentRequestItem__order">
      <span>Order details</span>
      <BaseModal>
        {({ isModalOpen, openModal, closeModal }) => (
          <div
            className="AppointmentRequestItem__order__icon AppointmentRequestItem__icon"
            onClick={openModal}
          >
            <AddIcon />
            <Modal open={isModalOpen} onClose={closeModal}>
              <div className="AppointmentRequestItem__order-modal">
                <h2>Order details</h2>

                {items.length === 0 && <p>There is no items</p>}
                <div className="AppointmentRequestItem__order-grid">
                  {items.map(item => (
                    <div key={item.id}>
                      <img
                        src={item.images[0] ? item.images[0].url : defaultImage}
                        alt="order"
                      />
                      <h3>{item.title}</h3>
                      <p>{item.price_in_centum}$</p>
                    </div>
                  ))}
                </div>
              </div>
            </Modal>
          </div>
        )}
      </BaseModal>
    </div>
  );
};

export default AppointmentViewDetails;
