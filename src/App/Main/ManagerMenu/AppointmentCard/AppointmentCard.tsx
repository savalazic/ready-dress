import React from 'react';
import AppointmentRequestItem from '../AppointmentRequestItem';
import { StoreBranchAppointment } from 'services/merchant/storeBranch/storeBranchReducer';
import { Icon } from 'semantic-ui-react';

const AppointmentCard = (props: StoreBranchAppointment) => {
  return (
    <AppointmentRequestItem {...props}>
      <div className="AppointmentRequestItem__icon AppointmentRequestItem__message">
        <Icon name="mail outline" color="black" size="large" />
      </div>
    </AppointmentRequestItem>
  );
};

export default AppointmentCard;
