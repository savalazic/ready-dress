import React, { useEffect } from 'react';
import { getAppointmentsForStoreBranchLoadingSelector } from 'services/merchant/storeBranch/storeBranchSelectors';
import { State } from 'services/state';
import {
  getAppointmentsForStoreBranchRequest,
  getStoreBranchesRequest,
  setSelectedStoreBranch,
} from 'services/merchant/storeBranch/storeBranchActions';
import isEmpty from 'lodash/isEmpty';
import map from 'lodash/map';
import { RouteComponentProps, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { StoreBranchAppointment } from 'services/merchant/storeBranch/storeBranchReducer';
import LoadingContainer from 'components/LoadingContainer';
import AppointmentCard from '../AppointmentCard';

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;
interface IProps {
  appointments: { [key: number]: StoreBranchAppointment };
  noDataText: string;
}

type Props = DispatchProps &
  StateProps &
  RouteComponentProps<{ id?: string }> &
  IProps;

const AppointmentsList = ({
  appointments,
  isLoading,
  match,
  noDataText,
  getAppointmentsForStoreBranchRequest,
  getStoreBranchesRequest,
  setSelectedStoreBranch,
}: Props) => {
  useEffect(() => {
    const branchId = Number(match.params.id);
    getStoreBranchesRequest(branchId);
    setSelectedStoreBranch(branchId);
    getAppointmentsForStoreBranchRequest(branchId);
  }, [
    getAppointmentsForStoreBranchRequest,
    getStoreBranchesRequest,
    match.params.id,
    setSelectedStoreBranch,
  ]);

  if (!isLoading && isEmpty(appointments)) {
    return <div>{noDataText}</div>;
  }

  return (
    <LoadingContainer isLoading={isLoading}>
      {map(appointments, item => (
        <AppointmentCard key={item.id} {...item} />
      ))}
    </LoadingContainer>
  );
};

const mapStateToProps = (state: State) => ({
  isLoading: getAppointmentsForStoreBranchLoadingSelector(state),
});

const actions = {
  getAppointmentsForStoreBranchRequest,
  getStoreBranchesRequest,
  setSelectedStoreBranch,
};

export default connect(
  mapStateToProps,
  actions,
)(withRouter(AppointmentsList));
