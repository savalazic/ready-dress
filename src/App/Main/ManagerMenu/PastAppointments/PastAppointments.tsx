import React from 'react';
import { State } from 'services/state';
import { getPastAppointments } from 'services/merchant/storeBranch/storeBranchSelectors';
import { connect } from 'react-redux';
import AppointmentsList from '../AppointmentsList';

type Props = ReturnType<typeof mapStateToProps>;

const PastAppointments = ({ pastAppointments }: Props) => {
  return (
    <>
      <h3>Past Appointments</h3>

      <AppointmentsList
        appointments={pastAppointments}
        noDataText="No past appointments"
      />
    </>
  );
};

const mapStateToProps = (state: State) => ({
  pastAppointments: getPastAppointments(state),
});

export default connect(mapStateToProps)(PastAppointments);
