import React, { ReactNode } from 'react';
import { formatDate, formatTime } from 'utils/date';
import { StoreBranchAppointment } from 'services/merchant/storeBranch/storeBranchReducer';
import cx from 'classnames';
import AppointmentViewDetails from '../AppointmentViewDetails';

import './AppointmentRequestItem.scss';

interface Props extends StoreBranchAppointment {
  children: ReactNode;
}

const AppointmentRequestItem = ({
  children,
  from,
  to,
  status,
  items,
}: Props) => {
  return (
    <div
      className={cx(
        'AppointmentRequestItem',
        status === 'pending_with_new_dates' &&
          'AppointmentRequestItem--disabled',
      )}
    >
      <div className="AppointmentRequestItem__date">
        {formatDate(new Date(from))}
      </div>
      <div className="AppointmentRequestItem__wrap">
        <div className="AppointmentRequestItem__time">
          {formatTime(new Date(from))}-{formatTime(new Date(to))}
        </div>
        <AppointmentViewDetails items={items} />
        {children}
      </div>
    </div>
  );
};

export default AppointmentRequestItem;
