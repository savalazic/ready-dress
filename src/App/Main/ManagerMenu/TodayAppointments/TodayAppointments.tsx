import React from 'react';
import { State } from 'services/state';
import { getTodaysAppointments } from 'services/merchant/storeBranch/storeBranchSelectors';
import { connect } from 'react-redux';
import AppointmentsList from '../AppointmentsList';

type Props = ReturnType<typeof mapStateToProps>;

const TodayAppointments = ({ todayAppointments }: Props) => {
  return (
    <>
      <h3>Today's appointments</h3>

      <AppointmentsList
        appointments={todayAppointments}
        noDataText="No today's appointments"
      />
    </>
  );
};

const mapStateToProps = (state: State) => ({
  todayAppointments: getTodaysAppointments(state),
});

export default connect(mapStateToProps)(TodayAppointments);
