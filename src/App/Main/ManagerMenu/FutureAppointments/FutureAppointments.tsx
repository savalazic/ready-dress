import React from 'react';
import { State } from 'services/state';
import { getFutureAppointments } from 'services/merchant/storeBranch/storeBranchSelectors';
import { connect } from 'react-redux';
import AppointmentsList from '../AppointmentsList';

type Props = ReturnType<typeof mapStateToProps>;

const FutureAppointments = ({ futureAppointments }: Props) => {
  return (
    <>
      <h3>Upcoming Appointments</h3>
      <AppointmentsList
        appointments={futureAppointments}
        noDataText="No future appointments"
      />
    </>
  );
};

const mapStateToProps = (state: State) => ({
  futureAppointments: getFutureAppointments(state),
});

export default connect(mapStateToProps)(FutureAppointments);
