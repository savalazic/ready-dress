import React from 'react';
import BaseModal from 'components/BaseModal';
import { Modal, Icon, Grid } from 'semantic-ui-react';
import { Formik, Field, FormikErrors } from 'formik';
import Form from 'components/Form';
import { PrimaryButton, SecondaryButton } from 'components/Button';
import TextArea from 'components/TextArea';

import DateTimeInput from 'components/DateTimeInput';
import { object, string } from 'yup';
import moment from 'moment';

interface FormValues {
  from: string;
  to: string;
  comment: string;
}

interface Props {
  from: string;
  to: string;
  onSubmit(params: FormValues): void;
  isLoading: boolean;
}

const dateFormat = 'YYYY-MM-DD hh:mm';

const validateSchema = object().shape({
  from: string().required('Required'),
  to: string().required('Required'),
  comment: string().required('Required'),
});

const validate = (values: FormValues) => {
  const errors: FormikErrors<FormValues> = {};

  if (
    !moment(values.from, dateFormat).isSame(
      moment(values.to, dateFormat),
      'day',
    )
  ) {
    errors.from = 'Must be on the same day';
  }

  if (moment(values.from, dateFormat).isAfter(moment(values.to, dateFormat))) {
    errors.from = 'From date must be before to date';
  }

  return errors;
};

const AppointmentRequestNewTime = ({
  onSubmit,
  from,
  to,
  isLoading,
}: Props) => {
  return (
    <div className="AppointmentRequestItem__new-date">
      <BaseModal>
        {({ isModalOpen, openModal, closeModal }) => (
          <div className="AppointmentRequestItem__icon">
            <Icon
              name="mail outline"
              onClick={openModal}
              size="large"
              color="grey"
              inverted
            />
            <Modal
              open={isModalOpen}
              onClose={closeModal}
              closeIcon
              size="small"
            >
              <Modal.Content>
                <h3>Set new time</h3>
                <Formik
                  initialValues={{
                    from: moment(from).format(dateFormat),
                    to: moment(to).format(dateFormat),
                    comment: '',
                  }}
                  validate={validate}
                  validationSchema={validateSchema}
                  onSubmit={values => {
                    onSubmit(values);
                    closeModal();
                  }}
                >
                  {({ isValid }) => (
                    <Form>
                      <Grid columns="equal">
                        <Grid.Row>
                          <Grid.Column>
                            <Field
                              component={DateTimeInput}
                              id="from"
                              name="from"
                              label="From"
                              clearable
                            />
                          </Grid.Column>
                          <Grid.Column>
                            <Field
                              component={DateTimeInput}
                              id="to"
                              name="to"
                              label="To"
                              clearable
                            />
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column>
                            <Field
                              component={TextArea}
                              id="comment"
                              label="Message"
                              name="comment"
                              placeholder="Reason for setting new time"
                            />
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column>
                            <SecondaryButton fluid onClick={closeModal}>
                              Cancel
                            </SecondaryButton>
                          </Grid.Column>
                          <Grid.Column>
                            <PrimaryButton
                              loading={isLoading}
                              disabled={!isValid}
                              type="submit"
                              fluid
                            >
                              Save
                            </PrimaryButton>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Form>
                  )}
                </Formik>
              </Modal.Content>
            </Modal>
          </div>
        )}
      </BaseModal>
    </div>
  );
};

export default AppointmentRequestNewTime;
