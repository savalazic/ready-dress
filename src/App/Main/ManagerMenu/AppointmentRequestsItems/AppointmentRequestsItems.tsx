import React, { Component, Fragment } from 'react';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';
import map from 'lodash/map';
import isEmpty from 'lodash/isEmpty';

import {
  getAppointmentsForStoreBranchRequest,
  setSelectedStoreBranch,
  getStoreBranchesRequest,
  acceptAppointmentRequest,
  declineAppointmentRequest,
  suggestNewTimeRequest,
} from 'services/merchant/storeBranch/storeBranchActions';
import {
  getAppointmentRequests,
  getAppointmentsForStoreBranchLoadingSelector,
  isAppointmentRequestForStoreBranchLoading,
} from 'services/merchant/storeBranch/storeBranchSelectors';
import { State } from 'services/state';
import { StoreBranchAppointment } from 'services/merchant/storeBranch/storeBranchReducer';
import LoadingContainer from 'components/LoadingContainer';

import { CheckMarkIcon } from 'components/Icons';
import AppointmentRequestDecline from './AppointmentRequestDecline';
import AppointmentRequestNewTime from './AppointmentRequestNewTime';
import AppointmentRequestItem from '../AppointmentRequestItem';

interface Props extends RouteComponentProps<{ id?: string }> {
  getAppointmentsForStoreBranchRequest: (branchId: number) => void;
  appointmentRequests: { [id: number]: StoreBranchAppointment };
  isLoading: boolean;
  setSelectedStoreBranch: (branchId: number) => void;
  getStoreBranchesRequest(branchId: number): void;
  isItemLoading: boolean;
}

class AppointmentRequestsItems extends Component<Props & typeof actions> {
  componentDidMount() {
    const branchId = Number(this.props.match.params.id);
    // send request if user arrives to this page
    this.props.getStoreBranchesRequest(branchId);
    this.props.getAppointmentsForStoreBranchRequest(branchId);
    this.props.setSelectedStoreBranch(branchId);
  }

  renderEmpty() {
    return <div>No new appointments</div>;
  }

  render() {
    const {
      isLoading,
      appointmentRequests,
      acceptAppointmentRequest,
      declineAppointmentRequest,
      suggestNewTimeRequest,
      isItemLoading,
      match: { params },
    } = this.props;
    const branchId = Number(params.id);

    if (!isLoading && isEmpty(appointmentRequests)) {
      return this.renderEmpty();
    }

    return (
      <LoadingContainer isLoading={isLoading || isItemLoading}>
        <h3>Appointment Requests</h3>
        {map(appointmentRequests, item => (
          <Fragment key={item.id}>
            <AppointmentRequestItem {...item}>
              <div
                onClick={() => acceptAppointmentRequest(branchId, item.id)}
                className="AppointmentRequestItem__approve AppointmentRequestItem__icon"
              >
                <CheckMarkIcon />
              </div>
              <AppointmentRequestDecline
                isLoading={isItemLoading}
                onDecline={(comment: string) => {
                  declineAppointmentRequest(branchId, item.id, comment);
                }}
              />
              <AppointmentRequestNewTime
                isLoading={isItemLoading}
                from={item.from}
                to={item.to}
                onSubmit={({ from, to, comment }) =>
                  suggestNewTimeRequest({
                    comment,
                    branchId,
                    appointmentId: item.id,
                    from: new Date(from),
                    to: new Date(to),
                  })
                }
              />
            </AppointmentRequestItem>
          </Fragment>
        ))}
      </LoadingContainer>
    );
  }
}

const mapStateToProps = (state: State) => ({
  appointmentRequests: getAppointmentRequests(state),
  isLoading: getAppointmentsForStoreBranchLoadingSelector(state),
  isItemLoading: isAppointmentRequestForStoreBranchLoading(state),
});

const actions = {
  getAppointmentsForStoreBranchRequest,
  setSelectedStoreBranch,
  getStoreBranchesRequest,
  acceptAppointmentRequest,
  declineAppointmentRequest,
  suggestNewTimeRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(AppointmentRequestsItems);
