import React from 'react';

import { DeleteIcon } from 'components/Icons';
import BaseModal from 'components/BaseModal';
import { Modal } from 'semantic-ui-react';
import Form from 'components/Form';
import TextArea from 'components/TextArea';
import { Formik, Field } from 'formik';
import { object, string } from 'yup';
import { PrimaryButton } from 'components/Button';

interface Props {
  onDecline(comment: string): void;
  isLoading: boolean;
}

interface FormValues {
  comment: string;
}

const validationSchema = object().shape({
  comment: string().required('Required'),
});

const AppointmentRequestDecline = ({ onDecline, isLoading }: Props) => {
  const handleSubmit = (values: FormValues) => onDecline(values.comment);

  return (
    <div className="AppointmentRequestItem__decline ">
      <BaseModal>
        {({ isModalOpen, closeModal, openModal }) => (
          <div className="AppointmentRequestItem__icon">
            <div onClick={openModal}>
              <DeleteIcon />
            </div>

            <Modal
              open={isModalOpen}
              onClose={closeModal}
              size="mini"
              closeIcon
            >
              <Modal.Header>
                <h3>Decline Appointment Request</h3>
              </Modal.Header>
              <Modal.Content>
                <Formik
                  initialValues={{ comment: '' }}
                  validationSchema={validationSchema}
                  onSubmit={handleSubmit}
                >
                  {({ isValid }) => (
                    <Form>
                      <Field
                        component={TextArea}
                        id="comment"
                        placeholder="Reason for declining appointment request"
                        label="Comment"
                        name="comment"
                      />
                      {/* TODO: loading */}
                      <PrimaryButton
                        loading={isLoading}
                        disabled={!isValid}
                        type="submit"
                        fluid
                      >
                        Submit
                      </PrimaryButton>
                    </Form>
                  )}
                </Formik>
              </Modal.Content>
            </Modal>
          </div>
        )}
      </BaseModal>
    </div>
  );
};

export default AppointmentRequestDecline;
