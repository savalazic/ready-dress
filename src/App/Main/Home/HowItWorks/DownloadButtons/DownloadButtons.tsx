import React from 'react';
import './DownloadButtons.scss';
import { Image } from 'semantic-ui-react';

import AppleButton from 'assets/images/app-store.png';
import GoogleButton from 'assets/images/google-play.png';

const DownloadButtons = () => {
  return (
    <div className="DownloadButtons">
      <a href="#todo">
        <Image src={GoogleButton} width="150" />
      </a>
      <a href="#todo">
        <Image src={AppleButton} width="150" />
      </a>
    </div>
  );
};

export default DownloadButtons;
