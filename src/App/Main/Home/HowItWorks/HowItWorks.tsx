import AppHand from 'assets/images/app-hand.png';
import Section from 'components/Section';
import React from 'react';
import { Grid } from 'semantic-ui-react';
import DownloadButtons from './DownloadButtons';
import './HowItWorks.scss';

const HowItWorks = () => {
  return (
    <Section className="HowItWorks">
      <Grid container>
        <Grid.Row verticalAlign="middle" className="HowItWorks__row">
          <Grid.Column mobile={16} tablet={16} computer={8}>
            <div className="HowItWorks__wrap">
              <p className="HowItWorks__description">
                Download the app on iOS
                <br /> and Android
                <span> (Coming soon!)</span>
              </p>
              <DownloadButtons />
            </div>
          </Grid.Column>
          <Grid.Column
            mobile={16}
            tablet={16}
            computer={8}
            className="HowItWorks__imageWrap"
          >
            <img src={AppHand} className="HowItWorks__image" alt="AppHand" />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Section>
  );
};

export default HowItWorks;
