import Observer from '@researchgate/react-intersection-observer';
import cls from 'classnames';
import Section from 'components/Section';
import React, { PureComponent } from 'react';
import { Grid } from 'semantic-ui-react';

import './Why.scss';

class Why extends PureComponent {
  public state = { isIntersecting: false };

  public handleIntersection = (event: any, unobserve: any) => {
    if (event.isIntersecting) {
      unobserve();
    }
    this.setState({ isIntersecting: event.isIntersecting ? true : false });
  };

  public render() {
    return (
      <Observer onChange={this.handleIntersection} threshold={0.2}>
        <div>
          <Section
            className={cls('Why', {
              'Why--isInViewport': this.state.isIntersecting,
            })}
          >
            <Section.Heading align="right" width="1400px">
              Why?
            </Section.Heading>
            <Grid container>
              <Grid.Row>
                <Grid.Column mobile={16} tablet={16} computer={9}>
                  <div className="Why__images">
                    <div className="Why__image Why__image--large" />
                    <div className="Why__image" />
                    <div className="Why__image" />
                    <div className="Why__image" />
                    <div className="Why__image Why__image--large" />
                    <div className="Why__image" />
                  </div>
                </Grid.Column>
                <Grid.Column mobile={16} tablet={16} computer={7}>
                  <div className="Why__item">
                    <h3>Quick, fun and convenient</h3>
                    <h2>Try on exactly what you want</h2>
                  </div>
                  <div className="Why__item">
                    <h3>
                      No hassle of waiting for shipping and returning unwanted
                      items
                    </h3>
                    <h2>No browsing through aisle after aisle</h2>
                  </div>
                  <div className="Why__item">
                    <h3>Discover new local boutiques</h3>
                    <h2>
                      Save time! All your shopping can be done in a 30 minute
                      lunch break
                    </h2>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Section>
        </div>
      </Observer>
    );
  }
}

export default Why;
