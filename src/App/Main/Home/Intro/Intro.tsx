import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import { connect } from 'react-redux';

import './Intro.scss';

import { getGridRequest } from 'services/store/storeActions';
import { State } from 'services/state';
import {
  getGridItems,
  getGridLoadingSelector,
} from 'services/store/storeSelectors';

import Card from 'components/Card';
import ImageSlideshow from 'components/ImageSlideshow';
import Section from 'components/Section';
import LetsStart from './LetsStart';
import LoadingContainer from 'components/LoadingContainer';
import { getIsLoggedIn } from 'services/auth/authSelectors';

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;
type Props = DispatchProps & StateProps;

const Intro = ({
  getGridRequest,
  gridItems,
  isGridLoading,
  isLoggedIn,
}: Props) => {
  useEffect(() => {
    getGridRequest({});
  }, []);

  return (
    <Section className="Intro">
      <LoadingContainer isLoading={isGridLoading} className="Intro__grid">
        <div className="Intro__title Intro__title--top">
          <h1>Your new</h1>
          <Section.Heading>modern way to shop</Section.Heading>
        </div>
        <div className="Intro__title--bottom">
          <Section.Heading>
            The perfect blend between online and in-person retail shopping.
          </Section.Heading>
          {!isLoggedIn && <LetsStart />}
        </div>
        {gridItems.map((item: any, i: number) => (
          <Card
            key={i}
            className={cx({
              'Intro__featured--right': i === 0,
              'Intro__featured--top': i === 1,
            })}
          >
            {item.images.length === 1 ? (
              <Link
                to={`/stores/${item.store_branch_id}/products/${item.item_id}`}
              >
                <Card.Image image={item.images[0]} />
              </Link>
            ) : (
              <Link
                to={`/stores/${item.store_branch_id}/products/${item.item_id}`}
              >
                <ImageSlideshow images={item.images} />
              </Link>
            )}
          </Card>
        ))}
      </LoadingContainer>
    </Section>
  );
};

const mapStateToProps = (state: State) => ({
  gridItems: getGridItems(state),
  isGridLoading: getGridLoadingSelector(state),
  isLoggedIn: getIsLoggedIn(state),
});

const actions = {
  getGridRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(Intro);
