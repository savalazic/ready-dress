import React from 'react';
import Hanger from 'components/Hanger';
import './LetsStart.scss';
import ModalLink from 'components/ModalLink';

const LetsStart = () => {
  return (
    <div className="LetsStart">
      <h2 className="LetsStart__heading">Let's start</h2>
      <div className="LetsStart__circle-wrap">
        <div className="LetsStart__circle4">
          <div className="LetsStart__circle3">
            <div className="LetsStart__circle2">
              <div className="LetsStart__circle1">
                <div className="LetsStart__circle">
                  <ModalLink to="/register">
                    <Hanger width={180} />
                  </ModalLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LetsStart;
