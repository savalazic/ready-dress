import React from 'react';

import HowItWorks from './HowItWorks';
import Intro from './Intro';
import Steps from './Steps';
import Why from './Why';

const Home = () => {
  return (
    <div className="Home">
      <Intro />
      <Steps />
      <Why />
      <HowItWorks />
    </div>
  );
};

export default Home;
