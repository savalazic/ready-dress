import Section from 'components/Section';
import React from 'react';
import { Grid } from 'semantic-ui-react';
import Step from './Step';
import './Steps.scss';

import step1 from 'assets/images/step-1-icon.svg';
import step2 from 'assets/images/step-2-icon.svg';
import step3 from 'assets/images/step-3-icon.svg';

import img1 from 'assets/images/step-1.jpg';
import img2 from 'assets/images/step-2.jpg';
import img3 from 'assets/images/step-3.jpg';

const Steps = () => {
  return (
    <Section className="Steps">
      <Section.Heading className="Steps__heading">How we work</Section.Heading>
      <Grid container centered>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={5}>
            <Step
              alt="Step 1"
              image={img1}
              description="Shop online at your favorite local stores! That’s it, just shop and add to your cart as you normally would."
              icon={step1}
            />
          </Grid.Column>
          <Grid.Column mobile={16} tablet={16} computer={5}>
            <Step
              alt="Step 2"
              image={img2}
              description="When finished, select the most convenient time and date to try on your new selected items."
              icon={step2}
            />
          </Grid.Column>
          <Grid.Column mobile={16} tablet={16} computer={5}>
            <Step
              alt="Step 3"
              image={img3}
              description="Show up to the store at your selected time, and your desired items will be waiting for you in the dressing room. Easy as that!"
              icon={step3}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Section>
  );
};

export default Steps;
