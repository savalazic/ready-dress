import React from 'react';
import './Step.scss';
import { Image } from 'semantic-ui-react';

interface Props {
  image: string;
  icon: any;
  description: string;
  alt: string;
}

const Step = ({ image, icon, description, alt }: Props) => {
  return (
    <div className="Step">
      <Image src={image} alt={alt} />
      <div className="Step__wrap">
        <Image className="Step__icon" src={icon} alt={alt} />
        <p className="Step__description">{description}</p>
      </div>
    </div>
  );
};

export default Step;
