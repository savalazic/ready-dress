import React from 'react';
import { Container } from 'semantic-ui-react';
import Section from 'components/Section';

const ReturnPolicies = () => {
  return (
    <Section>
      <Container>
        <h2>Return Policies</h2>
        <p>Will update this when a new store is added</p>
      </Container>
    </Section>
  );
};

export default ReturnPolicies;
