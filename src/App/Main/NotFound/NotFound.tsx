import React from 'react';
import './NotFound.scss';
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <div className="NotFound">
      <h1 className="NotFound__heading">404</h1>
      <p className="NotFound__message">
        There is nothing here. Go back <Link to="/">Home</Link>
      </p>
    </div>
  );
};

export default NotFound;
