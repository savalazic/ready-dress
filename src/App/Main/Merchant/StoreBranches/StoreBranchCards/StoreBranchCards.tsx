import React from 'react';
import map from 'lodash/map';
import StoreBranchCard from '../StoreBranchCard';
import { StoreBranch } from 'services/merchant/storeBranch/storeBranchReducer';

interface Props {
  storeBranches: { [id: number]: StoreBranch } | null;
}

const StoreBranchCards = ({ storeBranches }: Props) => {
  return (
    <div className="StoreBranchCards">
      {map(storeBranches, (storeBranch: StoreBranch) => (
        <StoreBranchCard
          key={String(storeBranch.id)}
          id={storeBranch.id}
          city={storeBranch.city}
          phone_number={storeBranch.phone_number}
          state={storeBranch.state}
          street_name={storeBranch.street_name}
          street_number={storeBranch.street_number}
        />
      ))}
    </div>
  );
};

export default StoreBranchCards;
