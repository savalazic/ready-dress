import React from 'react';
import { Route, RouteComponentProps } from 'react-router-dom';

import StoreBranchList from './StoreBranchList';

const StoreBranches = ({ match }: RouteComponentProps) => (
  <div className="StoreBranches">
    <Route exact path={`${match.path}/`} component={StoreBranchList} />
  </div>
);

export default StoreBranches;
