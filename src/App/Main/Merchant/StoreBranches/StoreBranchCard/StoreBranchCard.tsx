import React from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { Card } from 'semantic-ui-react';
import ModalLink from 'components/ModalLink';
import {
  useIsStoreBranchCardModal,
  useLinkToContext,
} from '../StoreBranchList/StoreBranchList';

interface Props extends RouteComponentProps {
  id: number;
  city: string;
  phone_number: string;
  state: string;
  street_name: string;
  street_number: string;
}

const StoreBranchCard = ({
  match,
  id,
  city,
  phone_number,
  state,
  street_name,
  street_number,
}: Props) => {
  const isModal = useIsStoreBranchCardModal();
  const toLink = useLinkToContext() || 'items';

  return (
    <Card link className="StoreBranchCard">
      <Card.Content
        as={isModal ? ModalLink : Link}
        to={`${match.url}/${id}/${toLink}`}
      >
        <Card.Header>
          {city}, {id}
        </Card.Header>
        <Card.Meta>
          {street_name} {street_number}, {state}
        </Card.Meta>
        <Card.Description>{phone_number}</Card.Description>
      </Card.Content>
    </Card>
  );
};

export default withRouter(StoreBranchCard);
