import React, { Component, useContext } from 'react';
import { connect } from 'react-redux';
import { Container } from 'semantic-ui-react';

import { getStoreBranchesRequest } from 'services/merchant/storeBranch/storeBranchActions';
import LoadingContainer from 'components/LoadingContainer';
import { State } from 'services/state';
import {
  getStoreBranchesLoadingSelector,
  getStoreBranchesSelector,
} from 'services/merchant/storeBranch/storeBranchSelectors';
import StoreBranchCards from '../StoreBranchCards';

interface IProps {
  // TODO: avoid prop drilling?
  isModal?: boolean;
  linkTo?: string;
}

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;

type Props = DispatchProps & StateProps & IProps;

const LinkToContext = React.createContext('');
const ModalLinkContext = React.createContext(false);

export const useIsStoreBranchCardModal = () => useContext(ModalLinkContext);
export const useLinkToContext = () => useContext(LinkToContext);

class StoreBranchList extends Component<Props> {
  componentDidMount() {
    this.props.getStoreBranchesRequest({});
  }

  render() {
    const {
      isStoreBranchesLoading,
      storeBranches,
      isModal,
      linkTo,
    } = this.props;

    return (
      <LinkToContext.Provider value={linkTo as string}>
        <ModalLinkContext.Provider value={!!isModal}>
          <div className="StoreBranches">
            <h2 className="Admin__heading">Your store branches</h2>
            <LoadingContainer isLoading={isStoreBranchesLoading}>
              <Container>
                <StoreBranchCards storeBranches={storeBranches} />
              </Container>
            </LoadingContainer>
          </div>
        </ModalLinkContext.Provider>
      </LinkToContext.Provider>
    );
  }
}

const mapStateToProps = (state: State) => ({
  isStoreBranchesLoading: getStoreBranchesLoadingSelector(state),
  storeBranches: getStoreBranchesSelector(state),
});

const actions = {
  getStoreBranchesRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(StoreBranchList);
