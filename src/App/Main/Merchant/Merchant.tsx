import React from 'react';
import { Route, Redirect, RouteComponentProps } from 'react-router-dom';
import './Merchant.scss';

import StoreBranches from './StoreBranches';
import StoreItems from './StoreItems';

const Merchant = ({ match }: RouteComponentProps) => {
  return (
    <div className="Merchant">
      <Route
        exact
        path={`${match.path}`}
        component={() => <Redirect to={`${match.path}/store-branches`} />}
      />
      <Route path={`${match.path}/store-branches`} component={StoreBranches} />
      <Route
        path={`${match.path}/store-branches/:storeBranchId/items`}
        component={StoreItems}
      />
    </div>
  );
};

export default Merchant;
