import isEmpty from 'lodash/isEmpty';
import React, { Component } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  getItemRequest,
  editItemRequest,
  removeImageRequest,
  addImageRequest,
} from 'services/merchant/storeItem/storeItemActions';
import { connect } from 'react-redux';
import { State } from 'services/state';
import {
  getItemsSelector,
  getEditItemLoadingSelector,
  getItemLoadingSelector,
  getAddImageLoadingSelector,
  getRemoveItemLoadingSelector,
} from 'services/merchant/storeItem/storeItemSelectors';
import {
  StoreItem,
  StoreImage,
} from 'services/merchant/storeItem/storeItemReducer';
import { Container } from 'semantic-ui-react';
import { Formik, Field } from 'formik';
import { object, number, string } from 'yup';
import Form from 'components/Form';
import Input from 'components/Input';
import { PrimaryButton } from 'components/Button';
import LoadingContainer from 'components/LoadingContainer';
import ImageAddRemove from 'components/ImageAddRemove';
import { difference } from 'utils/object';
import { getPrevPath } from 'utils/string';
import TextArea from 'components/TextArea';

interface Props
  extends RouteComponentProps<{ id: string; storeBranchId: string }> {
  getItemRequest(id: number, branchId: number): { item: StoreItem };
  editItemRequest(
    item: StoreItem,
    id: number,
    branchId: number,
  ): { item: StoreItem };
  items: {
    [id: number]: StoreItem;
  };
  isEditItemLoading: boolean;
  isItemLoading: boolean;
  isAddImageLoading: boolean;
  isRemoveImageLoading: boolean;
  removeImageRequest: (
    branchId: number,
    itemId: number,
    imageId: number,
  ) => void;
  addImageRequest: (branchId: number, itemId: number, image: File) => void;
}

interface FormValues {
  price_in_centum: number;
  title: string;
  id?: number;
  images: StoreImage[];
  description: string;
}

const validationSchema = object().shape({
  price_in_centum: number()
    .required('Required')
    .positive(),
  title: string().required('Required'),
});

class StoreAdminEdit extends Component<Props> {
  componentDidMount() {
    if (!this.props.items || !this.props.items[+this.props.match.params.id]) {
      this.props.getItemRequest(
        +this.props.match.params.id,
        +this.props.match.params.storeBranchId,
      );
    }
  }

  handleSubmit = (initialValues: FormValues) => (values: FormValues) => {
    const currentItem =
      this.props.items && this.props.items[+this.props.match.params.id];

    const changedValues = difference(values, initialValues);

    const item: any = {
      ...currentItem,
      ...changedValues,
    };

    if (isEmpty(changedValues)) {
      this.props.history.push(getPrevPath(this.props.match.url, -2));
    } else {
      this.props.editItemRequest(
        item,
        +this.props.match.params.id,
        +this.props.match.params.storeBranchId,
      );
    }
  };

  render() {
    const {
      items,
      match,
      isEditItemLoading,
      isItemLoading,
      isAddImageLoading,
      isRemoveImageLoading,
      removeImageRequest,
      addImageRequest,
    } = this.props;

    const currentItem = items && items[+match.params.id];

    if (!currentItem) return <h2>Item doesn't exist</h2>;

    return (
      <Container>
        <h2>Edit store item</h2>
        <LoadingContainer
          isLoading={
            isEditItemLoading ||
            isItemLoading ||
            isAddImageLoading ||
            isRemoveImageLoading
          }
        >
          <Formik
            initialValues={{
              price_in_centum: currentItem.price_in_centum,
              title: currentItem.title,
              images: currentItem.images,
              description: currentItem.description || '',
            }}
            onSubmit={this.handleSubmit(currentItem)}
            validationSchema={validationSchema}
          >
            {() => (
              <Form>
                <Field
                  component={Input}
                  name="title"
                  placeholder="Title"
                  label="Title"
                  disabled={isEditItemLoading}
                />
                <Field
                  component={Input}
                  name="price_in_centum"
                  placeholder="Price"
                  label="Price"
                  type="number"
                  disabled={isEditItemLoading}
                />
                <Field
                  component={TextArea}
                  name="description"
                  placeholder="Description"
                  label="Description"
                  disabled={isEditItemLoading}
                />
                <Field
                  component={ImageAddRemove}
                  name="images"
                  label="Images"
                  disabled={isEditItemLoading}
                  initialImages={currentItem.images.map((image: any) => ({
                    id: image.id,
                    preview: image.url,
                  }))}
                  handleImageRemove={(imageId: number) => {
                    removeImageRequest(
                      +match.params.storeBranchId,
                      +match.params.id,
                      imageId,
                    );
                  }}
                  handleImageAdd={(image: File) => {
                    addImageRequest(
                      +match.params.storeBranchId,
                      +match.params.id,
                      image,
                    );
                  }}
                />
                {/* CANNOT EDIT/DELETE color samples */}
                {/* <Field
                  component={ImageAddRemove}
                  name="images"
                  label="Images"
                  disabled
                  allowCrop
                  initialImages={currentItem.color_samples.map(
                    ({ id, url }) => ({
                      id,
                      preview: url,
                    }),
                  )}
                  handleImageRemove={(imageId: number) => {
                    removeImageRequest(
                      +match.params.storeBranchId,
                      +match.params.id,
                      imageId,
                    );
                  }}
                /> */}
                <PrimaryButton loading={isEditItemLoading} type="submit">
                  Submit
                </PrimaryButton>
              </Form>
            )}
          </Formik>
        </LoadingContainer>
      </Container>
    );
  }
}

const actions: any = {
  getItemRequest,
  removeImageRequest,
  editItemRequest,
  addImageRequest,
};

const mapStateToProps = (state: State) => ({
  items: getItemsSelector(state),
  isEditItemLoading: getEditItemLoadingSelector(state),
  isItemLoading: getItemLoadingSelector(state),
  isAddImageLoading: getAddImageLoadingSelector(state),
  isRemoveImageLoading: getRemoveItemLoadingSelector(state),
});

export default connect(
  mapStateToProps,
  actions,
)(withRouter(StoreAdminEdit));
