import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Formik, Field } from 'formik';
import { Container } from 'semantic-ui-react';
import { number, object, string } from 'yup';

import { State } from 'services/state';
import { addItemRequest } from 'services/merchant/storeItem/storeItemActions';
import { getAddItemLoadingSelector } from 'services/merchant/storeItem/storeItemSelectors';

import { PrimaryButton } from 'components/Button';
import Form from 'components/Form';
import Input from 'components/Input';
import ImagesUpload from 'components/ImagesUpload';
import TextArea from 'components/TextArea';

interface MatchParams {
  storeBranchId: string;
}
interface Props extends RouteComponentProps<MatchParams> {
  addItemRequest: (
    item: { title: string; price_in_centum: number },
    branchId: number,
  ) => void;
  isAddItemLoading: boolean;
}

interface FormValues {
  price_in_centum: number;
  title: string;
  images: string[];
  description: string;
  color_samples: string[];
}

const validationSchema = object().shape({
  price_in_centum: number()
    .required('Required')
    .positive('Price must be a positive number'),
  title: string().required('Required'),
});
const StoreItemAdd = ({ addItemRequest, match, isAddItemLoading }: Props) => {
  const handleSubmit = (values: FormValues) => {
    const item = {
      price_in_centum: values.price_in_centum,
      title: values.title,
      images: values.images,
      description: values.description,
      color_samples: values.color_samples,
    };

    addItemRequest(item, +match.params.storeBranchId);
  };

  return (
    <Container>
      <Formik
        initialValues={{
          price_in_centum: 0,
          title: '',
          images: [],
          description: '',
          color_samples: [],
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        {() => (
          <Form>
            <Field
              component={Input}
              name="title"
              placeholder="Title"
              label="Title"
              disabled={isAddItemLoading}
            />
            <Field
              component={Input}
              name="price_in_centum"
              placeholder="Price"
              label="Price"
              type="number"
              disabled={isAddItemLoading}
            />
            <Field
              component={TextArea}
              name="description"
              placeholder="Description"
              label="Description"
              disabled={isAddItemLoading}
            />
            <Field
              component={ImagesUpload}
              name="images"
              label="Images"
              disabled={isAddItemLoading}
            />
            <Field
              component={ImagesUpload}
              name="color_samples"
              label="Colors"
              disabled={isAddItemLoading}
              allowCrop
            />
            <PrimaryButton loading={isAddItemLoading} type="submit">
              Submit
            </PrimaryButton>
          </Form>
        )}
      </Formik>
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  isAddItemLoading: getAddItemLoadingSelector(state),
});

const actions: any = {
  addItemRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(withRouter(StoreItemAdd));
