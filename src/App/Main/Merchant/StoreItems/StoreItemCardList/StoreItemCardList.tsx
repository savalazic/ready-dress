import React from 'react';
import StoreItemCard from '../StoreItemCard';
import { StoreItem } from 'services/merchant/storeItem/storeItemReducer';
import map from 'lodash/map';

interface Props {
  items: {
    [id: number]: StoreItem;
  };
}
const StoreItemCardList = ({ items }: Props) => (
  <React.Fragment>
    {map(items, (item: StoreItem) => (
      <StoreItemCard
        key={String(item.id)}
        id={item.id}
        title={item.title}
        price={item.price_in_centum}
        img={item.images && item.images[0] && item.images[0].url}
      />
    ))}
  </React.Fragment>
);

export default StoreItemCardList;
