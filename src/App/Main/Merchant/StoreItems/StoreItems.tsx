import React from 'react';
import { Route, RouteComponentProps } from 'react-router-dom';

import StoreItemList from './StoreItemList';
import StoreItemAdd from './StoreItemAdd';
import StoreItemEdit from './StoreItemEdit';

const StoreItems = ({ match }: RouteComponentProps) => {
  return (
    <div className="StoreItems">
      <Route exact path={`${match.path}/`} component={StoreItemList} />
      <Route path={`${match.path}/add`} component={StoreItemAdd} />
      <Route path={`${match.path}/edit/:id`} component={StoreItemEdit} />
    </div>
  );
};

export default StoreItems;
