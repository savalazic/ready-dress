import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter, RouteComponentProps } from 'react-router-dom';
import { Grid, Card } from 'semantic-ui-react';

import { State } from 'services/state';
import { removeItemRequest } from 'services/merchant/storeItem/storeItemActions';
import { getRemoveItemLoadingSelector } from 'services/merchant/storeItem/storeItemSelectors';
import placeHolderImage from 'assets/images/No_Image_Available.jpg';
import './StoreAdminCard.scss';

import { PrimaryButton } from 'components/Button';
import BaseModal from 'components/BaseModal';
import ConfirmModal from 'components/ConfirmModal';

interface MatchParams {
  storeBranchId: string;
}
interface Props extends RouteComponentProps<MatchParams> {
  title: string;
  price: number;
  img: string;
  id: number;
  removeItemRequest: (id: number, branchId: number) => void;
  isRemoveItemLoading: boolean;
}

const defaultImage = placeHolderImage;

const StoreAdminCard = ({
  title,
  price,
  img = defaultImage,
  id,
  removeItemRequest,
  isRemoveItemLoading,
  match,
}: Props) => {
  return (
    <Grid.Column mobile={16} tablet={8} computer={4}>
      <Card className="Card">
        <div className="Card__img" style={{ backgroundImage: `url(${img})` }} />
        <Card.Content>
          <Card.Header>{title}</Card.Header>
          <Card.Meta>
            <span className="Card__price">{price}$</span>
          </Card.Meta>
        </Card.Content>
        <Card.Content extra>
          <Link to={`${match.url}/edit/${id}`}>
            <PrimaryButton>Edit</PrimaryButton>
          </Link>
          <BaseModal>
            {({ isModalOpen, openModal, closeModal }) => (
              <React.Fragment>
                <PrimaryButton onClick={openModal}>Delete</PrimaryButton>
                <ConfirmModal
                  isLoading={isRemoveItemLoading}
                  isModalOpen={isModalOpen}
                  confirmButtonText="Remove"
                  contentText={`Are you sure you want to remove ${title}?`}
                  handleClose={closeModal}
                  handleConfirm={() => {
                    removeItemRequest(id, +match.params.storeBranchId);
                    if (isRemoveItemLoading) {
                      closeModal();
                    }
                  }}
                />
              </React.Fragment>
            )}
          </BaseModal>
        </Card.Content>
      </Card>
    </Grid.Column>
  );
};

const mapStateToProps = (state: State) => ({
  isRemoveItemLoading: getRemoveItemLoadingSelector(state),
});

const actions = {
  removeItemRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(withRouter(StoreAdminCard));
