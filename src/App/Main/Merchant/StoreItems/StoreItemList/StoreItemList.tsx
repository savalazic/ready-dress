import { Grid } from 'semantic-ui-react';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps, match } from 'react-router-dom';

import { PrimaryButton } from 'components/Button';
import { getItemsRequest } from 'services/merchant/storeItem/storeItemActions';
import {
  getItemsSelector,
  getItemsLoadingSelector,
} from 'services/merchant/storeItem/storeItemSelectors';
import { State } from 'services/state';
import { StoreItem } from 'services/merchant/storeItem/storeItemReducer';
import LoadingContainer from 'components/LoadingContainer';

import StoreItemCardList from '../StoreItemCardList';

interface MatchParams {
  storeBranchId: string;
}
interface Props extends RouteComponentProps<MatchParams> {
  match: match<MatchParams>;
  getItemsRequest: (branchId: number) => void;
  items: StoreItem[];
  isItemsLoading: boolean;
}
class StoreItemList extends Component<Props> {
  componentDidMount() {
    const { match } = this.props;
    this.props.getItemsRequest(+match.params.storeBranchId);
  }

  render() {
    const { items, match, isItemsLoading } = this.props;

    return (
      <LoadingContainer isLoading={isItemsLoading}>
        <h2 className="Admin__heading">Your stores branch items</h2>
        <Grid container>
          <Link to={`${match.url}/add`}>
            <PrimaryButton className="Admin__addBtn">Add new</PrimaryButton>
          </Link>
          <Grid.Row>
            <StoreItemCardList items={items} />
          </Grid.Row>
        </Grid>
      </LoadingContainer>
    );
  }
}

const mapStateToProps = (state: State) => {
  return {
    items: getItemsSelector(state),
    isItemsLoading: getItemsLoadingSelector(state),
  };
};
const actions: any = {
  getItemsRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(StoreItemList);
