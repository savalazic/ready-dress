import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';
import { Formik, Field, FormikActions, FormikErrors } from 'formik';
import { object, string, date } from 'yup';
import { connect } from 'react-redux';
import FacebookLogin, { ReactFacebookLoginInfo } from 'react-facebook-login';

import { isEmailValid } from 'utils/validation';
import { SecondaryButton } from 'components/Button';
import Form from 'components/Form';
import Input from 'components/Input';
import Checkbox from 'components/Checkbox';
import PasswordInput from 'components/PasswordInput';
import { FACEBOOK_APP_ID } from 'config';
import { registerRequest, loginFacebook } from 'services/auth/authActions';
import './Register.scss';
import { State } from 'services/state';
import { getRegisterLoadingSelector } from 'services/auth/authSelectors';
import ModalLink from 'components/ModalLink';
import DateInput from 'components/DateInput';

interface Props {
  registerRequest: (
    email: string,
    password: string,
    password_confirmation: string,
    first_name: string,
    last_name: string,
    dob: any,
    phone_no: string,
    zipcode: string,
  ) => {};
  loginFacebook: (arg?: any) => {};
  isRegisterLoading: boolean;
}

interface FormValues {
  email: string;
  password: string;
  password_confirmation: string;
  first_name: string;
  last_name: string;
  dob: any;
  phone_no: string;
  zipcode: string;
  terms?: boolean;
}

const validationSchema = object().shape({
  email: string().required('Required'),
  password: string()
    .min(8, 'Minimum 8 characters')
    .required('Required'),
  password_confirmation: string().required('Required'),
  first_name: string().required('Required'),
  last_name: string().required('Required'),
  dob: date().required('Required'),
  phone_no: string().required('Required'),
  zipcode: string().required('Required'),
});

const validate = (values: FormValues) => {
  const errors: FormikErrors<FormValues> = {};

  if (!isEmailValid(values.email)) {
    errors.email = 'Email not valid';
  }
  if (
    // must be truthy to display required
    values.password &&
    values.password_confirmation &&
    values.password !== values.password_confirmation
  ) {
    errors.password_confirmation = 'Passwords must match';
  }

  if (!values.terms) {
    errors.terms = 'You must agree to proceed';
  }

  return errors;
};

class Register extends Component<Props> {
  handleSubmit = (values: FormValues, actions: FormikActions<FormValues>) => {
    const { registerRequest } = this.props;
    registerRequest(
      values.email,
      values.password,
      values.password_confirmation,
      values.first_name,
      values.last_name,
      values.dob,
      values.phone_no,
      values.zipcode,
    );
  };

  handleFacebookLogin = ({ accessToken }: ReactFacebookLoginInfo) => {
    const { loginFacebook } = this.props;
    if (accessToken) {
      loginFacebook(accessToken);
    }
  };

  render() {
    const { isRegisterLoading } = this.props;

    return (
      <Container>
        <h2 className="Register__heading">
          We can’t wait to have you shop with us.
        </h2>
        <h2 className="Register__heading">Let’s make your account!</h2>

        <div className="Register__heading-buttons">
          <SecondaryButton as={ModalLink} to="/login">
            I already have an account
          </SecondaryButton>

          <FacebookLogin
            appId={FACEBOOK_APP_ID}
            cssClass="Button PrimaryButton ui button"
            fields="name,email"
            callback={this.handleFacebookLogin}
          />
        </div>

        <span className="Register__divider">OR</span>

        <Formik
          initialValues={{
            email: '',
            password: '',
            password_confirmation: '',
            first_name: '',
            last_name: '',
            dob: '',
            phone_no: '',
            zipcode: '',
            terms: false,
          }}
          onSubmit={this.handleSubmit}
          validationSchema={validationSchema}
          validate={validate}
        >
          {() => (
            <Form className="Register__form">
              <Field
                component={Input}
                name="first_name"
                label="First name"
                disabled={isRegisterLoading}
              />
              <Field
                component={Input}
                name="last_name"
                label="Last name"
                disabled={isRegisterLoading}
              />
              <Field
                component={Input}
                name="email"
                label="Email"
                disabled={isRegisterLoading}
              />
              <Field
                component={DateInput}
                name="dob"
                id="dob"
                placeholder="Birthdate"
                label="Birthdate"
              />
              <Field
                component={PasswordInput}
                name="password"
                label="Password"
                disabled={isRegisterLoading}
              />
              <Field
                component={PasswordInput}
                name="password_confirmation"
                label="Password conformation"
                disabled={isRegisterLoading}
              />
              <Field
                component={Input}
                name="phone_no"
                label="Phone number"
                disabled={isRegisterLoading}
              />
              <Field
                component={Input}
                name="zipcode"
                label="Zipcode"
                disabled={isRegisterLoading}
              />
              <Field
                component={Checkbox}
                name="terms"
                label="Agree to terms and services"
              />
              <SecondaryButton loading={isRegisterLoading} type="submit">
                Start Shopping!
              </SecondaryButton>
            </Form>
          )}
        </Formik>
      </Container>
    );
  }
}

const mapStateToProps = (state: State) => ({
  isRegisterLoading: getRegisterLoadingSelector(state),
});

const actions: any = {
  registerRequest,
  loginFacebook,
};

export default connect(
  mapStateToProps,
  actions,
)(Register);
