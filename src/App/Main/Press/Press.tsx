import React from 'react';
import { Container } from 'semantic-ui-react';
import Section from 'components/Section';

const Press = () => {
  return (
    <Section>
      <Container>
        <h2>Press</h2>
        <p>Check back soon!</p>
      </Container>
    </Section>
  );
};

export default Press;
