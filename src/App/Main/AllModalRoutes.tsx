import React from 'react';

import Login from './Login';
import Register from './Register';
import ModalRoute from 'components/ModalRoute';
import Appointment from './Appointment';
import ManagerMenuRouter from './ManagerMenu/MenuRouter';
import CustomerMenuRouter from './CustomerMenu/CustomerMenuRouter';
import PasswordResetRequest from './PasswordResetRequest';
import PasswordReset from './PasswordReset';

const AllModalRoutes = () => {
  return (
    <>
      <ModalRoute path="/login" component={Login} />
      <ModalRoute path="/register" component={Register} />
      <ModalRoute
        path="/password_request"
        hasBack
        component={PasswordResetRequest}
      />
      <ModalRoute path="/password_reset" component={PasswordReset} />

      <ManagerMenuRouter />
      <CustomerMenuRouter />

      <Appointment />
    </>
  );
};

export default AllModalRoutes;
