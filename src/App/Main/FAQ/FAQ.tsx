import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'semantic-ui-react';
import Section from 'components/Section';

import './FAQ.scss';

const FAQ = () => {
  return (
    <Section className="FAQ">
      <Container>
        <h2>FAQ</h2>
        <div>
          <h4>
            Do I need to register an account before shopping through ReadyDress?
            Yes. Our registration process is fast, free, and will save you time
            for future fittings.
          </h4>
          <p>
            Yes. Our registration process is fast, free, and will save you time
            for future fittings.
          </p>
        </div>

        <div>
          <h4>How do I register?</h4>
          <p>
            Look in the top right corner of the home page – click the hanger
            that says Register!
          </p>
        </div>

        <div>
          <h4>Can I modify or cancel my fitting?</h4>
          <p>
            Yes, just be sure to do so at least an hour before your original
            fitting appointment.
          </p>
        </div>

        <div>
          <h4>What if I can’t make it to my fitting?</h4>
          <p>
            No problem! If you aren’t able to make it, or if you don’t cancel
            your fitting within an hour of appointment time, then your card on
            file will get charged a small $3 service fee.
          </p>
        </div>

        <div>
          <h4>Do I need a paid membership to shop through ReadyDress?</h4>
          <p>Nope! You can still shop through us without a paid membership.</p>
        </div>

        <div>
          <h4>What are the benefits of a paid membership?</h4>
          <p>
            You will get exclusive deals, get first dibs on new merchandise and
            be put in a monthly drawing to win cash paired with a donation to a
            charity of your choice, in your name!
          </p>
        </div>

        <div>
          <h4>What kind of payment methods do you accept?</h4>
          <p>We accept the following payment methods:</p>
          <ul>
            <li>Mastercard</li>
            <li>Visa</li>
            <li>Discover</li>
            <li>Amex</li>
          </ul>
        </div>

        <div>
          <h4>Is my privacy and personal information secure on your site?</h4>
          <p>
            Please check out our Privacy Policy to read full details on how we
            protect your private and personal information.
          </p>
        </div>

        <div>
          <h4>What’s your return policy?</h4>
          <p>
            Every store has a different return policy – we keep updated policies{' '}
            <Link to="/return-policy">here</Link>.
          </p>
        </div>

        <div>
          <h4>I can’t find an answer to my question!</h4>
          <p>
            Sorry about that, please send an e-mail to{' '}
            <a href="mailto:hayley@ready-dress.com" target="_blank">
              hayley@ready-dress.com
            </a>{' '}
            to get your question answered!
          </p>
        </div>
      </Container>
    </Section>
  );
};

export default FAQ;
