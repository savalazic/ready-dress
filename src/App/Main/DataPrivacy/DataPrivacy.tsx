import React from 'react';
import { Container } from 'semantic-ui-react';
import Section from 'components/Section';

import './DataPrivacy.scss';

const DataPrivacy = () => {
  return (
    <Section className="DataPrivacy">
      <Container>
        <h2>Data Privacy</h2>
        <p>
          This privacy policy sets out how READYDRESS uses and protects any
          information that you give READYDRESS when you use this website. READY
          is committed to ensuring that your privacy is protected. Should we ask
          you to provide certain information by which you can be identified when
          using this website, then you can be assured that it will only be used
          in accordance with this privacy statement. READYDRESS may change this
          policy from time to time by updating this page. You should check this
          page from time to time to ensure that you are happy with any changes.
        </p>

        <div>
          <h3>WHAT WE COLLECT</h3>
          <p>WE MAY COLLECT THE FOLLOWING INFORMATION:</p>
          <ul>
            <li>name</li>
            <li>
              contact information including email address and phone number
            </li>
            <li>
              demographic information such as postcode, preferences and
              interests
            </li>
            <li>
              other information relevant to customer surveys and/or offers
            </li>
          </ul>
        </div>

        <div>
          <h3>SECURITY</h3>
          <p>
            WE ARE COMMITTED TO ENSURING THAT YOUR INFORMATION IS SECURE. IN
            ORDER TO PREVENT UNAUTHORIZED ACCESS OR DISCLOSURE, WE HAVE PUT IN
            PLACE SUITABLE PHYSICAL, ELECTRONIC AND MANAGERIAL PROCEDURES TO
            SAFEGUARD AND SECURE THE INFORMATION WE COLLECT ONLINE.
          </p>
        </div>

        <div>
          <h3>HOW WE USE COOKIES</h3>
          <p>
            A COOKIE IS A SMALL FILE WHICH ASKS PERMISSION TO BE PLACED ON YOUR
            COMPUTER'S HARD DRIVE. ONCE YOU AGREE, THE FILE IS ADDED AND THE
            COOKIE HELPS ANALYSE WEB TRAFFIC OR LETS YOU KNOW WHEN YOU VISIT A
            PARTICULAR SITE. COOKIES ALLOW WEB APPLICATIONS TO RESPOND TO YOU AS
            AN INDIVIDUAL. THE WEB APPLICATION CAN TAILOR ITS OPERATIONS TO YOUR
            NEEDS, LIKES AND DISLIKES BY GATHERING AND REMEMBERING INFORMATION
            ABOUT YOUR PREFERENCES. WE USE TRAFFIC LOG COOKIES TO IDENTIFY WHICH
            PAGES ARE BEING USED. THIS HELPS US ANALYSE DATA ABOUT WEB PAGE
            TRAFFIC AND IMPROVE OUR WEBSITE IN ORDER TO TAILOR IT TO CUSTOMER
            NEEDS. WE ONLY USE THIS INFORMATION FOR STATISTICAL ANALYSIS
            PURPOSES AND THEN THE DATA IS REMOVED FROM THE SYSTEM. OVERALL,
            COOKIES HELP US PROVIDE YOU WITH A BETTER WEBSITE, BY ENABLING US TO
            MONITOR WHICH PAGES YOU FIND USEFUL AND WHICH YOU DO NOT. A COOKIE
            IN NO WAY GIVES US ACCESS TO YOUR COMPUTER OR ANY INFORMATION ABOUT
            YOU, OTHER THAN THE DATA YOU CHOOSE TO SHARE WITH US. YOU CAN CHOOSE
            TO ACCEPT OR DECLINE COOKIES. MOST WEB BROWSERS AUTOMATICALLY ACCEPT
            COOKIES, BUT YOU CAN USUALLY MODIFY YOUR BROWSER SETTING TO DECLINE
            COOKIES IF YOU PREFER. THIS MAY PREVENT YOU FROM TAKING FULL
            ADVANTAGE OF THE WEBSITE.
          </p>
        </div>

        <div>
          <h3>LINKS TO OTHER WEBSITES</h3>
          <p>
            OUR WEBSITE MAY CONTAIN LINKS TO OTHER WEBSITES OF INTEREST.
            HOWEVER, ONCE YOU HAVE USED THESE LINKS TO LEAVE OUR SITE, YOU
            SHOULD NOTE THAT WE DO NOT HAVE ANY CONTROL OVER THAT OTHER WEBSITE.
            THEREFORE, WE CANNOT BE RESPONSIBLE FOR THE PROTECTION AND PRIVACY
            OF ANY INFORMATION WHICH YOU PROVIDE WHILST VISITING SUCH SITES AND
            SUCH SITES ARE NOT GOVERNED BY THIS PRIVACY STATEMENT. YOU SHOULD
            EXERCISE CAUTION AND LOOK AT THE PRIVACY STATEMENT APPLICABLE TO THE
            WEBSITE IN QUESTION.
          </p>
        </div>

        <div>
          <h3>CONTROLLING YOUR PERSONAL INFORMATION</h3>
          <p>
            YOU MAY CHOOSE TO RESTRICT THE COLLECTION OR USE OF YOUR PERSONAL
            INFORMATION IN THE FOLLOWING WAYS:
          </p>
          <ul>
            <li>
              whenever you are asked to fill in a form on the website, look for
              the box that you can click to indicate that you do not want the
              information to be used by anybody for direct marketing purposes
            </li>
            <li>
              if you have previously agreed to us using your personal
              information for direct marketing purposes, you may change your
              mind at any time by writing to or emailing us at
              hayley@ready-dress.com
            </li>
          </ul>
        </div>

        <p>
          WE WILL NOT SELL, DISTRIBUTE OR LEASE YOUR PERSONAL INFORMATION TO
          THIRD PARTIES UNLESS WE HAVE YOUR PERMISSION OR ARE REQUIRED BY LAW TO
          DO SO. WE MAY USE YOUR PERSONAL INFORMATION TO SEND YOU PROMOTIONAL
          INFORMATION ABOUT THIRD PARTIES WHICH WE THINK YOU MAY FIND
          INTERESTING IF YOU TELL US THAT YOU WISH THIS TO HAPPEN.
        </p>

        <p>
          YOU MAY REQUEST DETAILS OF PERSONAL INFORMATION WHICH WE HOLD ABOUT
          YOU UNDER THE DATA PROTECTION ACT 1998. A SMALL FEE WILL BE PAYABLE.
          IF YOU WOULD LIKE A COPY OF THE INFORMATION HELD ON YOU PLEASE WRITE
          TO .
        </p>

        <p>
          IF YOU BELIEVE THAT ANY INFORMATION WE ARE HOLDING ON YOU IS INCORRECT
          OR INCOMPLETE, PLEASE WRITE TO OR EMAIL US AS SOON AS POSSIBLE, AT THE
          ABOVE ADDRESS. WE WILL PROMPTLY CORRECT ANY INFORMATION FOUND TO BE
          INCORRECT.
        </p>
      </Container>
    </Section>
  );
};

export default DataPrivacy;
