import React, { Component } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container } from 'semantic-ui-react';

import { getStoreBranchesRequest } from 'services/admin/storeBranch/storeBranchActions';
import LoadingContainer from 'components/LoadingContainer';
import { State } from 'services/state';
import {
  getStoreBranchesLoadingSelector,
  getStoreBranchesSelector,
} from 'services/admin/storeBranch/storeBranchSelectors';
import StoreBranchCards from '../StoreBranchCards';
import { PrimaryButton } from 'components/Button';
import { StoreBranch } from 'services/merchant/storeBranch/storeBranchReducer';

interface MatchParams {
  storeId: string;
}
interface Props extends RouteComponentProps<MatchParams> {
  getStoreBranchesRequest: (id: number) => void;
  isStoreBranchesLoading: boolean;
  storeBranches: StoreBranch[];
}

class StoreBranchList extends Component<Props> {
  componentDidMount() {
    this.props.getStoreBranchesRequest(+this.props.match.params.storeId);
  }

  render() {
    const { isStoreBranchesLoading, storeBranches, match } = this.props;

    return (
      <div className="StoreBranches">
        <h2 className="Admin__heading">Your store branches</h2>
        <LoadingContainer isLoading={isStoreBranchesLoading}>
          <Container>
            <Link to={`${match.url}/add`}>
              <PrimaryButton className="Admin__addBtn">Add new</PrimaryButton>
            </Link>
            <StoreBranchCards storeBranches={storeBranches} />
          </Container>
        </LoadingContainer>
      </div>
    );
  }
}

const mapStateToProps = (state: State) => ({
  isStoreBranchesLoading: getStoreBranchesLoadingSelector(state),
  storeBranches: getStoreBranchesSelector(state),
});

const actions: any = {
  getStoreBranchesRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(StoreBranchList);
