import map from 'lodash/map';
import React from 'react';
import StoreBranchCard from '../StoreBranchCard';

import './StoreBranchCards.scss';

import { StoreBranch } from 'services/merchant/storeBranch/storeBranchReducer';

interface IProps {
  storeBranches: StoreBranch[];
}

type Props = IProps;

const StoreBranchCards = ({ storeBranches }: Props) => {
  return (
    <div className="StoreBranchCards">
      {map(storeBranches, (storeBranch: StoreBranch) => (
        <StoreBranchCard
          key={storeBranch.id}
          id={storeBranch.id}
          city={storeBranch.city}
          phone_number={storeBranch.phone_number}
          state={storeBranch.state}
          street_name={storeBranch.street_name}
          street_number={storeBranch.street_number}
        />
      ))}
    </div>
  );
};

export default StoreBranchCards;
