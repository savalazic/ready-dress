import { Day } from './WorkingHours';
import { flatten, unflatten } from 'utils/object';

export const flattenWorkingHours = (days: Day[]) => {
  return days.reduce((acc: any, val, key) => {
    acc[key] = flatten(val);
    return acc;
  }, []);
};

export const unflattenWorkingHours = (days: Day[]) => {
  return days.reduce((acc: any, val, key) => {
    acc[key] = unflatten(val);
    return acc;
  }, []);
};
