import React, { useState, useEffect } from 'react';
import { FieldProps } from 'formik';
import { Checkbox } from 'semantic-ui-react';
import { TimeInput } from 'semantic-ui-calendar-react';

import './WorkingHours.scss';
import { flattenWorkingHours } from './helpers';

export interface Day {
  start: string;
  finish: string;
}

const days: string[] = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];

const daysShort: string[] = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

interface Props extends FieldProps {
  title: string;
}

const WorkingHours = ({
  title,
  field: { name, value },
  form: { setFieldValue },
}: Props) => {
  const [hours, setHours]: any = useState(flattenWorkingHours(value));

  useEffect(() => {
    setHours(flattenWorkingHours(value));
  }, [value]);

  const handleCheckboxChange = (index: number) => (
    e: any,
    { checked }: any,
  ) => {
    setFieldValue(
      name,
      value.map((v: Day, i: number) => {
        if (index === i) {
          return checked
            ? { [daysShort[index]]: { start: '10:00', finish: '22:00' } }
            : { [daysShort[index]]: null };
        }
        return v;
      }),
    );
  };

  const handleTimeStartChange = (index: number) => (
    e: any,
    { value: hour }: any,
  ) => {
    setFieldValue(
      name,
      value.map((v: Day, i: number) => {
        if (index === i) {
          return {
            [daysShort[index]]: {
              // @ts-ignore
              ...v[daysShort[index]],
              start: hour,
            },
          };
        }
        return v;
      }),
    );
  };

  const handleTimeEndChange = (index: number) => (
    e: any,
    { value: hour }: any,
  ) => {
    setFieldValue(
      name,
      value.map((v: Day, i: number) => {
        if (index === i) {
          return {
            [daysShort[index]]: {
              // @ts-ignore
              ...v[daysShort[index]],
              finish: hour,
            },
          };
        }
        return v;
      }),
    );
  };

  return (
    <div className="WorkingHours">
      <h4>{title}</h4>

      <div className="WorkingHours__list">
        {hours.map((day: Day, i: number) => (
          <div key={i} className="WorkingHours__item">
            <Checkbox
              toggle
              defaultChecked={!!(day.start && day.finish)}
              label={days[i]}
              onChange={handleCheckboxChange(i)}
            />
            {day.start && day.finish ? (
              <div className="WorkingHours__time">
                <TimeInput
                  value={day.start}
                  timeFormat="24"
                  onChange={handleTimeStartChange(i)}
                />
                {' - '}
                <TimeInput
                  value={day.finish}
                  timeFormat="24"
                  onChange={handleTimeEndChange(i)}
                />
              </div>
            ) : (
              <span>Closed</span>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default WorkingHours;
