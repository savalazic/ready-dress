import React from 'react';
import { Container } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router';
import { Formik, Field, FormikErrors, FormikValues } from 'formik';
import { object, string } from 'yup';

import Form from 'components/Form';
import Input from 'components/Input';
import { PrimaryButton } from 'components/Button';
import { State } from 'services/state';
import { getAddStoreBranchLoadingSelector } from 'services/admin/storeBranch/storeBranchSelectors';
import { addStoreBranchRequest } from 'services/admin/storeBranch/storeBranchActions';
import PlacesAutocomplete from 'components/PlacesAutocomplete';
import WorkingHours from '../WorkingHours/WorkingHours';

interface FormValues {
  street_name: string;
  street_number: string;
  city: string;
  state: string;
  lon: string;
  lat: string;
  phone_number: string;
}

const validationSchema = object().shape({
  phone_number: string().required('Required'),
});

const validate = (values: any) => {
  const errors: FormikErrors<FormikValues> = {};
  if (!values.location) {
    errors.location = 'Required';
  }
  return errors;
};

interface MatchParams {
  storeId: string;
}
interface Props extends RouteComponentProps<MatchParams> {
  addStoreBranchRequest: (storeBranch: FormValues, storeId: number) => void;
  isAddStoreBranchLoading: boolean;
}

const StoreBranchAdd = ({
  addStoreBranchRequest,
  isAddStoreBranchLoading,
  match,
}: Props) => {
  const handleSubmit = (values: any) => {
    const storeBranch = {
      street_name: values.location.street_name,
      street_number: values.location.street_number,
      city: values.location.city,
      state: values.location.state,
      lon: values.location.lon,
      lat: values.location.lat,
      phone_number: values.phone_number,
      working_hours: values.working_hours,
    };

    addStoreBranchRequest(storeBranch, +match.params.storeId);
  };

  return (
    <Container>
      <h2>Add store branch</h2>
      <Formik
        initialValues={{
          location: '',
          phone_number: '',
          working_hours: [
            { mon: null },
            { tue: null },
            { wed: null },
            { thu: null },
            { fri: null },
            { sat: null },
            { sun: null },
          ],
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        validate={validate}
      >
        {({ isValid }) => (
          <Form>
            <Field
              component={PlacesAutocomplete}
              name="location"
              id="location"
              placeholder="Search Address..."
              label="Add Location"
            />
            <Field
              component={Input}
              name="phone_number"
              id="phone_number"
              placeholder="Phone Number"
              label="Phone Number"
            />

            <Field
              component={WorkingHours}
              name="working_hours"
              id="working_hours"
              title="Working Hours"
            />

            <PrimaryButton
              loading={isAddStoreBranchLoading}
              disabled={!isValid}
              type="submit"
            >
              Submit
            </PrimaryButton>
          </Form>
        )}
      </Formik>
    </Container>
  );
};

const actions: any = {
  addStoreBranchRequest,
};

const mapStateToProps = (state: State) => ({
  isAddStoreBranchLoading: getAddStoreBranchLoadingSelector(state),
});

export default connect(
  mapStateToProps,
  actions,
)(withRouter(StoreBranchAdd));
