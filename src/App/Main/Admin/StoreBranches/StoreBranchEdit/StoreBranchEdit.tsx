import has from 'lodash/has';
import isString from 'lodash/isString';
import React, { useEffect } from 'react';
import { Container } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router';
import { Formik, Field, FormikErrors, FormikValues } from 'formik';
import { object, string } from 'yup';

import Form from 'components/Form';
import Input from 'components/Input';
import { PrimaryButton } from 'components/Button';
import { State } from 'services/state';
import {
  getEditStoreBranchLoadingSelector,
  getStoreBranchesSelector,
  getStoreBranchLoadingSelector,
} from 'services/admin/storeBranch/storeBranchSelectors';
import {
  editStoreBranchRequest,
  getStoreBranchRequest,
} from 'services/admin/storeBranch/storeBranchActions';
import LoadingContainer from 'components/LoadingContainer';
import { StoreBranch } from 'services/merchant/storeBranch/storeBranchReducer';
import PlacesAutocompleteInput from 'components/PlacesAutocomplete';
import { difference } from 'utils/object';
import WorkingHours from '../WorkingHours';

const validationSchema = object().shape({
  phone_number: string().required('Required'),
});

const validate = (values: any) => {
  const errors: FormikErrors<FormikValues> = {};
  if (!values.location) {
    errors.location = 'Required';
  }
  return errors;
};

interface MatchParams {
  storeId: string;
  storeBranchId: string;
}
interface Props extends RouteComponentProps<MatchParams> {
  editStoreBranchRequest: (
    storeBranch: StoreBranch,
    storeId: number,
    storeBranchId: number,
  ) => void;
  isEditStoreBranchLoading: boolean;
  storeBranches: {
    [id: number]: StoreBranch;
  };
  getStoreBranchRequest: (storeId: number, storeBranchId: number) => void;
  isStoreBranchLoading: boolean;
}

const StoreBranchEdit = ({
  editStoreBranchRequest,
  isEditStoreBranchLoading,
  storeBranches,
  match,
  getStoreBranchRequest,
  isStoreBranchLoading,
}: Props) => {
  useEffect(() => {
    if (!has(storeBranches, +match.params.storeBranchId)) {
      getStoreBranchRequest(+match.params.storeId, +match.params.storeBranchId);
    }
  }, [storeBranches]);

  const handleSubmit = (initialValues: any) => (values: any) => {
    const currentStoreBranchValues =
      storeBranches && storeBranches[+match.params.storeBranchId];

    const changedValues = difference(values, initialValues);

    const storeBranch = {
      ...currentStoreBranchValues,
      ...changedValues,
      ...(!isString(changedValues.location) && changedValues.location),
      working_hours: values.working_hours,
    };

    editStoreBranchRequest(
      storeBranch,
      +match.params.storeId,
      +match.params.storeBranchId,
    );
  };

  const currentStoreBranch =
    storeBranches && storeBranches[+match.params.storeBranchId];

  if (!currentStoreBranch) {
    return <h2>Store branch doesn't exist</h2>;
  }

  return (
    <Container>
      <h2>Edit store branch</h2>
      <LoadingContainer isLoading={isStoreBranchLoading}>
        <Formik
          initialValues={{
            location: `${currentStoreBranch.street_name ||
              ''} ${currentStoreBranch.street_number ||
              ''} ${currentStoreBranch.city || ''} ${currentStoreBranch.state ||
              ''}`,
            phone_number: currentStoreBranch.phone_number,
            working_hours: currentStoreBranch.working_hours,
          }}
          onSubmit={handleSubmit(currentStoreBranch)}
          validationSchema={validationSchema}
          validate={validate}
        >
          {({ isValid }) => (
            <Form>
              <Field
                component={PlacesAutocompleteInput}
                name="location"
                id="location"
                placeholder="Search Address..."
                label="Add Location"
              />
              <Field
                component={Input}
                name="phone_number"
                id="phone_number"
                placeholder="Phone Number"
                label="Phone Number"
              />

              <Field
                component={WorkingHours}
                name="working_hours"
                id="working_hours"
                title="Working Hours"
              />

              <PrimaryButton
                loading={isEditStoreBranchLoading}
                disabled={!isValid}
                type="submit"
              >
                Submit
              </PrimaryButton>
            </Form>
          )}
        </Formik>
      </LoadingContainer>
    </Container>
  );
};

const actions: any = {
  editStoreBranchRequest,
  getStoreBranchRequest,
};

const mapStateToProps = (state: State) => ({
  isEditStoreBranchLoading: getEditStoreBranchLoadingSelector(state),
  isStoreBranchLoading: getStoreBranchLoadingSelector(state),
  storeBranches: getStoreBranchesSelector(state),
});

export default connect(
  mapStateToProps,
  actions,
)(withRouter(StoreBranchEdit));
