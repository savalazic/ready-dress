import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { Card } from 'semantic-ui-react';

import { removeStoreBranchRequest } from 'services/admin/storeBranch/storeBranchActions';

import { PrimaryButton } from 'components/Button';
import BaseModal from 'components/BaseModal';
import ConfirmModal from 'components/ConfirmModal';

interface MatchParams {
  storeId: string;
}
interface IProps extends RouteComponentProps<MatchParams> {
  id: number;
  city: string;
  phone_number: string;
  state: string;
  street_name: string;
  street_number: string;
}

type DispatchProps = typeof actions;
type Props = DispatchProps & IProps;

const StoreBranchCard = ({
  match,
  id,
  city,
  phone_number,
  state,
  street_name,
  street_number,
  removeStoreBranchRequest,
}: Props) => {
  return (
    <Card link className="StoreBranchCard">
      <Card.Content as={Link} to={`${match.url}/${id}/store-managers`}>
        <Card.Header>
          {city}, {id}
        </Card.Header>
        <Card.Meta>
          {street_name} {street_number}, {state}
        </Card.Meta>
        <Card.Description>{phone_number}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Link to={`${match.url}/edit/${id}`}>
          <PrimaryButton>Edit</PrimaryButton>
        </Link>
        <BaseModal>
          {({ isModalOpen, openModal, closeModal }) => (
            <React.Fragment>
              <ConfirmModal
                isModalOpen={isModalOpen}
                confirmButtonText="Remove"
                contentText={`Are you sure you want to remove ${city}, ${street_name}, ${street_number} branch?`}
                handleClose={closeModal}
                handleConfirm={() => {
                  removeStoreBranchRequest(+match.params.storeId, id);
                  closeModal();
                }}
              />
              <PrimaryButton onClick={openModal}>Delete</PrimaryButton>
            </React.Fragment>
          )}
        </BaseModal>
      </Card.Content>
    </Card>
  );
};

const actions = {
  removeStoreBranchRequest,
};

export default connect(
  null,
  actions,
)(withRouter(StoreBranchCard));
