import React from 'react';
import { Route, RouteComponentProps } from 'react-router-dom';

import StoreBranchList from './StoreBranchList';
import StoreBranchEdit from './StoreBranchEdit';
import StoreBranchAdd from './StoreBranchAdd';

const StoreBranches = ({ match }: RouteComponentProps) => (
  <div className="StoreBranches">
    <Route exact path={`${match.path}/`} component={StoreBranchList} />
    <Route exact path={`${match.path}/add`} component={StoreBranchAdd} />
    <Route
      exact
      path={`${match.path}/edit/:storeBranchId`}
      component={StoreBranchEdit}
    />
  </div>
);

export default StoreBranches;
