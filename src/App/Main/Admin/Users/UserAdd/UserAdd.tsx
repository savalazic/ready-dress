import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import './UserAdd.scss';

import UserForm from '../UserForm';
import { UserFormValues } from 'services/admin/user/userReducer';
import { State } from 'services/state';
import { getAddUserLoadingSelector } from 'services/admin/user/userSelectors';
import { addUserRequest } from 'services/admin/user/userActions';

interface Props {
  isAddUserLoading: boolean;
  addUserRequest: (user: UserFormValues) => void;
}

const UserAdd = ({ isAddUserLoading, addUserRequest }: Props) => {
  const handleSubmit = (values: UserFormValues) => {
    addUserRequest(values);
  };

  return (
    <div className="UserAdd">
      <h2>Add new user</h2>
      <UserForm
        isLoading={isAddUserLoading}
        onSubmit={handleSubmit}
        initialValues={{
          dob: moment(new Date(), 'DD-MM-YYYY'),
        }}
      />
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  isAddUserLoading: getAddUserLoadingSelector(state),
});

const actions: any = {
  addUserRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(UserAdd);
