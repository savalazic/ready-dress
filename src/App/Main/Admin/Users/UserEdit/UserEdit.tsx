import has from 'lodash/has';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import './UserEdit.scss';

import UserForm from '../UserForm';
import { UserFormValues, User } from 'services/admin/user/userReducer';
import { State } from 'services/state';
import {
  getEditUserLoadingSelector,
  getUserLoadingSelector,
  getUsersSelector,
} from 'services/admin/user/userSelectors';
import {
  editUserRequest,
  getUserRequest,
} from 'services/admin/user/userActions';

interface MatchParams {
  id: string;
}
interface Props extends RouteComponentProps<MatchParams> {
  isEditUserLoading: boolean;
  isUserLoading: boolean;
  editUserRequest: (id: number, user: UserFormValues) => void;
  getUserRequest: (id: number) => void;
  users: {
    [id: number]: User;
  } | null;
}

const UserEdit = ({
  match,
  isEditUserLoading,
  isUserLoading,
  editUserRequest,
  getUserRequest,
  users,
}: Props) => {
  useEffect(() => {
    if (!has(users, match.params.id)) {
      getUserRequest(+match.params.id);
    }
  }, []);

  const handleSubmit = (values: UserFormValues) => {
    editUserRequest(+match.params.id, values);
  };

  const currentUser = users && users[+match.params.id];

  if (!currentUser) {
    return <h2>User doesn't exist</h2>;
  }

  return (
    <div className="UserEdit">
      <h2>Edit user</h2>
      <UserForm
        isLoading={isEditUserLoading || isUserLoading}
        onSubmit={handleSubmit}
        initialValues={currentUser}
        isEdit
      />
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  isEditUserLoading: getEditUserLoadingSelector(state),
  isUserLoading: getUserLoadingSelector(state),
  users: getUsersSelector(state),
});

const actions: any = {
  editUserRequest,
  getUserRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(withRouter(UserEdit));
