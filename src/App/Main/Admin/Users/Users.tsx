import React from 'react';
import { Route, RouteComponentProps } from 'react-router-dom';
import { Container } from 'semantic-ui-react';

import './Users.scss';

import UsersList from './UsersList';
import UserAdd from './UserAdd';
import UserEdit from './UserEdit';

const Users = ({ match }: RouteComponentProps) => {
  return (
    <div className="Users">
      <Container>
        <Route exact path={`${match.path}`} component={UsersList} />
        <Route path={`${match.path}/add`} component={UserAdd} />
        <Route path={`${match.path}/edit/:id`} component={UserEdit} />
      </Container>
    </div>
  );
};

export default Users;
