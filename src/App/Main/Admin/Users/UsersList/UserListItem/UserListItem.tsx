import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter, RouteComponentProps } from 'react-router-dom';
import { List } from 'semantic-ui-react';

import './UserListItem.scss';

import Button, { PrimaryButton } from 'components/Button';
import BaseModal from 'components/BaseModal';
import ConfirmModal from 'components/ConfirmModal';
import { State } from 'services/state';
import { getRemoveUserLoading } from 'services/admin/user/userSelectors';
import { removeUserRequest } from 'services/admin/user/userActions';

interface IProps extends RouteComponentProps {
  name: string;
  id: number;
}

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;

type Props = DispatchProps & StateProps & IProps;

const UserListItem = ({
  match,
  name,
  id,
  removeUserRequest,
  isRemoveUserLoading,
}: Props) => (
  <List.Item className="UserListItem">
    <List.Content className="UserListItem__content">
      <List.Icon name="user" />
      <span>{name}</span>
    </List.Content>
    <List.Content>
      <Button as={Link} to={`${match.url}/edit/${id}`}>
        Edit
      </Button>
      <BaseModal>
        {({ isModalOpen, openModal, closeModal }) => (
          <React.Fragment>
            <PrimaryButton onClick={openModal}>Delete</PrimaryButton>
            <ConfirmModal
              isLoading={isRemoveUserLoading}
              isModalOpen={isModalOpen}
              handleClose={closeModal}
              handleConfirm={() => removeUserRequest(id)}
              contentText="Are you sure"
              confirmButtonText="Confirm"
            />
          </React.Fragment>
        )}
      </BaseModal>
    </List.Content>
  </List.Item>
);

const mapStateToProps = (state: State) => ({
  isRemoveUserLoading: getRemoveUserLoading(state),
});

const actions = {
  removeUserRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(withRouter(UserListItem));
