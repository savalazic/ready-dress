import map from 'lodash/map';
import size from 'lodash/size';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { List } from 'semantic-ui-react';

import UserListItem from './UserListItem';
import { State } from 'services/state';
import {
  getUsersSelector,
  getUsersLoadingSelector,
} from 'services/admin/user/userSelectors';
import { User } from 'services/admin/user/userReducer';
import { getUsersRequest } from 'services/admin/user/userActions';
import LoadingContainer from 'components/LoadingContainer';
import { PrimaryButton } from 'components/Button';

interface Props extends RouteComponentProps {
  users: {
    [id: number]: User;
  } | null;
  getUsersRequest: () => void;
  isUsersLoading: boolean;
}

const UsersList = ({
  users,
  getUsersRequest,
  isUsersLoading,
  match,
}: Props) => {
  useEffect(() => {
    getUsersRequest();
  }, [getUsersRequest]);

  return (
    <div className="UsersList">
      <h2 className="Admin__heading">Users</h2>
      <PrimaryButton as={Link} to={`${match.path}/add`}>
        Add new user
      </PrimaryButton>
      <LoadingContainer isLoading={isUsersLoading}>
        <List divided>
          {map(users, user => (
            <UserListItem
              key={String(user.id)}
              name={user.email}
              id={user.id}
            />
          ))}
          {size(users) === 0 && <h4>No users</h4>}
        </List>
      </LoadingContainer>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  users: getUsersSelector(state),
  isUsersLoading: getUsersLoadingSelector(state),
});

const actions: any = {
  getUsersRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(UsersList);
