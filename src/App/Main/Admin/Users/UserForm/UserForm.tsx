import React from 'react';
import { Formik, Field, FormikErrors, FormikValues } from 'formik';
import { object, string, date } from 'yup';

import './UserForm.scss';

import LoadingContainer from 'components/LoadingContainer';
import Form from 'components/Form';
import Input from 'components/Input';
import PasswordInput from 'components/PasswordInput';
import DateInput from 'components/DateInput';
import DropdownInput from 'components/DropdownInput';
import { PrimaryButton } from 'components/Button';

import { isEmailValid } from 'utils/validation';
import { UserFormValues } from 'services/admin/user/userReducer';
import { UserRoles } from 'services/auth/authReducer';

interface Props {
  isLoading: boolean;
  initialValues?: any;
  onSubmit: any;
  isEdit?: boolean;
}

const validationSchema = object().shape({
  email: string().required('Required'),
  password: string()
    .min(8, 'Minimum 8 characters')
    .required('Required'),
  password_confirmation: string().required('Required'),
  role: string().required('Required'),
  first_name: string().required('Required'),
  last_name: string().required('Required'),
  dob: date().required('Required'),
  phone_no: string().required('Required'),
  zipcode: string().required('Required'),
});

const editValidationSchema = object().shape({
  role: string().required('Required'),
  first_name: string().required('Required'),
  last_name: string().required('Required'),
  dob: date().required('Required'),
  phone_no: string().required('Required'),
  zipcode: string().required('Required'),
});

const validate = (values: UserFormValues) => {
  const errors: FormikErrors<FormikValues> = {};

  if (!isEmailValid(values.email)) {
    errors.email = 'Email not valid';
  }
  if (
    values.password &&
    values.password_confirmation &&
    values.password !== values.password_confirmation
  ) {
    errors.password_confirmation = 'Passwords must match';
  }
  return errors;
};

const UserRolesMap = [
  { text: 'Customer', value: UserRoles.customer },
  { text: 'Store Branch Manager', value: UserRoles.storeBranchManager },
  { text: 'Store Manager', value: UserRoles.storeManager },
];

const UserForm = ({
  isLoading,
  initialValues,
  onSubmit,
  isEdit = false,
}: Props) => {
  return (
    <div className="UserForm">
      <LoadingContainer isLoading={isLoading}>
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
          validationSchema={isEdit ? editValidationSchema : validationSchema}
          validate={validate}
        >
          {({ isValid, setFieldValue }) => (
            <Form>
              {!isEdit && (
                <>
                  <Field
                    component={Input}
                    name="email"
                    id="email"
                    placeholder="Email"
                    label="Email"
                  />
                  <Field
                    component={PasswordInput}
                    name="password"
                    id="password"
                    placeholder="Password"
                    label="Password"
                  />
                  <Field
                    component={PasswordInput}
                    name="password_confirmation"
                    id="password_confirmation"
                    placeholder="Password confirmation"
                    label="Password confirmation"
                  />
                </>
              )}
              <Field
                component={DropdownInput}
                name="role"
                id="role"
                placeholder="Role"
                label="Role"
                options={UserRolesMap}
              />
              <Field
                component={Input}
                name="first_name"
                id="first_name"
                placeholder="First Name"
                label="First Name"
              />
              <Field
                component={Input}
                name="last_name"
                id="last_name"
                placeholder="Last Name"
                label="Last Name"
              />
              <Field
                component={DateInput}
                name="dob"
                id="dob"
                placeholder="Birthdate"
                label="Birthdate"
              />
              <Field
                component={Input}
                name="phone_no"
                id="phone_no"
                placeholder="Phone Number"
                label="Phone Number"
              />
              <Field
                component={Input}
                name="zipcode"
                id="zipcode"
                placeholder="ZIP code"
                label="ZIP code"
              />
              <PrimaryButton
                loading={isLoading}
                disabled={!isValid}
                type="submit"
              >
                Submit
              </PrimaryButton>
            </Form>
          )}
        </Formik>
      </LoadingContainer>
    </div>
  );
};

export default UserForm;
