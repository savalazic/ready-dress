import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Container } from 'semantic-ui-react';

import { Store } from 'services/admin/store/storeReducer';
import { getStoresRequest } from 'services/admin/store/storeActions';
import {
  getStoresSelector,
  getStoresLoadingSelector,
} from 'services/admin/store/storeSelectors';
import { State } from 'services/state';

import StoreList from './StoreList';
import LoadingContainer from 'components/LoadingContainer';

interface Props {
  stores: {
    [id: number]: Store;
  } | null;
  isStoresLoading: boolean;
  getStoresRequest: () => void;
}

const Stores = ({ stores, isStoresLoading, getStoresRequest }: Props) => {
  useEffect(() => {
    getStoresRequest();
  }, []);

  return (
    <div className="Stores">
      <Container>
        <h2 className="Admin__heading">Your stores</h2>
        <LoadingContainer isLoading={isStoresLoading}>
          <StoreList stores={stores} />
        </LoadingContainer>
      </Container>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  stores: getStoresSelector(state),
  isStoresLoading: getStoresLoadingSelector(state),
});

const actions: any = {
  getStoresRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(Stores);
