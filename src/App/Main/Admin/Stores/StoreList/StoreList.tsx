import map from 'lodash/map';
import React from 'react';
import StoreCard from '../StoreCard';
import { Store } from 'services/admin/store/storeReducer';

interface Props {
  stores: {
    [id: number]: Store;
  } | null;
}

const StoreList = ({ stores }: Props) => {
  return (
    <div className="StoreList">
      {stores &&
        map(stores, (store: Store) => (
          <StoreCard key={String(store.id)} id={store.id} name={store.name} />
        ))}
    </div>
  );
};

export default StoreList;
