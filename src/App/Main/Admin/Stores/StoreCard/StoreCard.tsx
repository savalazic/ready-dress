import React from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { Card } from 'semantic-ui-react';

interface Props extends RouteComponentProps {
  id: number;
  name: string;
}

const StoreCard = ({ id, name, match }: Props) => (
  <Card
    link
    className="StoreCard"
    as={Link}
    to={`${match.url}/${id}/store-branches`}
  >
    <Card.Content>
      <Card.Header>{name}</Card.Header>
      <Card.Meta>{id}</Card.Meta>
    </Card.Content>
  </Card>
);

export default withRouter(StoreCard);
