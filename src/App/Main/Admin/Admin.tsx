import React from 'react';
import { Route, RouteComponentProps, Link } from 'react-router-dom';
import { Container, Divider, Grid, Segment } from 'semantic-ui-react';
import './Admin.scss';

import { PrimaryButton } from 'components/Button';

import Users from './Users';
import Stores from './Stores';
import StoreBranches from './StoreBranches';
import StoreManagers from './StoreManagers';

const AdminIntro = ({ match }: RouteComponentProps) => (
  <Container>
    <Segment placeholder>
      <Grid columns={2} relaxed="very">
        <Grid.Column>
          <PrimaryButton as={Link} to={`${match.path}/stores`}>
            Manage Stores
          </PrimaryButton>
        </Grid.Column>
        <Grid.Column verticalAlign="middle">
          <PrimaryButton as={Link} to={`${match.path}/users`}>
            Manage Users
          </PrimaryButton>
        </Grid.Column>
      </Grid>
      <Divider vertical>Or</Divider>
    </Segment>
  </Container>
);

const Admin = ({ match }: RouteComponentProps) => {
  return (
    <div className="Admin">
      <Route exact path={`${match.path}`} component={AdminIntro} />
      <Route path={`${match.path}/users`} component={Users} />
      <Route exact path={`${match.path}/stores`} component={Stores} />
      <Route
        path={`${match.path}/stores/:storeId/store-branches`}
        component={StoreBranches}
      />
      <Route
        path={`${
          match.path
        }/stores/:storeId/store-branches/:storeBranchId/store-managers`}
        component={StoreManagers}
      />
    </div>
  );
};

export default Admin;
