import React from 'react';
import { Container } from 'semantic-ui-react';

import StoreManagerList from './StoreManagerList';
import StoreManagerAdd from './StoreManagerAdd';

const StoreManagers = () => {
  return (
    <Container className="StoreManagers">
      <h2>Store managers</h2>
      <StoreManagerAdd />
      <StoreManagerList />
    </Container>
  );
};

export default StoreManagers;
