import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps, Link } from 'react-router-dom';
import { Dropdown } from 'semantic-ui-react';
import { State } from 'services/state';
import {
  getUsersDropdownArraySelector,
  getUsersLoadingSelector,
} from 'services/admin/user/userSelectors';
import { getUsersRequest } from 'services/admin/user/userActions';
import { addStoreManagerRequest } from 'services/admin/storeManager/storeManagerActions';
import { getAddStoreManagerLoading } from 'services/admin/storeManager/storeManagerSelectors';

import Button, { PrimaryButton } from 'components/Button';

import './StoreManagerAdd.scss';

interface MatchParams {
  storeId: string;
  storeBranchId: string;
}
interface IProps extends RouteComponentProps<MatchParams> {}

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;
type Props = DispatchProps & StateProps & IProps;

const StoreManagerAdd = ({
  getUsersRequest,
  isUsersLoading,
  users,
  addStoreManagerRequest,
  isAddManagerLoading,
  match,
}: Props) => {
  const [searchQuery, setSearchQuery] = useState('');
  const [value, setValue] = useState('');

  useEffect(() => {
    getUsersRequest({});
  }, [getUsersRequest]);

  const handleSearchChange = (
    e: any,
    { searchQuery }: { searchQuery: string },
  ) => setSearchQuery(searchQuery);

  const handleChange = (e: any, { value }: { value: string }) => {
    setValue(value);
  };

  const handleAddStoreManager = () => {
    addStoreManagerRequest(
      +value,
      +match.params.storeId,
      +match.params.storeBranchId,
    );
  };

  return (
    <div className="StoreManagerAdd">
      <Dropdown
        placeholder="Add manager"
        search
        selection
        loading={isUsersLoading}
        options={users}
        value={value}
        searchQuery={searchQuery}
        onSearchChange={handleSearchChange}
        // @ts-ignore
        onChange={handleChange}
      />
      <PrimaryButton
        loading={isAddManagerLoading}
        onClick={handleAddStoreManager}
      >
        Add
      </PrimaryButton>
      <Button className="transparent" as={Link} to="/admin/users">
        Manage Users
      </Button>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  users: getUsersDropdownArraySelector(state),
  isUsersLoading: getUsersLoadingSelector(state),
  isAddManagerLoading: getAddStoreManagerLoading(state),
});

const actions = {
  getUsersRequest,
  addStoreManagerRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(withRouter(StoreManagerAdd));
