import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { List } from 'semantic-ui-react';

import './StoreManagerListItem.scss';

import { removeStoreManagerRequest } from 'services/admin/storeManager/storeManagerActions';
import { getRemoveStoreManagerLoading } from 'services/admin/storeManager/storeManagerSelectors';

import { PrimaryButton } from 'components/Button';
import BaseModal from 'components/BaseModal';
import ConfirmModal from 'components/ConfirmModal';
import { State } from 'services/state';

interface MatchParams {
  storeId: string;
  storeBranchId: string;
}
interface IProps extends RouteComponentProps<MatchParams> {
  name: string;
  id: number;
}

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;
type Props = DispatchProps & StateProps & IProps;

const StoreManagerListItem = ({
  name,
  id,
  removeStoreManagerRequest,
  match,
  isRemoveManagerLoading,
}: Props) => (
  <List.Item className="StoreManagerListItem">
    <List.Content className="StoreManagerListItem__content">
      <List.Icon name="user" />
      <span>{name}</span>
    </List.Content>
    <List.Content>
      <BaseModal>
        {({ isModalOpen, openModal, closeModal }) => (
          <React.Fragment>
            <PrimaryButton onClick={openModal}>Delete</PrimaryButton>
            <ConfirmModal
              isLoading={isRemoveManagerLoading}
              isModalOpen={isModalOpen}
              handleClose={closeModal}
              handleConfirm={() =>
                removeStoreManagerRequest(
                  id,
                  +match.params.storeId,
                  +match.params.storeBranchId,
                )
              }
              contentText="Are you sure"
              confirmButtonText="Confirm"
            />
          </React.Fragment>
        )}
      </BaseModal>
    </List.Content>
  </List.Item>
);

const mapStateToProps = (state: State) => ({
  isRemoveManagerLoading: getRemoveStoreManagerLoading(state),
});

const actions = {
  removeStoreManagerRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(withRouter(StoreManagerListItem));
