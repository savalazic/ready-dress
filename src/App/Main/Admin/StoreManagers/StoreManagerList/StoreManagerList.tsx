import map from 'lodash/map';
import size from 'lodash/size';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { List } from 'semantic-ui-react';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { State } from 'services/state';
import { getStoreManagersRequest } from 'services/admin/storeManager/storeManagerActions';
import {
  getStoreManagersSelector,
  getStoreManagersLoadingSelector,
} from 'services/admin/storeManager/storeManagerSelectors';
import { StoreManager } from 'services/admin/storeManager/storeManagerReducer';

import StoreManagerListItem from './StoreManagerListItem';
import LoadingContainer from 'components/LoadingContainer';

interface MatchParams {
  storeId: string;
  storeBranchId: string;
}
interface IProps extends RouteComponentProps<MatchParams> {
  storeManagers: {
    [id: number]: StoreManager;
  } | null;
}

type DispatchProps = typeof actions;
type StateProps = ReturnType<typeof mapStateToProps>;
type Props = DispatchProps & StateProps & IProps;

const StoreManagerList = ({
  match,
  storeManagers,
  getStoreManagersRequest,
  isStoreManagersLoading,
}: Props) => {
  useEffect(() => {
    getStoreManagersRequest(+match.params.storeId, +match.params.storeBranchId);
  }, [
    getStoreManagersRequest,
    match.params.storeBranchId,
    match.params.storeId,
  ]);

  return (
    <LoadingContainer isLoading={isStoreManagersLoading}>
      <List divided>
        {map(storeManagers, manager => (
          <StoreManagerListItem
            key={String(manager.id)}
            name={manager.email}
            id={manager.id}
          />
        ))}
        {size(storeManagers) === 0 && <h4>No store managers</h4>}
      </List>
    </LoadingContainer>
  );
};

const mapStateToProps = (state: State) => ({
  storeManagers: getStoreManagersSelector(state),
  isStoreManagersLoading: getStoreManagersLoadingSelector(state),
});

const actions = {
  getStoreManagersRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(withRouter(StoreManagerList));
