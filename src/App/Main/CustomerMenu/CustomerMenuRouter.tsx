import React from 'react';

import ModalRoute from 'components/ModalRoute';

import Menu from './CustomerMenu';
import Appointments from './Appointments';
import AccountInfo from '../AccountInfo';
import CreditCards from './CreditCards';

const CustomerMenuRouter = () => {
  return (
    <>
      <ModalRoute component={Menu} exact path="/menu/customer" />
      <ModalRoute
        component={AccountInfo}
        path="/menu/customer/account-info"
        hasBack
      />
      <ModalRoute
        component={Appointments}
        path="/menu/customer/appointments"
        hasBack
      />
      <ModalRoute component={CreditCards} path="/menu/customer/cards" hasBack />
    </>
  );
};

export default CustomerMenuRouter;
