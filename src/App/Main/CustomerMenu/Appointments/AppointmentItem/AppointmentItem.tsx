import React from 'react';
import cx from 'classnames';
import { formatDate, formatTime } from 'utils/date';
import { StoreAppointment } from 'services/appointment/appointmentReducer';

import AppointmentViewDetails from '../../../ManagerMenu/AppointmentViewDetails';

import './AppointmentItem.scss';
import { CheckMarkIcon, DeleteIcon } from 'components/Icons';

interface Props extends StoreAppointment {
  onAppointmentAccept: (id: number) => void;
  onAppointmentDecline: (id: number) => void;
}

const AppointmentItem = ({
  id,
  from,
  to,
  status,
  articles,
  onAppointmentAccept,
  onAppointmentDecline,
}: Props) => {
  return (
    <div className="AppointmentItem">
      <div className="AppointmentItem__date">{formatDate(new Date(from))}</div>
      <div
        className={cx('AppointmentItem__wrap', {
          AppointmentItem__wrap__approve: status === 'accepted',
          AppointmentItem__wrap__decline: status === 'declined',
          AppointmentItem__wrap__pending: status === 'pending',
          'AppointmentItem__wrap__pending-new-date':
            status === 'pending_with_new_dates',
        })}
      >
        <div className="AppointmentItem__time">
          {formatTime(new Date(from))}-{formatTime(new Date(to))}
        </div>
        <AppointmentViewDetails items={articles} />
        {status === 'pending_with_new_dates' && (
          <div className="AppointmentItem__actions">
            <div
              onClick={() => onAppointmentAccept(id)}
              className="AppointmentItem__icon approve"
            >
              <CheckMarkIcon />
            </div>
            <div
              onClick={() => onAppointmentDecline(id)}
              className="AppointmentItem__icon decline"
            >
              <DeleteIcon />
            </div>
          </div>
        )}
        <div className="AppointmentItem__status">
          {status === 'accepted' && <span>ACCEPTED</span>}
          {status === 'declined' && <span>DECLINED</span>}
          {status === 'pending' && <span>PENDING</span>}
          {status === 'pending_with_new_dates' && (
            <span>PENDING WITH NEW DATES</span>
          )}
        </div>
      </div>
    </div>
  );
};

export default AppointmentItem;
