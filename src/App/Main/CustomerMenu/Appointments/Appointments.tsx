import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import './Appointments.scss';

import { State } from 'services/state';
import {
  getAppointmentsSelector,
  isGetAppointmentsLoading,
  getAppointmentAcceptLoading,
  getAppointmentDeclineLoading,
} from 'services/appointment/appointmentSelectors';
import {
  getAppointmentsRequest,
  acceptAppointmentRequest,
  declineAppointmentRequest,
} from 'services/appointment/appointmentActions';
import LoadingContainer from 'components/LoadingContainer';
import { StoreAppointment } from 'services/appointment/appointmentReducer';

import AppointmentItem from './AppointmentItem';

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof actions;

type Props = StateProps & DispatchProps;

const Appointments = ({
  appointments,
  isAppointmentsLoading,
  getAppointmentsRequest,
  acceptAppointmentRequest,
  declineAppointmentRequest,
  isAcceptLoading,
  isDeclineLoading,
}: Props) => {
  useEffect(() => {
    getAppointmentsRequest({});
  }, []);

  return (
    <LoadingContainer
      isLoading={isAppointmentsLoading || isAcceptLoading || isDeclineLoading}
      className="Appointments"
    >
      <h2>Appointments</h2>
      {appointments.map((appointment: StoreAppointment) => (
        <AppointmentItem
          key={appointment.id}
          id={appointment.id}
          from={appointment.from}
          to={appointment.to}
          status={appointment.status}
          articles={appointment.articles}
          onAppointmentAccept={id => acceptAppointmentRequest(id)}
          onAppointmentDecline={id => declineAppointmentRequest(id)}
        />
      ))}
    </LoadingContainer>
  );
};

const mapStateToProps = (state: State) => ({
  appointments: getAppointmentsSelector(state),
  isAppointmentsLoading: isGetAppointmentsLoading(state),
  isAcceptLoading: getAppointmentAcceptLoading(state),
  isDeclineLoading: getAppointmentDeclineLoading(state),
});

const actions = {
  getAppointmentsRequest,
  acceptAppointmentRequest,
  declineAppointmentRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(Appointments);
