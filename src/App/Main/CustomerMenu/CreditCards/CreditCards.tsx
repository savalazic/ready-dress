import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import ReactCreditCard from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import { Elements } from 'react-stripe-elements';

import {
  getCreditCardRequest,
  removeCreditCardRequest,
} from 'services/payment/paymentActions';
import {
  getCreditCard,
  isCreditCardLoading,
  isDeleteCreditCardLoading,
} from 'services/payment/paymentSelectors';
import LoadingContainer from 'components/LoadingContainer';
import { State } from 'services/state';
import { DangerButton, PrimaryButton } from 'components/Button';

import './CreditCards.scss';
import AddCreditCard from './AddCreditCard';

type InnerProps = typeof actions & ReturnType<typeof mapStateToProps>;

interface Props {
  onComplete(): void;
  editMode?: boolean;
}

const getIssuer = (brand: string) => {
  if (brand === 'American Express') {
    return 'amex';
  }

  if (brand === 'Visa') {
    return brand.toLowerCase();
  }

  return '';
};

const formatExpiry = (month: number, year: number) => {
  return `${String(month).padStart(2, '0')}/${String(year).slice(2)}`;
};

const formatCardNumber = (last4: string, issuer: string) => {
  let maxLength = 16;

  if (issuer === 'amex') {
    maxLength = 15;
  } else if (issuer === 'dinersclub') {
    maxLength = 14;
  } else if (['hipercard', 'mastercard', 'visa'].includes(issuer)) {
    maxLength = 19;
  }

  return last4.padStart(maxLength, '*');
};

const CreditCards: React.FC<Props & InnerProps> = ({
  getCreditCardRequest,
  removeCreditCardRequest,
  isCreditCardDeleting,
  creditCard,
  isLoading,
  editMode = true,
  onComplete,
}) => {
  useEffect(() => {
    getCreditCardRequest();
  }, [getCreditCardRequest]);

  return (
    <LoadingContainer isLoading={isLoading} className="CreditCards">
      {creditCard && (
        <div className="CreditCard">
          <ReactCreditCard
            cvc=""
            name=""
            number={formatCardNumber(
              creditCard.last4,
              getIssuer(creditCard.brand),
            )}
            issuer={getIssuer(creditCard.brand)}
            expiry={formatExpiry(creditCard.exp_month, creditCard.exp_year)}
            preview
          />
          {editMode ? (
            <DangerButton
              loading={isCreditCardDeleting}
              onClick={() => removeCreditCardRequest(creditCard.id)}
              fluid
              className="CreditCard__delete"
            >
              Delete card
            </DangerButton>
          ) : (
            <PrimaryButton fluid onClick={onComplete}>
              Submit
            </PrimaryButton>
          )}
        </div>
      )}
      {!creditCard && !isLoading && (
        <Elements>
          <AddCreditCard onAddComplete={onComplete} />
        </Elements>
      )}
    </LoadingContainer>
  );
};

const mapStateToProps = (state: State) => ({
  creditCard: getCreditCard(state),
  isLoading: isCreditCardLoading(state),
  isCreditCardDeleting: isDeleteCreditCardLoading(state),
});

const actions = {
  getCreditCardRequest,
  removeCreditCardRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(CreditCards);
