import React, { useState } from 'react';
import {
  injectStripe,
  CardElement,
  ReactStripeElements,
} from 'react-stripe-elements';
import { connect } from 'react-redux';

import { SecondaryButton } from 'components/Button';
import LoadingContainer from 'components/LoadingContainer';
import { showNotification } from 'services/notification/notificationActions';
import { saveCreditCardRequest } from 'services/payment/paymentActions';
import { State } from 'services/state';
import {
  isSaveCreditCardLoading,
  isCreditCardLoading,
} from 'services/payment/paymentSelectors';

type Props = ReactStripeElements.InjectedStripeProps &
  typeof actions &
  ReturnType<typeof mapStateToProps>;

interface OuterProps {
  onAddComplete?(): void;
}

const AddCreditCard: React.FC<Props & OuterProps> = ({
  stripe,
  saveCreditCardRequest,
  showNotification,
  isFetchingCard,
  isSavingCard,
  onAddComplete,
}) => {
  const [loading, setLoading] = useState(false);

  const handleCreditCartSubmission = async (e: React.ChangeEvent<any>) => {
    if (stripe) {
      setLoading(true);

      const { token, error } = await stripe.createToken();

      if (token) {
        setLoading(false);
        saveCreditCardRequest(token.id);
      } else {
        const defaultError = 'Something wrong happend, please try again.';
        const errorMessage = error
          ? error.message || defaultError
          : defaultError;
        showNotification(errorMessage, 3000, 'error');
        setLoading(false);
        onAddComplete && onAddComplete();
      }
    }
  };

  return (
    <LoadingContainer isLoading={loading || isFetchingCard || isSavingCard}>
      <CardElement />
      <SecondaryButton onClick={handleCreditCartSubmission} fluid>
        Save
      </SecondaryButton>
    </LoadingContainer>
  );
};

const mapStateToProps = (state: State) => ({
  isSavingCard: isSaveCreditCardLoading(state),
  isFetchingCard: isCreditCardLoading(state),
});

const actions = {
  showNotification,
  saveCreditCardRequest,
};

export default connect(
  mapStateToProps,
  actions,
)(injectStripe(AddCreditCard));
