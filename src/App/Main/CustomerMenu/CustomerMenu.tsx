import React from 'react';

import ModalLink from 'components/ModalLink';
import { PrimaryButton, SecondaryButton } from 'components/Button';
import LogoutButton from 'components/Button/LogoutButton';
import { Link } from 'react-router-dom';

const MenuLink = ({ to, children, as = ModalLink }: any) => (
  <PrimaryButton className="Menu__item" as={as} to={to} fluid>
    {children}
  </PrimaryButton>
);

const CustomerMenu = () => {
  return (
    <div className="Menu">
      <MenuLink to="/menu/customer/account-info">Account Info</MenuLink>
      <MenuLink to="/menu/customer/appointments">Appointment Requests</MenuLink>
      <MenuLink to="/menu/customer/cards">Update Card Info</MenuLink>
      <MenuLink to="/faq" as={Link}>
        FAQ
      </MenuLink>
      <MenuLink to="/contact" as={Link}>
        Contact us
      </MenuLink>
      <LogoutButton className="Menu__item" as={SecondaryButton} fluid />
    </div>
  );
};

export default CustomerMenu;
