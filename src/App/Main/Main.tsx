import React from 'react';
import { Route } from 'react-router-dom';

import Admin from './Admin';
import Merchant from './Merchant';
import Home from './Home';
import NotFound from './NotFound';
import Stores from './Stores';
import FAQ from './FAQ';
import About from './About';
import Press from './Press';
import Contact from './Contact';
import DataPrivacy from './DataPrivacy';
import ReturnPolicies from './ReturnPolicies';
import PrivateRoute from 'components/PrivateRoute';
import ModalSwitch from 'components/ModalSwitch';

import AllModalRoutes from './AllModalRoutes';

const Main = () => {
  return (
    <div className="Main">
      <ModalSwitch>
        <Route exact path="/" component={Home} />
        <Route path="/faq" component={FAQ} />
        <Route path="/about-us" component={About} />
        <Route path="/press" component={Press} />
        <Route path="/contact" component={Contact} />
        <Route path="/return-policies" component={ReturnPolicies} />
        <Route path="/data-privacy" component={DataPrivacy} />
        <Route path="/stores" component={Stores} />
        <PrivateRoute role="store_manager" path="/admin" component={Admin} />
        <PrivateRoute
          role="store_branch_manager"
          path="/merchant"
          component={Merchant}
        />
        {/* Modal routes have to be defined here  */}
        {/* Because Modal switch needs to render the background routes */}
        {/* TODO: find a way how to do the above */}
        {/* TODO: PrivateModalRoute */}
        <AllModalRoutes />

        <Route component={NotFound} />
      </ModalSwitch>
    </div>
  );
};

export default Main;
