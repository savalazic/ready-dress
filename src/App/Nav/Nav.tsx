import React, { useState, useRef } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import cx from 'classnames';
import useClickAway from 'hooks/useClickAway';

import logo from 'assets/images/logo.svg';
import './Nav.scss';

import { State } from 'services/state';
import { getAuthUser } from 'services/auth/authSelectors';
import { UserRoles, AuthUser } from 'services/auth/authReducer';

import Hanger from 'components/Hanger';
import ModalLink from 'components/ModalLink';
import ProfileMenu from 'components/ProfileMenu';
import CanView from 'components/CanView';

interface Props {
  user: AuthUser | null;
}

const Nav = ({ user }: Props) => {
  const [isOpen, setOpen] = useState(false);
  const toggleNav = () => setOpen(!isOpen);
  const closeNav = () => setOpen(false);

  const menuRef = useRef(null);
  useClickAway(menuRef, () => {
    closeNav();
  });

  return (
    <nav className="Nav" ref={menuRef}>
      <NavLink to="/">
        <img className="Nav__logo" src={logo} alt="logo" />
      </NavLink>
      <div
        className={cx('Nav__menu', {
          'Nav__menu--isOpen': isOpen,
        })}
      >
        <NavLink
          to="/faq"
          className="Nav__menuItem"
          activeClassName="Nav__menuItem--active"
          onClick={closeNav}
        >
          <Hanger />
          <span>FAQ</span>
        </NavLink>
        <NavLink
          to="/about-us"
          className="Nav__menuItem"
          activeClassName="Nav__menuItem--active"
          onClick={closeNav}
        >
          <Hanger />
          <span>About Us</span>
        </NavLink>

        <NavLink
          to="/stores"
          className="Nav__menuItem"
          activeClassName="Nav__menuItem--active"
          onClick={closeNav}
        >
          <Hanger />
          <span>Stores</span>
        </NavLink>
        {!user && (
          <ModalLink
            to="/register"
            className="Nav__menuItem"
            activeClassName="Nav__menuItem--active"
            onClick={closeNav}
          >
            <Hanger />
            <span>Register</span>
          </ModalLink>
        )}
        {!user && (
          <ModalLink
            to="/login"
            className="Nav__menuItem"
            activeClassName="Nav__menuItem--active"
            onClick={closeNav}
          >
            <Hanger />
            <span>Login</span>
          </ModalLink>
        )}
        <CanView role={UserRoles.storeManager}>
          <NavLink
            to="/admin"
            className="Nav__menuItem"
            activeClassName="Nav__menuItem--active"
            onClick={closeNav}
          >
            <Hanger />
            <span>Admin</span>
          </NavLink>
        </CanView>

        <CanView role={UserRoles.storeBranchManager}>
          <NavLink
            to="/merchant"
            className="Nav__menuItem"
            activeClassName="Nav__menuItem--active"
            onClick={closeNav}
          >
            <Hanger />
            <span>Merchant</span>
          </NavLink>
        </CanView>
        <ProfileMenu />
      </div>

      <div
        onClick={toggleNav}
        className={cx('Nav__mobile', {
          'Nav__mobile--active': isOpen,
        })}
      >
        <Hanger />
      </div>
    </nav>
  );
};

const mapStateToProps = (state: State) => ({
  user: getAuthUser(state),
});

export default connect(mapStateToProps)(Nav);
